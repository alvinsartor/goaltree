"""
Definition of urls for project.
"""

from datetime import datetime
from django.urls import path
from django.contrib import admin
from django.contrib.auth import views as auth_views
from app import forms

from app.views import aboutViews
from app.views import categoriesViews
from app.views import goalTreeViews
from app.views import homeViews
from app.views import journalViews
from app.views import webhookViews
from app.views import userMessagesViews
from app.views import dashboardViews


urlpatterns = [
    # special
    path('admin/', admin.site.urls),
    
    # home page
    path('', homeViews.home, name='home'),
    path('login/', homeViews.LoginView.as_view(), name='login'),
    path('signup/', homeViews.SignupView.as_view(), name='signup'),
    path('logout/', auth_views.LogoutView.as_view(next_page='/'), name='logout'),
    path('password-reset/', auth_views.PasswordResetView.as_view(email_template_name='app/emails/email-password-recovery.html', subject_template_name='app/emails/email-password-recovery-subject.txt', success_url='/')),
    path('password-reset/<uidb64>/<token>/', auth_views.PasswordResetConfirmView.as_view(success_url='/'), name='password-reset-confirm'),
    
    # categories page:
    path('categories/', categoriesViews.CategoriesView.as_view(), name='categories'),
    path('categories/archive/', categoriesViews.ArchivedCategoriesView.as_view(), name='categories-archive'),
    path('create_category/', categoriesViews.CreateCategoryView.as_view(), name='CreateCategory'),
    path('update_category/<int:pk>', categoriesViews.UpdateCategoryView.as_view(), name='UpdateCategory'),
    path('delete_category/<int:pk>', categoriesViews.DeleteCategoryView.as_view(), name='DeleteCategory'),

    # goals page:
    path('tree/<int:category_pk>', goalTreeViews.TreeView.as_view(), name='tree'),
    path('create_edge/<int:goal_pk_start>/<int:goal_pk_end>', goalTreeViews.create_edge, name='createEdge'),
    path('delete_edge/<int:goal_pk_start>/<int:goal_pk_end>', goalTreeViews.delete_edge, name='deleteEdge'),
    path('create_goal/', goalTreeViews.CreateGoalView.as_view(), name='createGoal'),
    path('update_goal/<int:pk>', goalTreeViews.UpdateGoalView.as_view(), name='updateGoal'),
    path('delete_goal/<int:pk>', goalTreeViews.DeleteGoalView.as_view(), name='deleteGoal'),
    path('update_category_settings/<int:pk>', categoriesViews.UpdateCategorySettings.as_view(), name='UpdateCategorySettings'),
    
    # journal
    path('journal/<int:pk>', journalViews.CategoryJournalEntriesView.as_view(), name='journalEntries'),
    path('create_journal_entry/', journalViews.CreateJournalEntryView.as_view(), name='createJournalEntries'),
    path('edit_journal_entry/<int:pk>', journalViews.UpdateJournalEntryView.as_view(), name='editJournalEntries'),
    path('delete_journal_entry/<int:pk>', journalViews.DeleteJournalEntryView.as_view(), name='deleteJournalEntries'),

    # dashboard:
    path('dashboard/', dashboardViews.UserDashboardView.as_view(), name='dashboard'),
    path('dashboard/category_stats/<int:pk>', dashboardViews.CategoryStatsView.as_view(), name='categoryStats'),
    path('dashboard/user_exp_stats/<int:pk>', dashboardViews.UserExperienceStatsView.as_view(), name='userStats'),
    path('dashboard/user_stats/<int:pk>', dashboardViews.UserStatsView.as_view(), name='userStats'),

    # about
    path('about/', aboutViews.AboutView.as_view(), name='about'),

    # user messages
    path('user-messages-status/', userMessagesViews.UserMessagesView.as_view()),   
    path('user-messages/home-completed', userMessagesViews.SetHomeTutorialAsCompletedView.as_view()),   
    path('user-messages/categories-completed', userMessagesViews.SetCategoriesTutorialAsCompletedView.as_view()),
    path('user-messages/tree-completed', userMessagesViews.SetTreeTutorialAsCompletedView.as_view()),   
    path('user-messages/dashboard-completed', userMessagesViews.SetDashboardTutorialAsCompletedView.as_view()),   
    path('user-messages/donation-completed', userMessagesViews.SetDonationThanksAsShowed.as_view()),   

    # webhooks
    path('webhook/bmac', webhookViews.BuyMeACoffeDonationWebhook),
]
