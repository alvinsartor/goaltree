import json

from django.shortcuts import render
from django.http import JsonResponse, Http404
from django.core import serializers
from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import login_required
from django.views import View
from django.views.generic.detail import DetailView

from app.models import Category, Color, User

    
@method_decorator(login_required, name='dispatch')
class CategoryStatsView(DetailView):
    model = Category
    
    def get(self, request, *args, **kwargs):
        object = self.get_object()
        if self.get_object().user == self.request.user:
            stats = object.get_statistics()
            return JsonResponse(stats, safe=False, status=200)     
        else:
            return Http404()


@method_decorator(login_required, name='dispatch')
class UserStatsView(DetailView):
    model = User
    
    def get(self, request, *args, **kwargs):
        object = self.get_object()
        if object == self.request.user:
            stats = object.get_statistics()
            return JsonResponse(stats, safe=False, status=200)     
        else:
            return Http404()


@method_decorator(login_required, name='dispatch')
class UserExperienceStatsView(DetailView):
    model = User
    
    def get(self, request, *args, **kwargs):
        object = self.get_object()
        if object == self.request.user:
            stats = object.get_exp_statistics()
            return JsonResponse(stats, safe=False, status=200)     
        else:
            return Http404()


@method_decorator(login_required, name='dispatch')
class UserDashboardView(View):
    def get(self, request, *args, **kwargs):
        allColors = list(Color.objects.all().order_by('order'))
        return render(
            request, 
            'app/page-dashboard.html', 
            {
                'categories': Category.objects.filter(user=request.user, archived=False).select_related('canvas_color'),
                'colors': serializers.serialize("json", allColors),               
            })

