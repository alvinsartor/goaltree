from django.core.exceptions import PermissionDenied
from django.core import serializers
from django.http import JsonResponse
from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import login_required
from django.views.generic.edit import FormView
from django.views.generic.edit import UpdateView
from django.views.generic.edit import DeleteView
from django.views.generic.detail import DetailView

from app.forms import JournalEntryForm
from app.models import JournalEntry, Category


@method_decorator(login_required, name='dispatch')
class CategoryJournalEntriesView(DetailView):
    model = Category
    
    def get(self, request, *args, **kwargs):
        category = self.get_object()
        if self.get_object().user == self.request.user:
            journal_entries = category.journal_entries.all().order_by('date').prefetch_related('category')
            jsons = [entry.to_json() for entry in journal_entries]
            return JsonResponse(jsons, safe=False)
        else:
            return Http404()


@method_decorator(login_required, name='dispatch')
class CreateJournalEntryView(FormView):
    model = JournalEntry
    form_class = JournalEntryForm
    template_name = ''

    def form_valid(self, form):
        entry = form.save()
        return JsonResponse(entry.to_json())

    def get_form_kwargs(self):
        """Just checking that the owner of the category where the journal entry is added 
        is also the user that is making the request"""
        kwargs = super().get_form_kwargs()
        category = kwargs['data']['category']
        if not category or Category.objects.get(pk=category).user != self.request.user:
            raise PermissionDenied()
        return kwargs



@method_decorator(login_required, name='dispatch')
class UpdateJournalEntryView(UpdateView):
    model = JournalEntry
    form_class = JournalEntryForm
    template_name = ''
    success_url = '/'

    def form_valid(self, form):
        entry = form.save()
        return JsonResponse(entry.to_json())

    def get_object(self, *args, **kwargs):
        obj = super().get_object(*args, **kwargs)
        if obj.category.user != self.request.user:
            raise PermissionDenied()
        return obj  
    

@method_decorator(login_required, name='dispatch')
class DeleteJournalEntryView(DeleteView):
    model = JournalEntry
    form_class = JournalEntryForm
    template_name = ''
    success_url = '/'
 
    def get_object(self, *args, **kwargs):
        obj = super().get_object(*args, **kwargs)
        if obj.category.user != self.request.user:
            raise PermissionDenied()
        return obj

