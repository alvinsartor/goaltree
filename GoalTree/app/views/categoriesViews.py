import json

from django.shortcuts import render
from django.core.exceptions import PermissionDenied
from django.http import HttpResponse
from django.core import serializers
from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import login_required
from django.views import View
from django.views.generic.edit import FormView
from django.views.generic.edit import UpdateView
from django.views.generic.edit import DeleteView

from app.forms import CategorySettingsForm, CategoryForm
from app.models import Category, Color


@method_decorator(login_required, name='dispatch')
class UpdateCategorySettings(UpdateView):
    model = Category
    form_class = CategorySettingsForm
    template_name = ''
    success_url = '/'

  
@method_decorator(login_required, name='dispatch')
class CategoriesView(View):
    def get(self, request):
        allCategories = list(Category.objects.filter(user=request.user, archived=False).order_by('pk'))
        allColors = list(Color.objects.all().order_by('order'))
        return render(
            request, 
            'app/page-categories.html', 
            { 
                'userPk': request.user.pk,
                'categories': serializers.serialize("json", allCategories),
                'colors': serializers.serialize("json", allColors),
                'categoryForm': CategoryForm(),
                'is_archive': False,
            }
        )
 

@method_decorator(login_required, name='dispatch')
class ArchivedCategoriesView(View):
    def get(self, request):
        allCategories = list(Category.objects.filter(user=request.user, archived=True).order_by('pk'))
        allColors = list(Color.objects.all().order_by('order'))
        return render(
            request, 
            'app/page-categories.html', 
            { 
                'userPk': request.user.pk,
                'categories': serializers.serialize("json", allCategories),
                'colors': serializers.serialize("json", allColors),
                'categoryForm': CategoryForm(),
                'is_archive': True,
            }
        )
 

@method_decorator(login_required, name='dispatch')
class UpdateCategoryView(UpdateView):
    model = Category
    form_class = CategoryForm
    template_name = ''
    success_url = '/'

    def form_valid(self, form):
        category = form.save()
        return HttpResponse(
            serializers.serialize("json", [category]), 
            content_type="application/json", 
            status=200)

    def get_object(self, *args, **kwargs):
        obj = super().get_object(*args, **kwargs)
        if obj.user != self.request.user:
            raise PermissionDenied()
        return obj

    def get_form_kwargs(self):
        """Injecting user just before of checking the form"""
        kwargs = super().get_form_kwargs()
        data = kwargs['data'].copy()
        _mutable = data._mutable
        data._mutable = True
        data['user'] = self.request.user.pk
        data._mutable = _mutable
        return {
            'data': data, 
            'files': kwargs['files'], 
            'initial': kwargs['initial'], 
            'prefix': kwargs['prefix'],
            'instance': kwargs['instance']
        }

        
@method_decorator(login_required, name='dispatch')
class CreateCategoryView(FormView):
    model = Category
    form_class = CategoryForm
    template_name = ''

    def form_valid(self, form):
        category = form.save()
        return HttpResponse(
            serializers.serialize("json", [category]), 
            content_type="application/json", 
            status=200)
        
    def get_form_kwargs(self):
        """Injecting user just before of checking the form"""
        kwargs = super().get_form_kwargs()
        data = kwargs['data'].copy()
        _mutable = data._mutable
        data._mutable = True
        data['user'] = self.request.user.pk
        data._mutable = _mutable
        return {
            'data': data, 
            'files': kwargs['files'], 
            'initial': kwargs['initial'], 
            'prefix': kwargs['prefix'],
        }

@method_decorator(login_required, name='dispatch')
class DeleteCategoryView(DeleteView):
    model = Category
    form_class = CategoryForm
    template_name = ''
    success_url = '/'

    def get_object(self, *args, **kwargs):
        obj = super().get_object(*args, **kwargs)
        if obj.user != self.request.user:
            raise PermissionDenied()
        return obj
