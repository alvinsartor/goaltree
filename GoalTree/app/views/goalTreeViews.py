import json

from django.db import connection
from django.shortcuts import render
from django.core.exceptions import PermissionDenied
from django.http import HttpResponse, Http404
from django.core import serializers
from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import login_required
from django.views import View
from django.views.generic.edit import FormView
from django.views.generic.edit import UpdateView
from django.views.generic.edit import DeleteView

from app.forms import GoalForm, CategorySettingsForm
from app.models import Category, Goal, Color, Experience
from app.serializers import GoalSerializer


@method_decorator(login_required, name='dispatch')
class TreeView(View):

    def get(self, request, *args, **kwargs):
        category_pk = kwargs['category_pk']
        try:
            category = Category.objects.get(pk=category_pk, user=request.user);
        except Category.DoesNotExist:
            raise Http404()        

        goals = GoalSerializer.serialize_queryset(Goal.objects.filter(category__name = category.name))
        goals_pks = [str(goal['id']) for goal in goals]       
        edges = self.fetch_edges_list_for_goals(goals_pks) if goals_pks else []
        colors = list(Color.objects.all().order_by('order'))
 
        return render(
            request,
            'app/page-goal-tree.html',
            {
                'userPk': request.user.pk,
                'category': category,
                'canvasColor': serializers.serialize("json", [category.canvas_color]),
                'goalsColor': serializers.serialize("json", [category.goals_color]),
                'currentCategoryPk':category_pk,         
                'goals': json.dumps(goals),
                'edges': json.dumps(edges),
                'experience_levels': serializers.serialize("json", list(Experience.objects.all())),
                'goalForm': GoalForm(),
                'categorySettingsForm': CategorySettingsForm(instance=category),
                'colors': serializers.serialize("json", colors),
                'move_ancestors_along': category.move_ancestors_along,
            }
        )

    def fetch_edges_list_for_goals(self, goal_pks):
        with connection.cursor() as cursor:
            pks = ",".join(goal_pks)
            cursor.execute('SELECT from_goal_id, to_goal_id FROM app_goal_higher_goals WHERE from_goal_id IN (' + pks + ')')
            rows = cursor.fetchall()
 
        return ['{}-{}'.format(r[0],r[1]) for r in rows]    


@login_required
def create_edge(request, goal_pk_start, goal_pk_end):
    goalStart = Goal.objects.get(pk=goal_pk_start)
    goalEnd = Goal.objects.get(pk=goal_pk_end)
   
    if goalStart.category.user != request.user:
        raise PermissionDenied()

    if request.method == 'POST' and goalStart is not None and goalEnd is not None:
        goalStart.higher_goals.add(goalEnd);
        return HttpResponse(status=200)
    else:
        return HttpResponse(
            json.dumps({"nothing to see": "this isn't happening"}),
            content_type="application/json"
        )

@login_required
def delete_edge(request, goal_pk_start, goal_pk_end):
    goalStart = Goal.objects.get(pk=goal_pk_start)
    goalEnd = Goal.objects.get(pk=goal_pk_end)
    
    if goalStart.category.user != request.user:
        raise PermissionDenied()

    goalsAreValid = goalStart is not None and goalEnd is not None and \
        goal_pk_end in goalStart.higher_goals.values_list("id", flat=True)

    if request.method == 'POST' and goalsAreValid:
        goalStart.higher_goals.remove(goalEnd);
        return HttpResponse(status=200)
    else:
        return HttpResponse(
            json.dumps({"nothing to see": "this isn't happening"}),
            content_type="application/json"
        )

@method_decorator(login_required, name='dispatch')
class CreateGoalView(FormView):
    model = Goal
    form_class = GoalForm
    template_name = ''

    def form_valid(self, form):
        goal = form.save()
        return HttpResponse(
            json.dumps(GoalSerializer.serialize_single(goal)),
            content_type="application/json", 
            status=200)

    def get_form_kwargs(self):
        """Just checking that the owner of the category where the goal is added 
        is also the user that is making the request"""
        kwargs = super().get_form_kwargs()
        category = kwargs['data']['category']
        if not category or Category.objects.get(pk=category).user != self.request.user:
            raise PermissionDenied()
        return kwargs
    
@method_decorator(login_required, name='dispatch')
class UpdateGoalView(UpdateView):
    model = Goal
    form_class = GoalForm
    template_name = ''
    success_url = '/'

    def form_valid(self, form):
        goal = form.save()
        return HttpResponse(
            json.dumps(GoalSerializer.serialize_single(goal)),
            content_type="application/json", 
            status=200)

    def get_object(self, *args, **kwargs):
        obj = super().get_object(*args, **kwargs)
        if obj.category.user != self.request.user:
            raise PermissionDenied()
        return obj  
    

@method_decorator(login_required, name='dispatch')
class DeleteGoalView(DeleteView):
    model = Goal
    form_class = GoalForm
    template_name = ''
    success_url = '/'
 
    def get_object(self, *args, **kwargs):
        obj = super().get_object(*args, **kwargs)
        if obj.category.user != self.request.user:
            raise PermissionDenied()
        return obj   