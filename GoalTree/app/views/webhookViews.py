import json

from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
from django.core.mail import send_mail

from app.models import User


@csrf_exempt    
def BuyMeACoffeDonationWebhook(request):
    try:
        body = json.loads(request.body.decode('utf-8'))
        user = User.objects.get(email=body['response']['supporter_email'])
        user.has_donated=True
        user.save()
    except Exception as e:
        email_content = str(e) + "\n\n\n" + str(request.__dict__)
        send_mail('Error in BMAC webhook', email_content, 'goaltree@alwaysdata.net', ['ananasproject@gmail.com'], fail_silently=False)
    return HttpResponse(status=200)   

