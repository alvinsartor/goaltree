import json

from django.contrib.auth import authenticate, login
from django.forms.models import model_to_dict
from django.http import HttpRequest
from django.http import HttpResponse
from django.http import HttpResponseRedirect
from django.http import JsonResponse
from django.shortcuts import render
from django.urls import reverse
from django.views import View

from app.models import User, UserMessages


def home(request):
    """Renders the home page."""
    assert isinstance(request, HttpRequest)
    return render(request, 'app/page-home.html')


class LoginView(View):
    def post(self, request):
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(username=username, password=password)

        if user is None:
            return HttpResponse('User not found', status=404)    
            
        login(request, user)
        return HttpResponseRedirect(reverse('home'), status=200)

        
class SignupView(View):
    def post(self, request):
        username = request.POST['username']
        password = request.POST['password']
               
        if len(password) < 8: 
            return HttpResponse( "Come on, at least 8 characters, I am not asking much.", status=400)

        if User.objects.filter(email=username).exists():
            return HttpResponse("This email is alreaady registered. Did you forget your password?", status=400)

        user = User(email=username)
        user.set_password(password)
        user.save()

        user = authenticate(username=username, password=password)
        login(request, user)
        return HttpResponseRedirect(reverse('home'), status=200)

class PasswordRecoveryView(View):
    def post(self, request):
        email = request.POST['email']
