from abc import ABC, abstractmethod

from django.forms.models import model_to_dict
from django.http import JsonResponse, HttpResponse
from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import login_required
from django.views.decorators.csrf import csrf_exempt
from django.views import View

from app.models import UserMessages


@method_decorator(login_required, name='dispatch')
class UserMessagesView(View):
    def get(self, request):
        try:
            messages = request.user.messages
        except UserMessages.DoesNotExist:
            messages = UserMessages(user=request.user)
            messages.save()
        return JsonResponse(model_to_dict(messages))


@method_decorator(login_required, name='dispatch')
@method_decorator(csrf_exempt, name='dispatch')
class AbstractUserMessagesTicker(View, ABC):

    @property
    @abstractmethod
    def property_to_set_as_completed(self):
        """ The UserMessages field to set to True for the current user. """

    def post(self, request):
        data_dictionary = { self.property_to_set_as_completed : True }
        UserMessages.objects.filter(user=request.user.messages).update(**data_dictionary)
        return HttpResponse(status=200)


class SetHomeTutorialAsCompletedView(AbstractUserMessagesTicker):
    property_to_set_as_completed = 'has_seen_tutorial_home'

class SetCategoriesTutorialAsCompletedView(AbstractUserMessagesTicker):
    property_to_set_as_completed = 'has_seen_tutorial_categories'

class SetTreeTutorialAsCompletedView(AbstractUserMessagesTicker):
    property_to_set_as_completed = 'has_seen_tutorial_tree'

class SetDashboardTutorialAsCompletedView(AbstractUserMessagesTicker):
    property_to_set_as_completed = 'has_seen_tutorial_dashboard'

class SetDonationThanksAsShowed(AbstractUserMessagesTicker):
    property_to_set_as_completed = 'has_been_thanked_for_donation'
