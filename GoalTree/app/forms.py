from django.forms import HiddenInput, TextInput, ModelForm, Textarea, CharField, PasswordInput, CheckboxInput, ModelChoiceField
from django.contrib.auth.forms import AuthenticationForm
from django.contrib.auth.forms import UsernameField
from django.utils.translation import ugettext_lazy as _
from app.models import Goal, Color, Category, User, JournalEntry


class GoalForm(ModelForm):
    class Meta: 
        model = Goal
        fields = ['title', 'description', 'completed', 'experience', 'posX', 'posY', 'category', 'pinned']
        widgets = {
            'title': TextInput(attrs={'class': 'goal-title-edit'}),
            'description': Textarea(attrs={'cols': 35, 'rows': 3, 'class':'goal-description-edit'}),
            'completed': CheckboxInput(attrs={'class':'toggle-input', 'id':'switch'}),
            'experience': HiddenInput(),
            'posX': HiddenInput(), 
            'posY': HiddenInput(), 
            'category': HiddenInput(),
            'pinned': CheckboxInput(attrs={'style': 'display: none;'}),
            }


class CategorySettingsForm(ModelForm):
    class Meta:
        model = Category
        fields = ['panX', 'panY', 'scale', 'goals_color', 'move_ancestors_along']
        widgets = {
            'panX': HiddenInput(), 
            'panY': HiddenInput(), 
            'scale': HiddenInput(), 
            'goals_color': HiddenInput(), 
            'move_ancestors_along': HiddenInput(),
            }


class CategoryForm(ModelForm):
    class Meta: 
        model = Category
        canvas_color = ModelChoiceField(queryset=Color.objects.all(), to_field_name='name')
        goals_color = ModelChoiceField(queryset=Color.objects.all(), to_field_name='name')

        fields = ['name', 'description', 'canvas_color', 'user', 'archived']
        widgets = {
            'name': TextInput(attrs={'class': 'category-name-edit'}),
            'description': Textarea(attrs={'cols': 40, 'rows': 2, 'class':'category-desc-edit', 'resize' : 'none'}),
            'user': HiddenInput(),
            'archived': HiddenInput(),
            }


class JournalEntryForm(ModelForm):
    class Meta: 
        model = JournalEntry
        fields = ['text', 'category']
        