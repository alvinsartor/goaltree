from django.forms.models import model_to_dict

class GoalSerializer:
    fields = ['id', 'title', 'description', 'experience', 'posX', 'posY', 'pinned', 'completed']

    @classmethod
    def serialize_queryset(cls, goals):
        return list(goals.values(*cls.fields))

    @classmethod
    def serialize_collection(cls, goals):
        return [cls.serialize_single(goal) for goal in goals]

    @classmethod
    def serialize_single(cls, goal):
        return model_to_dict(goal, fields=cls.fields)