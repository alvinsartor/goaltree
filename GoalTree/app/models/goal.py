from django.db import models
from django.utils import timezone

from app.models.category import Category
from app.models.experience import Experience

class Goal(models.Model):
    """Goals are the objectives to be achieved and represents the nodes of the tree."""
    title = models.CharField(max_length=200, blank = False, null = False)
    description = models.CharField(max_length=1000, blank = True)
    pub_date = models.DateTimeField('date published', auto_now_add=True)
    completion_date = models.DateTimeField('date completion', blank=True, null=True, default=None)
    completed = models.BooleanField(blank=True, default=False)
    posX = models.FloatField(default=0.0)
    posY = models.FloatField(default=0.0)
    category = models.ForeignKey(Category, on_delete=models.CASCADE)
    higher_goals = models.ManyToManyField("self", blank=True, symmetrical=False, related_name="lesser_goals")
    experience = models.ForeignKey(Experience, default=1, on_delete=models.DO_NOTHING)
    pinned = models.BooleanField(blank=True, default=False)

    def is_leaf_goal(self):
        """Determines whether this goal has no connected lower goals."""
        return self.lesser_goals.count() == 0

    def is_final_goal(self):
        """Determines whether this goal has no connected lower goals."""
        return self.higher_goals.count() == 0

    def save(self, *args, **kwargs):
        if self.completed and self.completion_date is None:
            self.completion_date = timezone.now()
        elif not self.completed and self.completion_date is not None:
            self.completion_date = None
        super().save(*args, **kwargs)

    def __str__(self):
        return f'{self.title} - {self.description}'
