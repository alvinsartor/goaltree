from django.db import models

from app.models.category import Category

class JournalEntry(models.Model):
    text = models.TextField()
    date = models.DateTimeField(auto_now_add=True)
    category = models.ForeignKey(Category, on_delete=models.CASCADE, related_name="journal_entries")

    def to_json(self):
        return {
            'pk': self.pk,
            'category': self.category.pk,
            'date': self.date.timestamp(),
            'text': self.text
            }