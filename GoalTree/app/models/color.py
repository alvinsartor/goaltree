from django.db import models

class Color(models.Model):
    """Colors are used by the users to customise the appearence of the website."""
    name = models.CharField(max_length=50, blank=False, null=False)
    c1 = models.CharField(max_length=7, blank=False, null=False)
    c3 = models.CharField(max_length=7, blank=False, null=False)
    c5 = models.CharField(max_length=7, blank=False, null=False)
    c7 = models.CharField(max_length=7, blank=False, null=False)
    c9 = models.CharField(max_length=7, blank=False, null=False)
    text =  models.CharField(max_length=7, blank=False, null=False, default="#FFFFFF")
    order = models.IntegerField(default=0)

    def __str__(self):
        return self.name