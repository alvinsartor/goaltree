import time

from django.db import models
from django.db.models import Sum
from django.contrib.auth.models import AbstractUser, BaseUserManager

from app.utils import UserLevelHandler, GoalsStatsExtractor

class UserManager(BaseUserManager):
    """Define a model manager for User model with no username field."""

    use_in_migrations = True

    def _create_user(self, email, password, **extra_fields):
        """Create and save a User with the given email and password."""
        if not email:
            raise ValueError('The given email must be set')
        email = self.normalize_email(email)
        user = self.model(email=email, **extra_fields)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_user(self, email, password=None, **extra_fields):
        """Create and save a regular User with the given email and password."""
        extra_fields.setdefault('is_staff', False)
        extra_fields.setdefault('is_superuser', False)
        return self._create_user(email, password, **extra_fields)

    def create_superuser(self, email, password, **extra_fields):
        """Create and save a SuperUser with the given email and password."""
        extra_fields.setdefault('is_staff', True)
        extra_fields.setdefault('is_superuser', True)

        if extra_fields.get('is_staff') is not True:
            raise ValueError('Superuser must have is_staff=True.')
        if extra_fields.get('is_superuser') is not True:
            raise ValueError('Superuser must have is_superuser=True.')

        return self._create_user(email, password, **extra_fields)


class User(AbstractUser):
    """User object for the application"""       
    username = None
    email = models.EmailField('email address', unique=True)       
    has_donated = models.BooleanField(blank=True, default=False)

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = []

    objects = UserManager()

    def get_experience_points(self):
        """Computes the experience points accumulated by a user"""
        from app.models.goal import Goal
        return Goal.objects.filter(category__user=self, completed=True).aggregate(s=Sum('experience__exp_points'))['s'] or 0;

    def get_exp_statistics(self):
        """Gathers information about experience and level of a user"""
        experience = self.get_experience_points()
        level, exp_needed_to_lv_up = UserLevelHandler.get_user_level_from_experience(experience)
        return {
            'timestamp': time.time(),
            'level': level,
            'experience': experience,
            'exp_needed_to_lv_up': exp_needed_to_lv_up,
            'level_completion_percentage': UserLevelHandler.get_level_completion_percentage(level, experience),            
        }

    def get_statistics(self):
        from app.models.goal import Goal
        from app.models.category import Category

        """Gathers information useful to describe the current state of a user"""
        categories = Category.objects.filter(user=self, archived=False)
        goals = Goal.objects.filter(category__user=self, category__archived=False)
        experience = self.get_experience_points()
        level, exp_needed_to_lv_up = UserLevelHandler.get_user_level_from_experience(experience)
        return {
            'timestamp': time.time(),
            'name': self.email,
            'level': level,
            'experience': experience,
            'exp_needed_to_lv_up': exp_needed_to_lv_up,
            'level_completion_percentage': UserLevelHandler.get_level_completion_percentage(level, experience),
            'nr_categories': categories.count(),
            'hot_categories': GoalsStatsExtractor.get_goal_distribution_over_categories(categories),
            'goals_count': goals.count(),
            'completed': goals.filter(completed=True).count(),
            'open': goals.filter(completed=False).count(),
            'experience_gained': experience,
            'experience_available': goals.filter(completed=False).aggregate(s=Sum('experience__exp_points'))['s'] or 0,
            'goal_distribution' : GoalsStatsExtractor.get_goal_difficulty_distribution(goals),
            'goals_completion_sum': GoalsStatsExtractor.get_goals_completion_sum_for_last_period(goals),
            'goals_addition_sum': GoalsStatsExtractor.get_goals_addition_sum_for_last_period(goals),
            'latest_completed_goals': GoalsStatsExtractor.get_latest_completed_goals(goals)
        }

class UserMessages(models.Model):
    """ Model containing the information relative to the messages shown to the user """
    user = models.OneToOneField(User, on_delete=models.CASCADE, primary_key=True, related_name="messages")
    has_been_thanked_for_donation = models.BooleanField(blank=True, default=False)
    has_seen_tutorial_home = models.BooleanField(blank=True, default=False)
    has_seen_tutorial_categories = models.BooleanField(blank=True, default=False)
    has_seen_tutorial_tree = models.BooleanField(blank=True, default=False)
    has_seen_tutorial_dashboard = models.BooleanField(blank=True, default=False)
