import time

from django.db import models
from django.db.models import Sum, Count

from app.utils import GoalsStatsExtractor
from app.models.user import User
from app.models.color import Color

class Category(models.Model):
    """Categories include multiple goals."""
    name = models.CharField(max_length=200)
    description = models.CharField(max_length=100, blank = True)
    user = models.ForeignKey(User, null=False, on_delete=models.CASCADE)

    # category settings
    panX = models.IntegerField(default=0)
    panY = models.IntegerField(default=0)
    scale = models.FloatField(default=1)
    canvas_color = models.ForeignKey(Color, default=1, null=False, on_delete=models.DO_NOTHING, related_name="canvas")
    goals_color = models.ForeignKey(Color, default=2, null=False, on_delete=models.DO_NOTHING, related_name="goals")
    move_ancestors_along = models.BooleanField(blank=True, default=False)
    archived = models.BooleanField(blank=True, default=False)

    class Meta:
        verbose_name_plural = "Categories"

    def __str__(self):
        return self.name

    def get_goal_difficulty_distribution(self):        
        difficulty_distribution = Goal.objects.filter(category=self)\
            .values('experience__level')\
            .annotate(lv_count=Count('experience__level'))\
            .order_by('experience__level')
        on_dict = dict((lv['experience__level'], lv['lv_count']) for lv in difficulty_distribution)        
        # now we fill the gaps with [lv, 0] to make it easier for the client-side script
        result = []
        for i in range(1, 11):
            count_i = [i, on_dict[i]] if i in on_dict else [i, 0] 
            result.append(count_i)
        return result

    def get_statistics(self):
        """Returns a dictionary with the statistics for this category"""
        goals = self.goal_set.all()
        return {
            'timestamp': time.time(),
            'name': self.name,
            'goals_count': goals.count(),
            'completed': goals.filter(completed=True).count(),
            'open': goals.filter(completed=False).count(),
            'experience_gained': goals.filter(completed=True).aggregate(s=Sum('experience__exp_points'))['s'] or 0,
            'experience_available': goals.filter(completed=False).aggregate(s=Sum('experience__exp_points'))['s'] or 0,
            'goal_distribution' : GoalsStatsExtractor.get_goal_difficulty_distribution(goals),
            'goals_completion_sum': GoalsStatsExtractor.get_goals_completion_sum_for_last_period(goals),
            'goals_addition_sum': GoalsStatsExtractor.get_goals_addition_sum_for_last_period(goals),
            'latest_completed_goals': GoalsStatsExtractor.get_latest_completed_goals(goals)
        }

    def save(self, *args, **kwargs):
        if not self.goal_set.exists():
            self.goals_color = self.canvas_color
        super().save(*args, **kwargs)
        