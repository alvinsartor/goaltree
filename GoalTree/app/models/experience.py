from django.db import models

class Experience(models.Model):
    """Each goal has a certain amount of experience assigned to it.
    A model is more apt than a number as it is possible to associate 
    a description and an arbitrary amount of exp. points to it."""
    level = models.IntegerField(unique=True)
    description = models.CharField(max_length=255)
    exp_points = models.IntegerField(default=1)
