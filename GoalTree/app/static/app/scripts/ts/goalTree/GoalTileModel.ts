interface IGoalTileJson {
    id: number;
    title: string;
    description: string;
    experience: number;
    completed: boolean;
    posX: number;
    posY: number;
    pinned: boolean;
}

class GoalTileModel {
    public tileName: string;
    public pk: number;
    public title: string;
    public description: string;
    private experience: Experience;
    public completed: boolean;
    public pinned: boolean;
    public position: Point;
    private destroying = false;

    public readonly goalTile: HTMLElement;
    private readonly goalForm: HTMLFormElement;
    private readonly goalTitleEdit: HTMLInputElement;
    private readonly goalDescriptionEdit: HTMLTextAreaElement;
    private readonly goalCompletedEdit: HTMLInputElement;
    private readonly goalExperienceEdit: HTMLInputElement;
    private readonly goalExperienceLabel: HTMLSpanElement;
    private readonly goalExperiencePassive: HTMLInputElement;
    private readonly goalDeleteBt: HTMLButtonElement;
    private readonly goalPinIcon: HTMLButtonElement;
    private readonly goalPinToggle: HTMLButtonElement;
    private readonly goalPinPassive: HTMLInputElement;

    private readonly toggleColorOff = "#888888";
    private toggleColorOn = () => Settings.instance().goalsColor.text;

    //variables used for drag&drop
    private dragInitial = Point.zero();
    private dragActual = Point.zero();
    private readonly onGoalMoved = new LiteEvent<Point>();

    public get goalMoved(): ILiteEvent<Point> { return this.onGoalMoved.expose(); }

    /**
    * Initializes a new instance of GoalTileModel using an identifier and a json containing the necessary information.
    */
    constructor(goalTileJson: IGoalTileJson) {
        this.tileName = GoalTileModel.getTileName(goalTileJson.id);
        this.pk = goalTileJson.id;
        this.title = goalTileJson.title;
        this.description = goalTileJson.description;
        this.experience = Settings.instance().getExperienceFromPk(goalTileJson.experience);
        this.completed = goalTileJson.completed;
        this.pinned = goalTileJson.pinned;
        this.position = new Point(goalTileJson.posX, goalTileJson.posY);

        const template = (document.getElementById("goal-tile-template") as HTMLTemplateElement)
            .content.querySelector(".goal-tile.live");

        this.goalTile = document.importNode<any>(template, true);
        this.goalForm = this.goalTile.querySelector("form") as HTMLFormElement;
        this.goalTitleEdit = this.goalTile.querySelector(".goal-title-edit") as HTMLInputElement;
        this.goalDescriptionEdit = this.goalTile.querySelector(".goal-description-edit") as HTMLTextAreaElement;
        this.goalExperienceEdit = this.goalTile.querySelector("input[name='experience-range']") as HTMLInputElement;
        this.goalExperiencePassive = this.goalTile.querySelector("input[name='experience']") as HTMLInputElement;
        this.goalExperienceLabel = this.goalTile.querySelector(".range-result") as HTMLSpanElement;
        this.goalCompletedEdit = this.goalTile.querySelector(".toggle-input") as HTMLInputElement;
        this.goalDeleteBt = this.goalTile.querySelector('button[class="delete-goal-bt"') as HTMLButtonElement;
        this.goalPinToggle = this.goalTile.querySelector('button[class="pin-goal-bt"') as HTMLButtonElement;
        this.goalPinIcon = this.goalTile.querySelector('div[class="pin-goal-bt"') as HTMLButtonElement;
        this.goalPinPassive = this.goalTile.querySelector('input[name="pinned"') as HTMLInputElement;

        this.populateTile();
        this.addTileEventListeners();
        CategoryModel.instance().canvas.appendChild(this.goalTile);
    }

    /**
     * Function called when some information of the goal has been changed and it needs to be refreshed.
     * @param goalTileJson the JSON containing the new information.
     */
    public updateGoalInformation(goalTileJson: IGoalTileJson) {
        if (this.pk !== goalTileJson.id)
            throw Error("Are you trying to substitute this goal with another one? This is not possible");

        this.title = goalTileJson.title;
        this.description = goalTileJson.description;
        this.completed = goalTileJson.completed;
        this.position = new Point(goalTileJson.posX, goalTileJson.posY);
        this.experience = Settings.instance().getExperienceFromPk(goalTileJson.experience);
        this.pinned = goalTileJson.pinned;

        this.populateTile();
        this.repaintGoalTile();
    }

    /**
     * Given a goal pk, the function returns a tile name.
     * @param pk
     */
    public static getTileName(pk: number): string {
        return `goal-tile-${pk}`;
    }

    /**
     * Returns the measurements of the goal model tile.
     */
    public extractMeasurements(): Rectangle {
        const tile = document.getElementById(this.tileName);
        if (tile === null || tile === undefined) throw new Error(`Cannot find tile associated with ${this.tileName}`);

        const actualRect = tile.getBoundingClientRect();
        const adjustedScale = new Point(actualRect.width, actualRect.height);
        const adjustedPosition = new Point(actualRect.x, actualRect.y + window.scrollY)
            .subtract(CategoryModel.instance().getCanvasPosition());

        return new Rectangle(adjustedPosition, adjustedScale);
    }

    /**
     * Remove the current goal
     */
    public removeGoal(): void {
        this.destroying = true;
        CategoryModel.instance().canvas.removeChild(this.goalTile);
    }

    private addTileEventListeners(): void {
        this.goalTile.onmousedown = (e: MouseEvent) => this.dragMouseDown(e);
        this.goalTile.ontouchstart = (e: TouchEvent) => this.handleTouchOnTile(e);
        this.goalTile.ondblclick = (e: UIEvent) => this.setToEditMode(e);
    }

    private nextTimeout: number | null = null;
    private lastTouchTime: number = 0;

    private handleTouchOnTile(e: TouchEvent): void {
        e.preventDefault();
        e.stopPropagation();
        const touchTime = new Date().getTime();
        if (this.nextTimeout === null || touchTime - this.lastTouchTime > 500) {
            //first touch, we schedule the drag action in 500ms
            this.lastTouchTime = touchTime;
            this.nextTimeout = setTimeout(() => {
                    this.dragMouseDown(e);
                    this.nextTimeout = null;
                },
                500);

            // if the user lifts the finger, we automatically un-schedule the drag action
            document.ontouchend = () => clearTimeout(this.nextTimeout!);

            // if the user starts moving, it is clear the desired action is to drag. 
            // We immediately initialize it and remove the scheduled action.
            document.ontouchmove = (e1) => {
                if (Point.extractPagePointFromUiEvent(e).distance(Point.extractPagePointFromUiEvent(e1)) < 15) return;
                clearTimeout(this.nextTimeout!);
                this.nextTimeout = null;
                this.dragMouseDown(e1);
            };
        } else {
            // second touch, we un-schedule the drag and trigger the edit mode.
            clearTimeout(this.nextTimeout);
            this.nextTimeout = null;
            document.ontouchend = null;
            document.ontouchmove = null;
            this.setToEditMode(e);
        }
    }

    private removeTileEventListeners(): void {
        this.goalTile.onmousedown = (e: MouseEvent) => e.stopPropagation();
        this.goalTile.ontouchstart = (e: TouchEvent) => e.stopPropagation();
        this.goalTile.ondblclick = (e: UIEvent) => e.stopImmediatePropagation();
    }

    private setToEditMode(e: UIEvent) {
        e.preventDefault();
        e.stopPropagation();
        this.goalTile.classList.remove("live");
        this.goalTile.classList.add("edit");
        this.redrawConnectedEdges();
        this.goalTitleEdit.focus();

        const setToLiveMode = (e1: UIEvent | undefined) => {
            if (e1 === undefined) return;
            if (!this.goalTile.contains((e1.target) as any) || e1 instanceof KeyboardEvent) {
                document.removeEventListener("mousedown", setToLiveMode);
                document.removeEventListener("touchstart", setToLiveMode);
                Settings.instance().singleTouch.unsubscribe(setToLiveMode);
                this.goalTitleEdit.onkeydown = null;
                this.goalDescriptionEdit.onkeydown = null;
                if (this.destroying) return; //the listener has been removed. We won't end up here anymore.
                this.addTileEventListeners();
                this.goalTile.classList.remove("edit");
                this.goalTile.classList.add("live");
                this.redrawConnectedEdges();
                this.updateIfValueChanged();
            }
        };

        const setToLiveOnEnter = (e1: KeyboardEvent) => {
            if (e1.keyCode === 13) {
                e1.stopPropagation();
                e1.preventDefault();
                setToLiveMode(e1);
                return false;
            }
            return true;
        };
        const setToLiveOnEnterShiftSensible = (e1: KeyboardEvent) => {
            if (e1.keyCode === 13 && !e1.shiftKey) {
                e1.stopPropagation();
                e1.preventDefault();
                setToLiveMode(e1);
                return false;
            }
            return true;
        };

        this.removeTileEventListeners();
        document.addEventListener("mousedown", setToLiveMode);
        document.addEventListener("touchstart", setToLiveMode);
        Settings.instance().singleTouch.subscribe(setToLiveMode);
        this.goalTitleEdit.addEventListener("keypress", setToLiveOnEnter);
        this.goalDescriptionEdit.addEventListener("keypress", setToLiveOnEnterShiftSensible);
    }

    private redrawConnectedEdges() {
        CategoryModel.instance().getEdgesConnectedWith(this.pk).forEach(edge => edge.redraw());
    }

    private updateIfValueChanged(): void {
        if (this.goalTitleEdit.value.trim() !== this.title ||
            this.goalDescriptionEdit.value.trim() !== this.description ||
            this.goalExperienceEdit.value !== String(this.experience) ||
            this.goalCompletedEdit.checked !== this.completed ||
            this.goalPinPassive.checked !== this.pinned) {
            this.updateGoal();
        }
    }

    private populateTile() {
        // visible fields
        this.goalTile.id = `goal-tile-${this.pk}`;
        this.goalTile.querySelector("div[name='goal-title']")!.innerHTML = this.title;
        this.goalTile.querySelector("div[name='goal-description']")!.innerHTML = this.description;
        this.goalTile.querySelector("div[name='goal-experience']")!.innerHTML = this.buildExpText();
        this.goalPinIcon.style.display = this.pinned ? "block" : "none";

        this.goalForm.id = `goal-edit-form-${this.pk}`;
        this.goalTitleEdit.value = this.title;
        this.goalDescriptionEdit.value = this.description;

        (this.goalTile.querySelector("#id_category") as HTMLInputElement).value =
            CategoryModel.instance().pk.toString();

        this.goalExperienceLabel.innerHTML = String(this.experience.level);
        this.goalExperiencePassive.value = String(Settings.instance().getExperiencePkForLevel(this.experience.level));
        this.goalExperienceEdit.value = String(this.experience.level);
        this.goalExperienceEdit.oninput = () => {
            const expLevel = Number(this.goalExperienceEdit.value);
            this.goalExperienceLabel.innerHTML = this.goalExperienceEdit.value;
            this.goalExperiencePassive.value = String(Settings.instance().getExperiencePkForLevel(expLevel));
        };

        this.goalPinPassive.checked = this.pinned;
        this.goalPinIcon.style.display = this.pinned ? "block" : "none";
        this.goalPinToggle.querySelector("svg")!.style.fill = this.pinned ? this.toggleColorOn() : this.toggleColorOff;

        this.goalCompletedEdit.checked = this.completed;
        this.goalCompletedEdit.id = `switch-${this.pk}`;
        (this.goalTile.querySelector(".toggle-label") as HTMLLabelElement).setAttribute("for",
            this.goalCompletedEdit.id);

        this.goalDeleteBt.onclick = (e: MouseEvent) => this.deleteGoal(e, this);
        this.goalPinToggle.onclick = () => this.togglePinnedGoal();

        this.redrawGoalTile();
    }

    private buildExpText(): string {
        const exp = this.experience;
        return `Lv: ${exp.level}, Exp: ${exp.expPoints} | <b>${exp.description}</b>`;
    }

    /**
     * This function can be called each time the tile position, canvas size or the canvas top-left point change.
     */
    public redrawGoalTile(): void {
        const relativePosition = Settings.instance().getRelativeToTheCanvasPositionFromAbsolute(this.position);

        this.goalTile.style.left = relativePosition.x + "px";
        this.goalTile.style.top = relativePosition.y + "px";
        this.goalTile.style.transform = `scale(${Settings.instance().zoom})`;
    }

    /**
     * Re-applies the colors to a goal tile and its text. Usually called when color settings are changed.
     */
    public repaintGoalTile(): void {
        const textColor = Settings.instance().goalsColor.text;
        this.goalTile.style.color = textColor;
        this.goalTitleEdit.style.color = textColor;
        this.goalTitleEdit.style.borderColor = textColor;
        this.goalDescriptionEdit.style.color = textColor;
        this.goalDescriptionEdit.style.borderColor = textColor;
        this.goalDeleteBt.querySelector("svg")!.style.fill = textColor;
        this.goalPinToggle.querySelector("svg")!.style.fill = this.pinned ? textColor : "#888888";
        this.goalPinIcon.querySelector("svg")!.style.fill = textColor;

        this.goalTile.style.background = this.getBackgroundColor();
        this.goalTile.style.textDecoration = this.completed ? "line-through" : "None";
    }

    private getBackgroundColor = () => this.completed
        ? Settings.instance().goalsColor.c1
        : Settings.instance().goalsColor.c3;

    private dragMouseDown(e: UIEvent): void {
        e = e || window.event;
        e.preventDefault();
        e.stopPropagation();

        this.dragInitial = Point.extractPagePointFromUiEvent(e);
        this.dragActual = this.dragInitial.clone();
        var connectedEdges = CategoryModel.instance().getEdgesConnectedWith(this.pk);
        var connectedButtons = CategoryModel.instance().getConnectedButtons(this.pk);
        CategoryModel.instance().goalStartsDragging(this);

        document.onmousemove = (e1: MouseEvent) => this.elementDrag(e1, connectedEdges, connectedButtons);
        document.ontouchmove = (e1: TouchEvent) => this.elementDrag(e1, connectedEdges, connectedButtons);
        document.onmouseup = () => this.closeDragElement();
        document.ontouchend = () => this.closeDragElement();
    }

    private elementDrag(e: UIEvent, connectedEdges: EdgeModel[], connectedButtons: IEdgeButton[]) {
        const p = Point.extractPagePointFromUiEvent(e);

        const actualX = this.goalTile.offsetLeft - this.dragActual.x + p.x;
        const actualY = this.goalTile.offsetTop - this.dragActual.y + p.y;

        this.goalTile.style.left = actualX + "px";
        this.goalTile.style.top = actualY + "px";
        connectedEdges.forEach(edge => edge.redraw());
        connectedButtons.forEach(button => button.redraw());

        this.onGoalMoved.trigger(this.dragActual.subtract(p));
        this.dragActual = p;
    }

    private closeDragElement(): void {
        document.onmouseup = null;
        document.ontouchend = null;
        document.onmousemove = null;
        document.ontouchmove = null;

        CategoryModel.instance().goalEndsDragging(this);
        if (this.dragInitial.equals(this.dragActual)) return;

        const x = Number(this.goalTile.style.left.replace("px", ""));
        const y = Number(this.goalTile.style.top.replace("px", ""));

        this.position = Settings.instance().getAbsolutePositionFromRelativeToTheCanvas(new Point(x, y));
        this.updateGoal();
    }

    /**
     * Function called when an ancestor of this goal stars being dragged and the moveGoalChildrenAlong option is enabled.
     * @param goalToMoveAlongWith The ancestor being moved.
     */
    public startMovingAlongWithOtherGoal(goalToMoveAlongWith: GoalTileModel): void {
        this.goalTile.classList.add("goal-tile-active");
        goalToMoveAlongWith.goalMoved.subscribe((p) => this.moveAlongWithOtherGoal(p));
    }

    /**
     * Function called when the ancestor has been dropped.
     * Used to save the current position.
     */
    public stopMovingAlongWithOtherGoal(): void {
        // the category already unsubscribed this goal.
        this.goalTile.classList.remove("goal-tile-active");
        const x = Number(this.goalTile.style.left.replace("px", ""));
        const y = Number(this.goalTile.style.top.replace("px", ""));
        this.position = Settings.instance().getAbsolutePositionFromRelativeToTheCanvas(new Point(x, y));
        this.updateGoal();
    }

    private moveAlongWithOtherGoal(deltaMovement: Point | undefined): void {
        if (!deltaMovement) return;

        const actualX = this.goalTile.offsetLeft - deltaMovement.x;
        const actualY = this.goalTile.offsetTop - deltaMovement.y;
        this.goalTile.style.left = actualX + "px";
        this.goalTile.style.top = actualY + "px";
    }

    /**
     * Unpins the current goal.
     * Function called by the SidePanel.
     */
    public unpin(): void {
        if (!this.pinned) return;
        this.goalPinPassive.checked = false;
        this.updateGoal();
    }

    private updateGoal(): void {
        const form = $(`#goal-edit-form-${this.pk}`);
        (this.goalTile.querySelector("#id_posX")! as HTMLInputElement).value = String(this.position.x);
        (this.goalTile.querySelector("#id_posY")! as HTMLInputElement).value = String(this.position.y);

        // unpinning if pinned and just set as completed
        if (!this.completed && this.goalCompletedEdit.checked && this.pinned)
        {
            this.goalPinPassive.checked = false;
        }

        const ajaxSettings: JQueryAjaxSettings = {
            url: `/update_goal/${this.pk}`,
            type: "POST",
            data: form.serialize(),
            success: (goal) => {
                const oldPinnedStatus = this.pinned;
                this.updateGoalInformation(goal);
                this.redrawConnectedEdges();
                CategoryModel.instance().repaintAllElements();
                if (this.pinned !== oldPinnedStatus) CategoryModel.instance().goalPinnedStatusUpdated();
                UserLevelTracker.instance().checkForLevelChanges();
            },
            error: (err) => {
                console.log(err);
                this.populateTile(); //this resets the input fields
            }
        };
        $.ajax(ajaxSettings);
    };

    private async deleteGoal(e: UIEvent, goal: GoalTileModel): Promise<void> {
        e.preventDefault();
        if (!await PopupDialog.confirm("Are you sure you want to delete this goal?")) return;

        const form = $(`#goal-edit-form-${goal.pk}`);
        $.ajax({
            type: "POST",
            url: `/delete_goal/${goal.pk}`,
            data: form.serialize(),
            success: () => CategoryModel.instance().removeGoal(goal),
            error: (error) => { console.log(error); }
        });
    }

    private togglePinnedGoal() {
        this.goalPinPassive.checked = !this.goalPinPassive.checked;
        const toggleOn = this.goalPinPassive.checked;

        this.goalPinIcon.style.display = toggleOn ? "block" : "none";
        this.goalPinToggle.querySelector("svg")!.style.fill = toggleOn ? this.toggleColorOn() : this.toggleColorOff;
    }
}