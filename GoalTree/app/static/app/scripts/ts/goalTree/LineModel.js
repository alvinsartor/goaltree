"use strict";
class LineModel {
    /**
     * Initializes a new instance of LineModel.
     * @param lineId the id of the new line.
     * @param container the line container, must be a SVG HTML element.
     * @param getScale the function used to fetch the current scale. It is used to scale down the arrow tip size.
     */
    constructor(lineId, container) {
        this.ptInit = null;
        this.ptEnd = null;
        this.container = container;
        this.line = this.createLine(lineId, container);
    }
    /**
     * Redraws the current line using the two given points.
     */
    redraw(from, to, scale) {
        this.ptInit = from;
        this.ptEnd = to;
        const points = this.getArrowPoints(from, to, scale);
        this.line.setAttribute("points", this.arrowPointsToHtmlPoints(points));
    }
    /**
     * Gets the line encapsulated in this model.
     */
    getLine() {
        return this.line;
    }
    /**
     * Removes this line from the container. After this step the model can be disposed of.
     */
    removeLine() {
        this.container.removeChild(this.line);
    }
    /**
     * Returns the beginning and end points of the line.
     */
    getPoints() {
        if (this.ptInit === null || this.ptEnd === null)
            throw Error("Trying to get points from non-initialized line.");
        return [this.ptInit, this.ptEnd];
    }
    /**
     * Returns the middle point of the line.
     */
    getMiddlePoint() {
        if (this.ptInit === null || this.ptEnd === null)
            throw Error("Trying to get points from non-initialized line.");
        return this.ptInit.sum(this.ptEnd).multiply(1 / 2);
    }
    /**
     * Function used to set a callback for the onMouseOver event.
     * @param callback the function that will be called when the event is triggered.
     */
    setMouseOverCallback(callback) {
        this.line.onmouseover = callback;
    }
    getArrowPoints(p1, p2, scale) {
        if (p1.equals(p2))
            return [p1, p2];
        const arrowHeight = 15 * scale;
        const arrowWidth = 4 * scale;
        const pU = p2.subtract(p1).normalize();
        const pV = new Point(-pU.y, pU.x);
        const v1 = p2.subtract(pU.multiply(arrowHeight).sum(pV.multiply(arrowWidth)));
        const v2 = p2.subtract(pU.multiply(arrowHeight).subtract(pV.multiply(arrowWidth)));
        const mid = v1.sum(v2).multiply(0.5);
        return [p1, mid, v1, p2, v2, mid];
    }
    arrowPointsToHtmlPoints(points) {
        const reducer = (accumulator, pt) => accumulator + `${pt.x},${pt.y} `;
        return points.reduce(reducer, " ");
    }
    createLine(name, container) {
        const svgNs = "http://www.w3.org/2000/svg";
        const line = document.createElementNS(svgNs, "polyline");
        if (line === null || line === undefined)
            throw new Error("It was not possible to create the line.");
        line.id = name;
        line.style.cssText = "stroke:black;stroke-width:2";
        container.appendChild(line);
        return line;
    }
}
