
interface IExperience {
    pk: number;
    fields: {
        level: number;
        exp_points: number;
        description: string;
    };
}

class Experience {
    public readonly level: number;
    public readonly expPoints: number;
    public readonly description: string;

    private constructor(level: number, expPoints: number, description: string) {
        this.level = level;
        this.expPoints = expPoints;
        this.description = description;
    }

    /**
     * Creates an Experience object starting from a IExperience, retrieved from the server.
     * @param exp The IExperience object retrieved from the server.
     */
    public static fromIExperience(exp: IExperience): Experience {
        return new Experience(exp.fields.level, exp.fields.exp_points, exp.fields.description);
    }
}