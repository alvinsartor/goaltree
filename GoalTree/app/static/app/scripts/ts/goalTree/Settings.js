"use strict";
class Settings {
    constructor() {
        this.isInitialized = false;
        // stored settings
        this.categoryPk = -1;
        this.goalColor = Color.default();
        this.canvasPan = Point.zero();
        this.canvasZoom = 1;
        this.areEdgeButtonsShown = false;
        this.areEdgeButtonsTemporarilyOff = false;
        this.moveAncestorsAlong = false;
        this.experienceLevelsContainer = {};
        this.experiencePksContainer = {};
        // variables used during runtime
        this.panInitialShift = Point.zero();
        this.panInitial = Point.zero();
        this.panActual = Point.zero();
        this.maxScale = 2;
        this.minScale = 0.15;
        this.initialPinchDistance = 0;
        // event subscriptions
        this.onPanChanged = new LiteEvent();
        this.onZoomChanged = new LiteEvent();
        this.onColorsChanged = new LiteEvent();
        this.onEdgeButtonsChanged = new LiteEvent();
        this.onDoubleTouch = new LiteEvent();
        this.onSingleTouch = new LiteEvent();
        this.nextTouchTimeout = null;
        this.lastTouchTime = 0;
        this.nextTimeout = null;
        this.body = document.getElementById("body-content");
        this.canvas = document.querySelector("#goal-canvas");
    }
    get goalsColor() { return this.goalColor; }
    get pan() { return this.canvasPan.clone(); }
    get zoom() { return this.canvasZoom; }
    get showEdgeButtons() { return this.areEdgeButtonsShown; }
    get ancestorsAreMovedAlong() { return this.moveAncestorsAlong; }
    set ancestorsAreMovedAlong(value) {
        this.moveAncestorsAlong = value;
        this.sendFormIfValueIsDefinitive();
    }
    get panChanged() { return this.onPanChanged.expose(); }
    get zoomChanged() { return this.onZoomChanged.expose(); }
    get colorsChanged() { return this.onColorsChanged.expose(); }
    get edgeButtonsChanged() { return this.onEdgeButtonsChanged.expose(); }
    get doubleTouch() { return this.onDoubleTouch.expose(); }
    get singleTouch() { return this.onSingleTouch.expose(); }
    /**
     * Gets the instance of Settings.
     */
    static instance() {
        if (!this.inst.isInitialized)
            throw Error("The settings have not been initialized yet");
        return this.inst;
    }
    /**
     * Initializes the instance of settings so it can be used by the application.
     * @param initialSettings
     */
    static initialize(initialSettings) {
        if (this.inst.isInitialized)
            throw Error("Settings have already been initialized.");
        this.inst.internalInitialize(initialSettings);
    }
    internalInitialize(initialSettings) {
        this.fillExperienceContainer(initialSettings.experienceLevels);
        this.categoryPk = Number(initialSettings.categoryPk);
        this.goalColor = Color.fromIColor(JSON.parse(initialSettings.goalsColor)[0]);
        this.canvasPan = new Point(Number(initialSettings.categoryPanX), Number(initialSettings.categoryPanY));
        this.canvasZoom = Number(initialSettings.categoryZoom);
        this.moveAncestorsAlong = Boolean(initialSettings.moveAncestorsAlong);
        this.attachEvents();
        this.isInitialized = true;
    }
    /** the current pan, in string format (for UI) */
    get currentPanString() {
        return this.canvasPan.toString();
    }
    /** the current zoom, in string format (for UI) */
    get currentZoomString() {
        return (Math.round(this.canvasZoom * 100) / 100).toString();
    }
    temporarilyDeactivateEdgeButtonsIfActive() {
        if (this.areEdgeButtonsShown && CategoryModel.instance().goalsCount > 100) {
            this.edgeButtonPressed();
            this.areEdgeButtonsTemporarilyOff = true;
        }
    }
    reactivateEdgeButtonsIfTemporarilyDeactivated() {
        if (this.areEdgeButtonsTemporarilyOff) {
            this.edgeButtonPressed();
            this.areEdgeButtonsTemporarilyOff = false;
        }
    }
    attachEvents() {
        this.canvas.onmousedown = (e) => this.startPan(e);
        this.canvas.ontouchstart = (e) => this.handleTouch(e);
        this.body.onwheel = e => this.handleWheel(e);
    }
    /**
     * Function called when the edge-mode button is pressed.
     */
    edgeButtonPressed() {
        this.areEdgeButtonsShown = !this.areEdgeButtonsShown;
        this.onEdgeButtonsChanged.trigger();
    }
    /**
     * Given a point relative to the canvas, it removes zoom and pan to obtain an absolute point.
     * @param relative the absolute point used for the calculation.
     */
    getAbsolutePositionFromRelativeToTheCanvas(relative) {
        return relative
            .sum(Settings.instance().pan)
            .multiply(1 / Settings.instance().zoom);
    }
    /**
     * Given an absolute point, it scales and positions it relatively to the canvas.
     * @param absolute the absolute point used for the calculation.
     */
    getRelativeToTheCanvasPositionFromAbsolute(absolute) {
        return absolute
            .multiply(Settings.instance().zoom)
            .subtract(Settings.instance().pan);
    }
    handleTouch(e) {
        e.preventDefault();
        e.stopPropagation();
        this.onSingleTouch.trigger(e);
        if (e.touches.length === 2) {
            this.startPinch(e);
            return;
        }
        const touchTime = new Date().getTime();
        if (this.nextTouchTimeout === null || touchTime - this.lastTouchTime > 500) {
            //first touch, we schedule the pan action in 500ms
            this.lastTouchTime = touchTime;
            this.nextTouchTimeout = setTimeout(() => {
                this.startPan(e);
                this.nextTouchTimeout = null;
            }, 500);
            // if the user lifts the finger, we automatically un-schedule the pan action
            document.ontouchend = () => clearTimeout(this.nextTouchTimeout);
            // if the user starts moving, it is clear the desired action is to pan. 
            // We immediately initialize it and remove the scheduled action.
            document.ontouchmove = (e1) => {
                if (Point.extractPagePointFromUiEvent(e).distance(Point.extractPagePointFromUiEvent(e1)) < 15)
                    return;
                clearTimeout(this.nextTouchTimeout);
                this.nextTouchTimeout = null;
                this.startPan(e1);
            };
        }
        else {
            // second touch, we un-schedule the pan and trigger the event.
            clearTimeout(this.nextTouchTimeout);
            this.nextTouchTimeout = null;
            document.ontouchend = null;
            document.ontouchmove = null;
            this.onDoubleTouch.trigger(e);
        }
    }
    /* #-#-# Pan Events #-#-# */
    startPan(e) {
        this.panInitial = Point.extractPagePointFromUiEvent(e);
        this.panInitialShift = this.canvasPan.clone();
        this.panActual = Point.zero();
        this.canvas.onmousemove = (ev) => this.continuePan(ev);
        this.canvas.ontouchmove = (ev) => this.continuePan(ev);
        this.canvas.onmouseup = (ev) => this.endPan(ev);
        this.canvas.ontouchend = (ev) => this.endPan(ev);
    }
    continuePan(e) {
        this.temporarilyDeactivateEdgeButtonsIfActive();
        this.panActual = this.panInitial.subtract(Point.extractPagePointFromUiEvent(e));
        this.canvasPan = this.panActual.sum(this.panInitialShift);
        this.onPanChanged.trigger();
    }
    endPan(e) {
        this.canvas.onmousemove = null;
        this.canvas.ontouchmove = null;
        this.canvas.onmouseup = null;
        this.canvas.ontouchend = null;
        this.reactivateEdgeButtonsIfTemporarilyDeactivated();
        const isTouchAction = e instanceof TouchEvent;
        // if there has been no shift, no need to save.
        // strangely, sometimes the values are set back to 0 when using touch devices.
        // single touches are not triggering any pan action anyway, so there is no unnecessary communication.
        if (!isTouchAction && this.panActual.equals(Point.zero()))
            return;
        this.save();
    }
    /**
    * Sets the categoryPan equal to the center of the specified goal.
    */
    centerOnGoal(goal) {
        const canvasRect = this.canvas.getBoundingClientRect();
        const canvasCenter = new Point(canvasRect.width / 2, canvasRect.height / 2);
        const goalCenter = goal.extractMeasurements().center;
        const delta = goalCenter.subtract(canvasCenter);
        this.canvasPan = this.canvasPan.sum(delta).truncate();
        this.onPanChanged.trigger();
        this.save();
    }
    /**
    * Sets the categoryPan to the default value [0,0].
    */
    resetCategoryPan() {
        this.canvasPan = Point.zero();
        this.onPanChanged.trigger();
        this.save();
    }
    /* #-#-# Scale Events #-#-# */
    handleWheel(e) {
        if (e.ctrlKey) {
            this.temporarilyDeactivateEdgeButtonsIfActive();
            const mouseRelativeToCanvas = this.getDistanceBetweenMouseAndCanvasOrigin(e);
            const absoluteMousePosition = this.getAbsolutePositionFromRelativeToTheCanvas(mouseRelativeToCanvas);
            e.deltaY < 0 ? this.increaseScale() : this.decreaseScale();
            const mouseRelativeToCanvasAfterResize = this.getRelativeToTheCanvasPositionFromAbsolute(absoluteMousePosition);
            const deltaDifference = mouseRelativeToCanvasAfterResize.subtract(mouseRelativeToCanvas);
            this.canvasPan = this.canvasPan.sum(deltaDifference).truncate();
            this.onPanChanged.trigger();
            this.onZoomChanged.trigger();
            this.sendFormIfValueIsDefinitive();
            return false;
        }
        return true;
    }
    /**
     * Increases the zoom level by a unit.
     */
    increaseZoom() {
        this.temporarilyDeactivateEdgeButtonsIfActive();
        const canvasCenter = this.getCanvasCenter();
        const absoluteCanvasCenter = this.getAbsolutePositionFromRelativeToTheCanvas(canvasCenter);
        this.increaseScale();
        const canvasCenterAfterResize = this.getRelativeToTheCanvasPositionFromAbsolute(absoluteCanvasCenter);
        const deltaDifference = canvasCenterAfterResize.subtract(canvasCenter);
        this.canvasPan = this.canvasPan.sum(deltaDifference).truncate();
        this.onPanChanged.trigger();
        this.onZoomChanged.trigger();
        this.sendFormIfValueIsDefinitive();
    }
    /**
     * Decreases the zoom level by a unit.
     */
    decreaseZoom() {
        this.temporarilyDeactivateEdgeButtonsIfActive();
        const canvasCenter = this.getCanvasCenter();
        const absoluteCanvasCenter = this.getAbsolutePositionFromRelativeToTheCanvas(canvasCenter);
        this.decreaseScale();
        const canvasCenterAfterResize = this.getRelativeToTheCanvasPositionFromAbsolute(absoluteCanvasCenter);
        const deltaDifference = canvasCenterAfterResize.subtract(canvasCenter);
        this.canvasPan = this.canvasPan.sum(deltaDifference).truncate();
        this.onPanChanged.trigger();
        this.onZoomChanged.trigger();
        this.sendFormIfValueIsDefinitive();
    }
    increaseScale() {
        const increase = (actual) => actual >= 1 ? actual * 1.1 : actual / 0.9;
        this.canvasZoom = Math.min(increase(this.canvasZoom), this.maxScale);
        this.canvasZoom = this.canvasZoom < 1 && this.canvasZoom > 0.92 ? 1 : this.canvasZoom; //fix approx. errors
    }
    decreaseScale() {
        const decrease = (actual) => actual <= 1 ? actual * 0.9 : actual / 1.1;
        this.canvasZoom = Math.max(decrease(this.canvasZoom), this.minScale);
        this.canvasZoom = this.canvasZoom > 1 && this.canvasZoom < 1.09 ? 1 : this.canvasZoom; //fix approx. errors
    }
    startPinch(e) {
        if (e.touches.length === 2) {
            this.initialPinchDistance = this.getTouchDistance(e);
            this.body.ontouchmove = (e1) => this.continuePinch(e1);
            this.body.ontouchend = () => this.endPinch();
        }
    }
    continuePinch(e) {
        this.temporarilyDeactivateEdgeButtonsIfActive();
        const pinchRelativeToCanvas = this.getCenterOfPinch(e);
        const absolutePinchPosition = this.getAbsolutePositionFromRelativeToTheCanvas(pinchRelativeToCanvas);
        const newDistance = this.getTouchDistance(e);
        const scale = this.canvasZoom * newDistance / this.initialPinchDistance;
        this.canvasZoom = scale < this.minScale ? this.minScale : scale > this.maxScale ? this.maxScale : scale;
        this.initialPinchDistance = newDistance;
        const pinchRelativeToCanvasAfterResize = this.getRelativeToTheCanvasPositionFromAbsolute(absolutePinchPosition);
        const deltaDifference = pinchRelativeToCanvasAfterResize.subtract(pinchRelativeToCanvas);
        this.canvasPan = this.canvasPan.sum(deltaDifference).truncate();
        this.onZoomChanged.trigger();
        this.onPanChanged.trigger();
        this.sendFormIfValueIsDefinitive();
    }
    endPinch() {
        this.temporarilyDeactivateEdgeButtonsIfActive();
        this.body.ontouchmove = null;
        this.body.ontouchend = null;
    }
    getTouchDistance(e) {
        return Math.hypot(e.touches[0].pageX - e.touches[1].pageX, e.touches[0].pageY - e.touches[1].pageY);
    }
    getDistanceBetweenMouseAndCanvasOrigin(e) {
        const mousePos = Point.extractPagePointFromUiEvent(e);
        const canvasRect = this.canvas.getBoundingClientRect();
        const canvasZero = new Point(canvasRect.x, canvasRect.y);
        return mousePos.subtract(canvasZero);
    }
    getCenterOfPinch(e) {
        const t1 = new Point(e.touches[0].pageX, e.touches[0].pageY);
        const t2 = new Point(e.touches[1].pageX, e.touches[1].pageY);
        const touchCenter = Point.center(t1, t2);
        const canvasRect = this.canvas.getBoundingClientRect();
        const canvasZero = new Point(canvasRect.x, canvasRect.y);
        return touchCenter.subtract(canvasZero);
    }
    sendFormIfValueIsDefinitive() {
        if (this.nextTimeout !== null)
            clearTimeout(this.nextTimeout);
        this.nextTimeout = setTimeout(() => {
            this.reactivateEdgeButtonsIfTemporarilyDeactivated();
            this.save();
        }, 500);
    }
    /**
    * Sets the canvas zoom to the default value (1).
    */
    resetCategoryZoom() {
        const canvasCenter = this.getCanvasCenter();
        const absoluteCanvasCenter = this.getAbsolutePositionFromRelativeToTheCanvas(canvasCenter);
        this.canvasZoom = 1;
        const canvasCenterAfterResize = this.getRelativeToTheCanvasPositionFromAbsolute(absoluteCanvasCenter);
        const deltaDifference = canvasCenterAfterResize.subtract(canvasCenter);
        this.canvasPan = this.canvasPan.sum(deltaDifference).truncate();
        this.onZoomChanged.trigger();
        this.onPanChanged.trigger();
        this.save();
    }
    getCanvasCenter() {
        const canvasRect = this.canvas.getBoundingClientRect();
        const canvasMiddle = new Point(canvasRect.width / 2, canvasRect.height / 2);
        const canvasZero = new Point(canvasRect.x, canvasRect.y);
        return canvasZero.sum(canvasMiddle).truncate();
    }
    save() {
        const form = this.fetchAndFillSettingsForm();
        $.ajax({
            type: "POST",
            url: `/update_category_settings/${this.categoryPk}`,
            data: form.serialize(),
            error: (error) => { console.log(error); }
        });
    }
    fetchAndFillSettingsForm() {
        const form = $("#category-settings-form");
        const root = form[0];
        root.querySelector("#id_panX").value = this.pan.x.toString();
        root.querySelector("#id_panY").value = this.pan.y.toString();
        root.querySelector("#id_scale").value = this.zoom.toString();
        root.querySelector("#id_goals_color").value = this.goalsColor.pk.toString();
        root.querySelector("#id_move_ancestors_along").value =
            this.moveAncestorsAlong.toString();
        return form;
    }
    /* #-#-# Colors #-#-# */
    /**
     * Function called whenever the color for the goals has been changed.
     * @param color the new color for the goal tiles.
     */
    changeGoalsColor(color) {
        if (this.goalColor.pk === color.pk)
            return;
        this.goalColor = color;
        this.onColorsChanged.trigger();
        this.sendFormIfValueIsDefinitive();
    }
    /* #-#-# Experience #-#-# */
    fillExperienceContainer(experienceLevels) {
        experienceLevels.forEach(e => {
            const exp = Experience.fromIExperience(e);
            this.experienceLevelsContainer[exp.level] = [e.pk, exp];
            this.experiencePksContainer[e.pk] = exp;
        });
    }
    /**
     * Given an experience level, it returns the corresponding database pk (so it can be properly _#_saved_#_).
     * @param expLevel the experience level to fetch for.
     */
    getExperiencePkForLevel(expLevel) {
        return this.experienceLevelsContainer[expLevel][0];
    }
    /**
     * Given an experience level, it returns the corresponding experience object (so it can be properly _#_displayed_#_).
     * @param expLevel the experience level to fetch for.
     */
    getExperienceForLevel(expLevel) {
        return this.experienceLevelsContainer[expLevel][1];
    }
    /**
     * Given a pk, it returns the experience it corresponds with.
     */
    getExperienceFromPk(pk) {
        return this.experiencePksContainer[pk];
    }
}
Settings.inst = new Settings();
