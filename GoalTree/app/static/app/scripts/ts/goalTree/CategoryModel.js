"use strict";
class CategoryModel {
    constructor() {
        this.categoryPk = -1;
        this.goalsContainer = {};
        this.edgesContainer = {};
        this.edgeButtonsContainer = {};
        this.temporaryGoalTileModel = null;
        this.isInitialized = false;
        this.ancestors = new Set();
        this.goalsCounter = 0;
        // event subscriptions
        this.onGoalPinned = new LiteEvent();
        this.canvas = document.querySelector("#goal-canvas");
        this.svgContainer = document.querySelector("#lines-container");
    }
    get goalPinned() { return this.onGoalPinned.expose(); }
    /**
     * Gets the instance of Settings.
     */
    static instance() {
        if (!this.inst.isInitialized)
            throw Error("The CategoryModel has not been initialized yet");
        return this.inst;
    }
    /**
     * Initializes the instance of settings so it can be used by the application.
     * @param canvasPk The primary key of the canvas model. Used to modify the DB reference.
     * @param goalsJson The goals, in JSON format.
     * @param edgesJson The edges, in JSON format.
     */
    static initialize(canvasPk, goalsJson, edgesJson) {
        if (this.inst.isInitialized)
            throw Error("CategoryModel has already been initialized.");
        this.inst.internalInitialize(canvasPk, goalsJson, edgesJson);
    }
    internalInitialize(canvasPk, goalsJson, edgesJson) {
        this.isInitialized = true;
        this.categoryPk = canvasPk;
        goalsJson.forEach(goal => this.parseGoal(goal));
        edgesJson.forEach(edge => this.parseEdge(edge));
        this.canvas.ondblclick = (e) => this.addTemporaryGoal(e);
        // handled with events as single tap is used in Settings and it is better to handle both there to avoid conflicts.
        Settings.instance().doubleTouch.subscribe((e) => this.addTemporaryGoal(e));
        Settings.instance().panChanged.subscribe(this.redrawAllElements.bind(this));
        Settings.instance().zoomChanged.subscribe(this.redrawAllElements.bind(this));
        Settings.instance().colorsChanged.subscribe(this.repaintAllElements.bind(this));
        Settings.instance().edgeButtonsChanged.subscribe(this.handleEdgeButtons.bind(this));
        this.redrawAllElements();
        this.repaintAllElements();
        if (this.goalsCounter > 100)
            MessageManager.info("This category is huge! 😱👏\n\nThe edge buttons will be temporarily disabled when zooming or dragging to avoid lag.");
    }
    /**
    * Gets the canvas position.
    */
    getCanvasPosition() {
        return new Point(this.canvas.offsetLeft, this.canvas.offsetTop);
    }
    /**
     * The category unique identifier.
     */
    get pk() {
        return this.categoryPk;
    }
    get goalsCount() {
        return this.goalsCounter;
    }
    /**
     * Gets the edges that connect the goal with the specified PK (entering and exiting connections).
     * @param pk the pk of the goal to retrieve the edges of.
     */
    getEdgesConnectedWith(pk) {
        return this.edges.filter(edge => edge.isConnectedTo(pk));
    }
    /**
     * Gets the edges that exit from the goal with the specified PK.
     * @param pk the pk of the goal to retrieve the edges of.
     */
    getEdgesExitingFrom(pk) {
        return this.edges.filter(edge => edge.isExitingFrom(pk));
    }
    /**
     * Gets the edges that point to the goal with the given PK.
     * @param pk the pk of the goal to retrieve entering edges of.
     */
    getEdgesArrivingTo(pk) {
        return this.edges.filter(edge => edge.isArrivingTo(pk));
    }
    /**
     * Gets the buttons somehow connected to the goal with the specified PK.
     * @param pk the pk of the goal to retrieve the buttons of.
     */
    getConnectedButtons(pk) {
        if (!Settings.instance().showEdgeButtons)
            return [];
        const connectedGoalButton = this.edgeButtonsContainer[`${AddEdgeBtModel.namePrefix}${pk}`];
        const connectedEdgeButtons = this.getEdgesConnectedWith(pk)
            .map(edge => this.edgeButtonsContainer[`${RemoveEdgeBtModel.namePrefix}${edge.name}`]);
        return connectedEdgeButtons.concat([connectedGoalButton]);
    }
    get edges() {
        return Object.keys(this.edgesContainer).map(key => this.edgesContainer[key]);
    }
    get goals() {
        return Object.keys(this.goalsContainer).map(goal => this.goalsContainer[Number(goal)]);
    }
    get edgeButtons() {
        return Object.keys(this.edgeButtonsContainer).map(bt => this.edgeButtonsContainer[bt]);
    }
    parseGoal(goal) {
        if (this.goalsContainer[goal.id])
            throw new Error(`Goal with id '${goal.id}' already exists.`);
        this.goalsContainer[goal.id] = new GoalTileModel(goal);
        this.goalsCounter++;
    }
    parseEdge(edge) {
        const split = edge.split("-");
        const pkFrom = Number(split[0]);
        const pkTo = Number(split[1]);
        if (!this.goalsContainer[pkFrom])
            throw new Error(`Error when creating edge '${edge}': no goal with id '${pkFrom}'`);
        if (!this.goalsContainer[pkTo])
            throw new Error(`Error when creating edge '${edge}': no goal with id '${pkTo}'`);
        this.addEdge(pkFrom, pkTo);
    }
    /**
     * Function to be called before of establishing a connection.
     * It runs a search to see if it would create a cycle.
     * @param pkFrom the starting point of the connection.
     * @param pkTo the arrival point of the connection.
     */
    checkForCycles(pkFrom, pkTo) {
        const expandFunction = (n) => this.getEdgesExitingFrom(n).map(g => g.goalTo.pk);
        return CycleFinder.connectionCreatesCycle(pkFrom, pkTo, expandFunction);
    }
    /**
     * Adds a new edge to the category. This function is called when the edge is added dynamically (using Ajax)
     * @param pkFrom the pk of the starting goal
     * @param pkTo the pk of the destination goal
     */
    addEdge(pkFrom, pkTo) {
        const edgeModel = new EdgeModel(this.goalsContainer[pkFrom], this.goalsContainer[pkTo]);
        if (this.edgesContainer[edgeModel.name])
            throw new Error(`Edge named '${edgeModel.name}' already exists.`);
        this.edgesContainer[edgeModel.name] = edgeModel;
        this.repaintAllElements();
        if (Settings.instance().showEdgeButtons)
            this.spawnRemoveEdgeButton(edgeModel);
    }
    /**
     * Removes an edge from the category. This function is called when the edge is removed dynamically (using Ajax)
     * @param edge the edge to remove.
     */
    removeEdge(edge) {
        if (!this.edgesContainer[edge.name])
            throw new Error("Trying to remove nonexistent edge.");
        this.edgesContainer[edge.name].removeLine();
        delete this.edgesContainer[edge.name];
        this.repaintAllElements();
    }
    addTemporaryGoal(e) {
        const clicked = Point.extractPagePointFromUiEvent(e).subtract(this.getCanvasPosition());
        this.temporaryGoalTileModel = TemporaryGoalTileModel.spawn(clicked);
    }
    /**
     * Adds a new goal to the category. This function is called when the goal is added dynamically (using Ajax)
     * @param goal the newly added goal.
     */
    addGoal(goal) {
        if (this.goalsContainer[goal.id])
            throw new Error(`Goal with id '${goal.id}' already exists.`);
        const goalModel = new GoalTileModel(goal);
        this.goalsContainer[goal.id] = goalModel;
        this.goalsCounter++;
        this.repaintAllElements();
        if (Settings.instance().showEdgeButtons)
            this.spawnAddEdgeButton(goalModel);
        if (goalModel.pinned)
            this.onGoalPinned.trigger();
        UserLevelTracker.instance().checkForLevelChanges();
    }
    /**
    * Removes the goal from the category. This function is called when the goal is removed dynamically (using Ajax)
    * @param goal the goal to remove.
    */
    removeGoal(goal) {
        if (!this.goalsContainer[goal.pk])
            throw new Error("Trying to remove nonexistent goal.");
        const connectedButtons = this.getConnectedButtons(goal.pk);
        connectedButtons.forEach(bt => this.removeEdgeButton(bt.name));
        const connectedEdges = this.getEdgesConnectedWith(goal.pk);
        connectedEdges.forEach(edge => this.removeEdge(edge));
        this.goalsContainer[goal.pk].removeGoal();
        delete this.goalsContainer[goal.pk];
        if (goal.pinned)
            this.onGoalPinned.trigger();
        this.goalsCounter--;
        this.repaintAllElements();
        UserLevelTracker.instance().checkForLevelChanges();
    }
    /**
     * Function called whenever a goal starts being dragged. If the descendants should be moved along,
     * it fetches them and connects the listeners, otherwise it just returns.
     * @param goal the goal being dragged.
     */
    goalStartsDragging(goal) {
        if (!Settings.instance().ancestorsAreMovedAlong)
            return;
        this.ancestors = new Set(this.findAncestors(goal));
        this.ancestors.forEach(d => d.startMovingAlongWithOtherGoal(goal));
        this.findAllEdgesForGoals(this.ancestors).forEach(e => goal.goalMoved.subscribe(() => e.redraw()));
        this.findAllButtonsForGoals(this.ancestors).forEach(b => goal.goalMoved.subscribe(() => b.redraw()));
    }
    /**
     * Function called whenever a goal is being dropped. It releases eventual listeners.
     * @param goal the goal being dropped.
     */
    goalEndsDragging(goal) {
        if (!Settings.instance().ancestorsAreMovedAlong)
            return;
        goal.goalMoved.unsubscribeAll();
        this.ancestors.forEach(d => d.stopMovingAlongWithOtherGoal());
    }
    findAncestors(goal) {
        const ancestors = this.getEdgesArrivingTo(goal.pk).map(edge => edge.goalFrom);
        if (ancestors.length === 0)
            return [];
        else {
            const descendants = ancestors.map(g => this.findAncestors(g));
            return ancestors.concat(...descendants);
        }
    }
    findAllEdgesForGoals(goals) {
        const edges = new Set();
        goals.forEach(g => { this.getEdgesConnectedWith(g.pk).forEach(edge => edges.add(edge)); });
        return edges;
    }
    findAllButtonsForGoals(goals) {
        const buttons = new Set();
        goals.forEach(g => { this.getConnectedButtons(g.pk).forEach(button => buttons.add(button)); });
        return buttons;
    }
    /**
     * Function called by a goal whose 'pinned' status has been toggled.
     */
    goalPinnedStatusUpdated() {
        this.onGoalPinned.trigger();
    }
    /**
     * The pinned goals in this category.
     */
    get pinnedGoals() {
        return this.goals.filter(g => g.pinned);
    }
    redrawAllElements() {
        this.goals.forEach(goal => goal.redrawGoalTile());
        this.edges.forEach(edge => edge.redraw());
        if (Settings.instance().showEdgeButtons)
            this.edgeButtons.forEach(bt => bt.redraw());
        if (this.temporaryGoalTileModel != null)
            this.temporaryGoalTileModel.redrawGoalTile();
    }
    repaintAllElements() {
        this.goals.forEach(goal => goal.repaintGoalTile());
    }
    handleEdgeButtons() {
        if (Settings.instance().showEdgeButtons) {
            this.spawnEdgeButtons();
        }
        else {
            this.destroyEdgeButtons();
        }
    }
    spawnEdgeButtons() {
        this.edges.forEach(this.spawnRemoveEdgeButton.bind(this));
        this.goals.forEach(this.spawnAddEdgeButton.bind(this));
    }
    spawnRemoveEdgeButton(edge) {
        const removeEdgeButton = new RemoveEdgeBtModel(edge);
        this.edgeButtonsContainer[removeEdgeButton.name] = removeEdgeButton;
    }
    spawnAddEdgeButton(goal) {
        const addEdgeButton = new AddEdgeBtModel(goal);
        this.edgeButtonsContainer[addEdgeButton.name] = addEdgeButton;
    }
    destroyEdgeButtons() {
        this.edgeButtons.forEach(bt => bt.destroy());
    }
    /**
     * Removes the reference to one of the edge buttons. Usually called by the same button.
     * @param buttonName the name of the button to remove.
     */
    removeEdgeButton(buttonName) {
        if (!this.edgeButtonsContainer[buttonName])
            throw new Error("Trying to remove nonexistent edge button.");
        this.edgeButtonsContainer[buttonName].destroy();
        delete this.edgeButtonsContainer[buttonName];
    }
    /**
     * Gets an array containing all the goals this goal can be connected to.
     * @param goalTile the goal to inquire the possible connections of.
     */
    getGoalsConnectableWith(goalTile) {
        const isAlreadyConnected = (g1, g2) => {
            const edgeName = EdgeModel.getLineName(g1.pk, g2.pk);
            return this.edgesContainer[edgeName] !== undefined;
        };
        // This is a pretty lightweight filter to ensure the client to run smoothly.
        // A cycle detection is going to be automatically triggered just before of creating the connection.
        return this.goals.filter(g => g.pk !== goalTile.pk &&
            !isAlreadyConnected(g, goalTile) &&
            !isAlreadyConnected(goalTile, g));
    }
}
CategoryModel.inst = new CategoryModel();
