class CategoryModel {
    private static inst = new CategoryModel();

    private categoryPk = -1;
    private goalsContainer: { [pk: number]: GoalTileModel } = {};
    private edgesContainer: { [name: string]: EdgeModel; } = {};
    private edgeButtonsContainer: { [name: string]: IEdgeButton; } = {};
    private temporaryGoalTileModel: TemporaryGoalTileModel | null = null;

    public readonly canvas: HTMLElement;
    public readonly svgContainer: SVGElement;
    private isInitialized = false;
    private ancestors: Set<GoalTileModel> = new Set();
    private goalsCounter: number = 0;

    // event subscriptions
    private readonly onGoalPinned = new LiteEvent<void>();

    public get goalPinned(): ILiteEvent<void> { return this.onGoalPinned.expose(); }

    private constructor() {
        this.canvas = document.querySelector("#goal-canvas") as HTMLCanvasElement;
        this.svgContainer = document.querySelector("#lines-container") as SVGElement;
    }

    /**
     * Gets the instance of Settings.
     */
    public static instance(): CategoryModel {
        if (!this.inst.isInitialized) throw Error("The CategoryModel has not been initialized yet");
        return this.inst;
    }

    /**
     * Initializes the instance of settings so it can be used by the application.
     * @param canvasPk The primary key of the canvas model. Used to modify the DB reference.
     * @param goalsJson The goals, in JSON format.
     * @param edgesJson The edges, in JSON format.
     */
    public static initialize(canvasPk: number, goalsJson: IGoalTileJson[], edgesJson: string[]): void {
        if (this.inst.isInitialized) throw Error("CategoryModel has already been initialized.");
        this.inst.internalInitialize(canvasPk, goalsJson, edgesJson);
    }

    private internalInitialize(canvasPk: number, goalsJson: IGoalTileJson[], edgesJson: string[]) {
        this.isInitialized = true;
        this.categoryPk = canvasPk;
        goalsJson.forEach(goal => this.parseGoal(goal));
        edgesJson.forEach(edge => this.parseEdge(edge));

        this.canvas.ondblclick = (e: UIEvent) => this.addTemporaryGoal(e);
        // handled with events as single tap is used in Settings and it is better to handle both there to avoid conflicts.
        Settings.instance().doubleTouch.subscribe((e) => this.addTemporaryGoal(e!));

        Settings.instance().panChanged.subscribe(this.redrawAllElements.bind(this));
        Settings.instance().zoomChanged.subscribe(this.redrawAllElements.bind(this));
        Settings.instance().colorsChanged.subscribe(this.repaintAllElements.bind(this));
        Settings.instance().edgeButtonsChanged.subscribe(this.handleEdgeButtons.bind(this));

        this.redrawAllElements();
        this.repaintAllElements();

        if (this.goalsCounter > 100)
            MessageManager.info("This category is huge! 😱👏\n\nThe edge buttons will be temporarily disabled when zooming or dragging to avoid lag.");
    }

    /**
    * Gets the canvas position.
    */
    public getCanvasPosition(): Point {
        return new Point(this.canvas.offsetLeft, this.canvas.offsetTop);
    }

    /**
     * The category unique identifier.
     */
    public get pk(): number {
        return this.categoryPk;
    }

    public get goalsCount():number {
        return this.goalsCounter;
    }

    /**
     * Gets the edges that connect the goal with the specified PK (entering and exiting connections).
     * @param pk the pk of the goal to retrieve the edges of.
     */
    public getEdgesConnectedWith(pk: number): EdgeModel[] {
        return this.edges.filter(edge => edge.isConnectedTo(pk));
    }

    /**
     * Gets the edges that exit from the goal with the specified PK.
     * @param pk the pk of the goal to retrieve the edges of.
     */
    public getEdgesExitingFrom(pk: number): EdgeModel[] {
        return this.edges.filter(edge => edge.isExitingFrom(pk));
    }

    /**
     * Gets the edges that point to the goal with the given PK.
     * @param pk the pk of the goal to retrieve entering edges of.
     */
    public getEdgesArrivingTo(pk: number): EdgeModel[]
    {
        return this.edges.filter(edge => edge.isArrivingTo(pk));
    }

    /**
     * Gets the buttons somehow connected to the goal with the specified PK.
     * @param pk the pk of the goal to retrieve the buttons of.
     */
    public getConnectedButtons(pk: number): IEdgeButton[] {
        if (!Settings.instance().showEdgeButtons) return [];

        const connectedGoalButton = this.edgeButtonsContainer[`${AddEdgeBtModel.namePrefix}${pk}`];
        const connectedEdgeButtons = this.getEdgesConnectedWith(pk)
            .map(edge => this.edgeButtonsContainer[`${RemoveEdgeBtModel.namePrefix}${edge.name}`]);

        return connectedEdgeButtons.concat([connectedGoalButton]);
    }

    private get edges(): EdgeModel[] {
        return Object.keys(this.edgesContainer).map(key => this.edgesContainer[key]);
    }

    private get goals(): GoalTileModel[] {
        return Object.keys(this.goalsContainer).map(goal => this.goalsContainer[Number(goal)]);
    }

    private get edgeButtons(): IEdgeButton[] {
        return Object.keys(this.edgeButtonsContainer).map(bt => this.edgeButtonsContainer[bt]);
    }

    private parseGoal(goal: IGoalTileJson) {
        if (this.goalsContainer[goal.id]) throw new Error(`Goal with id '${goal.id}' already exists.`);
        this.goalsContainer[goal.id] = new GoalTileModel(goal);
        this.goalsCounter++;
    }

    private parseEdge(edge: string): void {
        const split = edge.split("-");
        const pkFrom = Number(split[0]);
        const pkTo = Number(split[1]);

        if (!this.goalsContainer[pkFrom])
            throw new Error(`Error when creating edge '${edge}': no goal with id '${pkFrom}'`);
        if (!this.goalsContainer[pkTo])
            throw new Error(`Error when creating edge '${edge}': no goal with id '${pkTo}'`);

        this.addEdge(pkFrom, pkTo);
    }

    /**
     * Function to be called before of establishing a connection.
     * It runs a search to see if it would create a cycle.
     * @param pkFrom the starting point of the connection.
     * @param pkTo the arrival point of the connection.
     */
    public checkForCycles(pkFrom: number, pkTo: number): boolean {
        const expandFunction = (n: number) => this.getEdgesExitingFrom(n).map(g => g.goalTo.pk);
        return CycleFinder.connectionCreatesCycle(pkFrom, pkTo, expandFunction);
    }

    /**
     * Adds a new edge to the category. This function is called when the edge is added dynamically (using Ajax)
     * @param pkFrom the pk of the starting goal
     * @param pkTo the pk of the destination goal
     */
    public addEdge(pkFrom: number, pkTo: number): void {
        const edgeModel = new EdgeModel(this.goalsContainer[pkFrom], this.goalsContainer[pkTo]);
        if (this.edgesContainer[edgeModel.name]) throw new Error(`Edge named '${edgeModel.name}' already exists.`);
        this.edgesContainer[edgeModel.name] = edgeModel;

        this.repaintAllElements();
        if (Settings.instance().showEdgeButtons) this.spawnRemoveEdgeButton(edgeModel);
    }

    /**
     * Removes an edge from the category. This function is called when the edge is removed dynamically (using Ajax)
     * @param edge the edge to remove.
     */
    public removeEdge(edge: EdgeModel) {
        if (!this.edgesContainer[edge.name]) throw new Error("Trying to remove nonexistent edge.");

        this.edgesContainer[edge.name].removeLine();
        delete this.edgesContainer[edge.name];
        this.repaintAllElements();
    }

    private addTemporaryGoal(e: UIEvent) {
        const clicked = Point.extractPagePointFromUiEvent(e).subtract(this.getCanvasPosition());
        this.temporaryGoalTileModel = TemporaryGoalTileModel.spawn(clicked);
    }

    /**
     * Adds a new goal to the category. This function is called when the goal is added dynamically (using Ajax)
     * @param goal the newly added goal.
     */
    public addGoal(goal: IGoalTileJson) {
        if (this.goalsContainer[goal.id]) throw new Error(`Goal with id '${goal.id}' already exists.`);
        const goalModel = new GoalTileModel(goal);
        this.goalsContainer[goal.id] = goalModel;
        this.goalsCounter++;

        this.repaintAllElements();
        if (Settings.instance().showEdgeButtons) this.spawnAddEdgeButton(goalModel);
        if (goalModel.pinned) this.onGoalPinned.trigger();
        UserLevelTracker.instance().checkForLevelChanges();
    }

    /**
    * Removes the goal from the category. This function is called when the goal is removed dynamically (using Ajax)
    * @param goal the goal to remove.
    */
    public removeGoal(goal: GoalTileModel) {
        if (!this.goalsContainer[goal.pk])
            throw new Error("Trying to remove nonexistent goal.");

        const connectedButtons = this.getConnectedButtons(goal.pk);
        connectedButtons.forEach(bt => this.removeEdgeButton(bt.name));

        const connectedEdges = this.getEdgesConnectedWith(goal.pk);
        connectedEdges.forEach(edge => this.removeEdge(edge));

        this.goalsContainer[goal.pk].removeGoal();
        delete this.goalsContainer[goal.pk];
        if (goal.pinned) this.onGoalPinned.trigger();

        this.goalsCounter--;
        this.repaintAllElements();
        UserLevelTracker.instance().checkForLevelChanges();
    }

    /**
     * Function called whenever a goal starts being dragged. If the descendants should be moved along,
     * it fetches them and connects the listeners, otherwise it just returns.
     * @param goal the goal being dragged.
     */
    public goalStartsDragging(goal: GoalTileModel): void {
        if (!Settings.instance().ancestorsAreMovedAlong) return;

        this.ancestors = new Set(this.findAncestors(goal));
        this.ancestors.forEach(d => d.startMovingAlongWithOtherGoal(goal));

        this.findAllEdgesForGoals(this.ancestors).forEach(e => goal.goalMoved.subscribe(() => e.redraw()));
        this.findAllButtonsForGoals(this.ancestors).forEach(b => goal.goalMoved.subscribe(() => b.redraw()));
    }

    /**
     * Function called whenever a goal is being dropped. It releases eventual listeners.
     * @param goal the goal being dropped.
     */
    public goalEndsDragging(goal: GoalTileModel): void {
        if (!Settings.instance().ancestorsAreMovedAlong) return;
        goal.goalMoved.unsubscribeAll();
        this.ancestors.forEach(d => d.stopMovingAlongWithOtherGoal());
    }

    private findAncestors(goal: GoalTileModel): GoalTileModel[] {
        const ancestors = this.getEdgesArrivingTo(goal.pk).map(edge => edge.goalFrom);

        if (ancestors.length === 0) return [];
        else {
            const descendants = ancestors.map(g => this.findAncestors(g));
            return ancestors.concat(...descendants);
        }
    }

    private findAllEdgesForGoals(goals: Set<GoalTileModel>): Set<EdgeModel> {
        const edges = new Set<EdgeModel>();
        goals.forEach(g => { this.getEdgesConnectedWith(g.pk).forEach(edge => edges.add(edge)); });
        return edges;
    }

    private findAllButtonsForGoals(goals: Set<GoalTileModel>): Set<IEdgeButton> {
        const buttons = new Set<IEdgeButton>();
        goals.forEach(g => { this.getConnectedButtons(g.pk).forEach(button => buttons.add(button)); });
        return buttons;
    }

    /**
     * Function called by a goal whose 'pinned' status has been toggled.
     */
    public goalPinnedStatusUpdated() {
        this.onGoalPinned.trigger();
    }

    /**
     * The pinned goals in this category. 
     */
    public get pinnedGoals() {
        return this.goals.filter(g => g.pinned);
    }

    private redrawAllElements(): void {
        this.goals.forEach(goal => goal.redrawGoalTile());
        this.edges.forEach(edge => edge.redraw());
        if (Settings.instance().showEdgeButtons) this.edgeButtons.forEach(bt => bt.redraw());
        if (this.temporaryGoalTileModel != null) this.temporaryGoalTileModel.redrawGoalTile();
    }

    public repaintAllElements(): void {
        this.goals.forEach(goal => goal.repaintGoalTile());
    }

    private handleEdgeButtons() {
        if (Settings.instance().showEdgeButtons) {
            this.spawnEdgeButtons();
        } else {
            this.destroyEdgeButtons();
        }
    }

    private spawnEdgeButtons(): void {
        this.edges.forEach(this.spawnRemoveEdgeButton.bind(this));
        this.goals.forEach(this.spawnAddEdgeButton.bind(this));
    }

    private spawnRemoveEdgeButton(edge: EdgeModel) {
        const removeEdgeButton = new RemoveEdgeBtModel(edge);
        this.edgeButtonsContainer[removeEdgeButton.name] = removeEdgeButton;
    }

    private spawnAddEdgeButton(goal: GoalTileModel) {
        const addEdgeButton = new AddEdgeBtModel(goal);
        this.edgeButtonsContainer[addEdgeButton.name] = addEdgeButton;
    }

    private destroyEdgeButtons(): void {
        this.edgeButtons.forEach(bt => bt.destroy());
    }

    /**
     * Removes the reference to one of the edge buttons. Usually called by the same button.
     * @param buttonName the name of the button to remove.
     */
    public removeEdgeButton(buttonName: string) {
        if (!this.edgeButtonsContainer[buttonName]) throw new Error("Trying to remove nonexistent edge button.");

        this.edgeButtonsContainer[buttonName].destroy();
        delete this.edgeButtonsContainer[buttonName];
    }

    /**
     * Gets an array containing all the goals this goal can be connected to.
     * @param goalTile the goal to inquire the possible connections of.
     */
    public getGoalsConnectableWith(goalTile: GoalTileModel): GoalTileModel[] {
        const isAlreadyConnected = (g1: GoalTileModel, g2: GoalTileModel) : boolean =>
        {
            const edgeName = EdgeModel.getLineName(g1.pk, g2.pk);
            return this.edgesContainer[edgeName] !== undefined;
        }

        // This is a pretty lightweight filter to ensure the client to run smoothly.
        // A cycle detection is going to be automatically triggered just before of creating the connection.
        return this.goals.filter(g =>
            g.pk !== goalTile.pk &&
            !isAlreadyConnected(g, goalTile) &&
            !isAlreadyConnected(goalTile, g));
    }
}