
class TemporaryGoalTileModel {

    private readonly tileName = "goal-tile-Tmp";
    private position = Point.zero();

    private readonly goalTile: HTMLElement;
    private readonly goalForm: HTMLFormElement;
    private readonly goalTitleEdit: HTMLInputElement;
    private readonly goalDescriptionEdit: HTMLTextAreaElement;
    private readonly goalExperienceEdit: HTMLInputElement;
    private readonly goalExperiencePassive: HTMLInputElement;
    private readonly goalExperienceLabel: HTMLSpanElement;
    private readonly goalDeleteBt: HTMLButtonElement;
    private readonly goalPinToggle: HTMLButtonElement;
    private readonly goalPinPassive: HTMLInputElement;

    /*NOTE: the temporary tile does not need a reference to the activation toggle to work, this is used automatically.*/

    private readonly toggleColorOff = "#888888";
    private toggleColorOn = () => Settings.instance().goalsColor.text;

    /**
    * Initializes a new instance of GoalTileModel using an identifier and a json containing the necessary information.
    */
    constructor(clickedPoint: Point) {
        const template = (document.getElementById("goal-tile-template") as HTMLTemplateElement)
            .content.querySelector(".goal-tile.live");

        this.goalTile = document.importNode<any>(template, true);
        this.goalForm = this.goalTile.querySelector("form") as HTMLFormElement;
        this.goalTitleEdit = this.goalTile.querySelector(".goal-title-edit") as HTMLInputElement;
        this.goalDescriptionEdit = this.goalTile.querySelector(".goal-description-edit") as HTMLTextAreaElement;
        this.goalExperienceEdit = this.goalTile.querySelector("input[name='experience-range']") as HTMLInputElement;
        this.goalExperiencePassive = this.goalTile.querySelector("input[name='experience']") as HTMLInputElement;
        this.goalExperienceLabel = this.goalTile.querySelector(".range-result") as HTMLSpanElement;
        this.goalDeleteBt = this.goalTile.querySelector('button[class="delete-goal-bt"') as HTMLButtonElement;
        this.goalPinToggle = this.goalTile.querySelector('button[class="pin-goal-bt"') as HTMLButtonElement;
        this.goalPinPassive = this.goalTile.querySelector('input[name="pinned"') as HTMLInputElement;

        this.setupTileElements();
        this.placeAndStorePosition(clickedPoint);
        this.setToEditMode();

        CategoryModel.instance().canvas.appendChild(this.goalTile);
        this.goalTitleEdit.focus();
        this.goalTile.ondblclick = (e: UIEvent) => e.stopImmediatePropagation();
    }

    public static spawn(clickedPoint: Point): TemporaryGoalTileModel {
        return new TemporaryGoalTileModel(clickedPoint);
    }

    /**
    * Remove the current goal
    */
    public removeGoal(): void {
        CategoryModel.instance().canvas.removeChild(this.goalTile);
    }

    /**
    * This function can be called each time the tile position, canvas size or the canvas top-left point change.
    */
    public redrawGoalTile(): void
    {
        const relativePosition = Settings.instance().getRelativeToTheCanvasPositionFromAbsolute(this.position);

        this.goalTile.style.left = relativePosition.x + "px";
        this.goalTile.style.top = relativePosition.y + "px";
        this.goalTile.style.transform = `scale(${Settings.instance().zoom})`;
    }

    private setToEditMode() {
        this.goalTile.classList.remove("live");
        this.goalTile.classList.add("edit");

        const updateOrDestroy = (e1: UIEvent | undefined) => {
            if (e1 === undefined) return;
            if (!this.goalTile.contains((e1.target) as any) || e1 instanceof KeyboardEvent) {
                document.removeEventListener("mousedown", updateOrDestroy);
                document.removeEventListener("touchstart", updateOrDestroy);
                Settings.instance().singleTouch.unsubscribe(updateOrDestroy);
                this.goalTitleEdit.onkeydown = null;
                this.goalDescriptionEdit.onkeydown = null;
                if (this.goalTitleEdit.value.trim() !== "") {
                    this.createGoal();
                } else {
                    CategoryModel.instance().canvas.removeChild(this.goalTile);
                }
            }
        };

        const setToLiveOnEnter = (e1: KeyboardEvent) =>
        {
            if (e1.keyCode === 13)
            {
                e1.stopPropagation();
                e1.preventDefault();
                updateOrDestroy(e1);
                return false;
            }
            return true;
        };
        const setToLiveOnEnterShiftSensible = (e1: KeyboardEvent) =>
        {
            if (e1.keyCode === 13 && !e1.shiftKey)
            {
                e1.stopPropagation();
                e1.preventDefault();
                updateOrDestroy(e1);
                return false;
            }
            return true;
        }

        document.addEventListener("mousedown", updateOrDestroy);
        document.addEventListener("touchstart", updateOrDestroy);
        Settings.instance().singleTouch.subscribe(updateOrDestroy);
        this.goalTitleEdit.addEventListener("keypress", setToLiveOnEnter);
        this.goalDescriptionEdit.addEventListener("keypress", setToLiveOnEnterShiftSensible);
    }

    private setupTileElements() {
        this.goalTile.id = this.tileName;
        this.goalForm.id = `goal-edit-form-Tmp`;
        this.goalDeleteBt.style.display = "none";
        (this.goalTile.querySelector("#id_category") as HTMLInputElement).value = CategoryModel.instance().pk.toString();

        this.goalExperienceEdit.value = "1";
        this.goalExperienceLabel.innerHTML = "1";
        this.goalExperiencePassive.value = String(Settings.instance().getExperiencePkForLevel(1));
        this.goalExperienceEdit.oninput = () => {
            const expLevel = Number(this.goalExperienceEdit.value);
            this.goalExperienceLabel.innerHTML = this.goalExperienceEdit.value;
            this.goalExperiencePassive.value = String(Settings.instance().getExperiencePkForLevel(expLevel));
        };

        this.goalTile.onmousedown = (e: MouseEvent) => e.stopPropagation();
        this.goalPinPassive.checked = false;
        this.goalPinToggle.querySelector("svg")!.style.fill = this.toggleColorOff;
        this.goalPinToggle.onclick = () => this.togglePinnedGoal();

        const textColor = Settings.instance().goalsColor.text;
        this.goalTile.style.color = textColor;
        this.goalTitleEdit.style.color = textColor;
        this.goalTitleEdit.style.borderColor = textColor;
        this.goalDescriptionEdit.style.color = textColor;
        this.goalDescriptionEdit.style.borderColor = textColor;
        this.goalTile.style.background = Settings.instance().goalsColor.c3;
    }

    private togglePinnedGoal()
    {
        this.goalPinPassive.checked = !this.goalPinPassive.checked;
        this.goalPinToggle.querySelector("svg")!.style.fill = this.goalPinPassive.checked
            ? this.toggleColorOn()
            : this.toggleColorOff;
    }

    /**
     * This function can be called each time the tile position, canvas size or the canvas top-left point change.
     */
    public placeAndStorePosition(clickedPoint: Point): void {
        const actualRect = this.goalTile.getBoundingClientRect();
        const distanceFromCenter = new Point(actualRect.width, actualRect.height).multiply(1/2);
        const relativePosition = clickedPoint.subtract(distanceFromCenter);

        this.goalTile.style.left = relativePosition.x + "px";
        this.goalTile.style.top = relativePosition.y + "px";
        this.goalTile.style.transform = `scale(${Settings.instance().zoom})`;

        this.position = relativePosition
            .sum(Settings.instance().pan)
            .multiply(1 / Settings.instance().zoom);
    }

    private createGoal(): void {
        const form = $(`#goal-edit-form-Tmp`);
        (this.goalTile.querySelector("#id_posX")! as HTMLInputElement).value = String(this.position.x);
        (this.goalTile.querySelector("#id_posY")! as HTMLInputElement).value = String(this.position.y);

        const ajaxSettings: JQueryAjaxSettings = {
            url: `/create_goal/`,
            type: "POST",
            data: form.serialize(),
            success: (goal) => {
                CategoryModel.instance().addGoal(goal);
                CategoryModel.instance().canvas.removeChild(this.goalTile);
            },
            error: (err) => {
                console.log(err);
                this.setupTileElements(); //this resets the input fields
            }
        };
        $.ajax(ajaxSettings);
    };
}