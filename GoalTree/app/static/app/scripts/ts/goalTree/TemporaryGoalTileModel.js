"use strict";
class TemporaryGoalTileModel {
    /**
    * Initializes a new instance of GoalTileModel using an identifier and a json containing the necessary information.
    */
    constructor(clickedPoint) {
        this.tileName = "goal-tile-Tmp";
        this.position = Point.zero();
        /*NOTE: the temporary tile does not need a reference to the activation toggle to work, this is used automatically.*/
        this.toggleColorOff = "#888888";
        this.toggleColorOn = () => Settings.instance().goalsColor.text;
        const template = document.getElementById("goal-tile-template")
            .content.querySelector(".goal-tile.live");
        this.goalTile = document.importNode(template, true);
        this.goalForm = this.goalTile.querySelector("form");
        this.goalTitleEdit = this.goalTile.querySelector(".goal-title-edit");
        this.goalDescriptionEdit = this.goalTile.querySelector(".goal-description-edit");
        this.goalExperienceEdit = this.goalTile.querySelector("input[name='experience-range']");
        this.goalExperiencePassive = this.goalTile.querySelector("input[name='experience']");
        this.goalExperienceLabel = this.goalTile.querySelector(".range-result");
        this.goalDeleteBt = this.goalTile.querySelector('button[class="delete-goal-bt"');
        this.goalPinToggle = this.goalTile.querySelector('button[class="pin-goal-bt"');
        this.goalPinPassive = this.goalTile.querySelector('input[name="pinned"');
        this.setupTileElements();
        this.placeAndStorePosition(clickedPoint);
        this.setToEditMode();
        CategoryModel.instance().canvas.appendChild(this.goalTile);
        this.goalTitleEdit.focus();
        this.goalTile.ondblclick = (e) => e.stopImmediatePropagation();
    }
    static spawn(clickedPoint) {
        return new TemporaryGoalTileModel(clickedPoint);
    }
    /**
    * Remove the current goal
    */
    removeGoal() {
        CategoryModel.instance().canvas.removeChild(this.goalTile);
    }
    /**
    * This function can be called each time the tile position, canvas size or the canvas top-left point change.
    */
    redrawGoalTile() {
        const relativePosition = Settings.instance().getRelativeToTheCanvasPositionFromAbsolute(this.position);
        this.goalTile.style.left = relativePosition.x + "px";
        this.goalTile.style.top = relativePosition.y + "px";
        this.goalTile.style.transform = `scale(${Settings.instance().zoom})`;
    }
    setToEditMode() {
        this.goalTile.classList.remove("live");
        this.goalTile.classList.add("edit");
        const updateOrDestroy = (e1) => {
            if (e1 === undefined)
                return;
            if (!this.goalTile.contains((e1.target)) || e1 instanceof KeyboardEvent) {
                document.removeEventListener("mousedown", updateOrDestroy);
                document.removeEventListener("touchstart", updateOrDestroy);
                Settings.instance().singleTouch.unsubscribe(updateOrDestroy);
                this.goalTitleEdit.onkeydown = null;
                this.goalDescriptionEdit.onkeydown = null;
                if (this.goalTitleEdit.value.trim() !== "") {
                    this.createGoal();
                }
                else {
                    CategoryModel.instance().canvas.removeChild(this.goalTile);
                }
            }
        };
        const setToLiveOnEnter = (e1) => {
            if (e1.keyCode === 13) {
                e1.stopPropagation();
                e1.preventDefault();
                updateOrDestroy(e1);
                return false;
            }
            return true;
        };
        const setToLiveOnEnterShiftSensible = (e1) => {
            if (e1.keyCode === 13 && !e1.shiftKey) {
                e1.stopPropagation();
                e1.preventDefault();
                updateOrDestroy(e1);
                return false;
            }
            return true;
        };
        document.addEventListener("mousedown", updateOrDestroy);
        document.addEventListener("touchstart", updateOrDestroy);
        Settings.instance().singleTouch.subscribe(updateOrDestroy);
        this.goalTitleEdit.addEventListener("keypress", setToLiveOnEnter);
        this.goalDescriptionEdit.addEventListener("keypress", setToLiveOnEnterShiftSensible);
    }
    setupTileElements() {
        this.goalTile.id = this.tileName;
        this.goalForm.id = `goal-edit-form-Tmp`;
        this.goalDeleteBt.style.display = "none";
        this.goalTile.querySelector("#id_category").value = CategoryModel.instance().pk.toString();
        this.goalExperienceEdit.value = "1";
        this.goalExperienceLabel.innerHTML = "1";
        this.goalExperiencePassive.value = String(Settings.instance().getExperiencePkForLevel(1));
        this.goalExperienceEdit.oninput = () => {
            const expLevel = Number(this.goalExperienceEdit.value);
            this.goalExperienceLabel.innerHTML = this.goalExperienceEdit.value;
            this.goalExperiencePassive.value = String(Settings.instance().getExperiencePkForLevel(expLevel));
        };
        this.goalTile.onmousedown = (e) => e.stopPropagation();
        this.goalPinPassive.checked = false;
        this.goalPinToggle.querySelector("svg").style.fill = this.toggleColorOff;
        this.goalPinToggle.onclick = () => this.togglePinnedGoal();
        const textColor = Settings.instance().goalsColor.text;
        this.goalTile.style.color = textColor;
        this.goalTitleEdit.style.color = textColor;
        this.goalTitleEdit.style.borderColor = textColor;
        this.goalDescriptionEdit.style.color = textColor;
        this.goalDescriptionEdit.style.borderColor = textColor;
        this.goalTile.style.background = Settings.instance().goalsColor.c3;
    }
    togglePinnedGoal() {
        this.goalPinPassive.checked = !this.goalPinPassive.checked;
        this.goalPinToggle.querySelector("svg").style.fill = this.goalPinPassive.checked
            ? this.toggleColorOn()
            : this.toggleColorOff;
    }
    /**
     * This function can be called each time the tile position, canvas size or the canvas top-left point change.
     */
    placeAndStorePosition(clickedPoint) {
        const actualRect = this.goalTile.getBoundingClientRect();
        const distanceFromCenter = new Point(actualRect.width, actualRect.height).multiply(1 / 2);
        const relativePosition = clickedPoint.subtract(distanceFromCenter);
        this.goalTile.style.left = relativePosition.x + "px";
        this.goalTile.style.top = relativePosition.y + "px";
        this.goalTile.style.transform = `scale(${Settings.instance().zoom})`;
        this.position = relativePosition
            .sum(Settings.instance().pan)
            .multiply(1 / Settings.instance().zoom);
    }
    createGoal() {
        const form = $(`#goal-edit-form-Tmp`);
        this.goalTile.querySelector("#id_posX").value = String(this.position.x);
        this.goalTile.querySelector("#id_posY").value = String(this.position.y);
        const ajaxSettings = {
            url: `/create_goal/`,
            type: "POST",
            data: form.serialize(),
            success: (goal) => {
                CategoryModel.instance().addGoal(goal);
                CategoryModel.instance().canvas.removeChild(this.goalTile);
            },
            error: (err) => {
                console.log(err);
                this.setupTileElements(); //this resets the input fields
            }
        };
        $.ajax(ajaxSettings);
    }
    ;
}
