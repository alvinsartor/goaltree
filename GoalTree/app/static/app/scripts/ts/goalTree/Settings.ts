
interface ISettings {
    categoryPk: string;
    goalsColor: string;
    categoryPanX: string;
    categoryPanY: string;
    categoryZoom: string;
    moveAncestorsAlong: string;
    experienceLevels: IExperience[];
}

class Settings {
    private static inst = new Settings();
    private isInitialized = false;

    // stored settings
    private categoryPk = -1;
    private goalColor = Color.default();
    private canvasPan = Point.zero();
    private canvasZoom = 1;
    private areEdgeButtonsShown = false;
    private areEdgeButtonsTemporarilyOff = false;
    private moveAncestorsAlong = false;
    private experienceLevelsContainer: { [level: number]: [number, Experience]; } = {};
    private experiencePksContainer: { [pk: number]: Experience; } = {};

    public get goalsColor(): Color { return this.goalColor; }

    public get pan(): Point { return this.canvasPan.clone(); }

    public get zoom(): number { return this.canvasZoom; }

    public get showEdgeButtons(): boolean { return this.areEdgeButtonsShown; }

    public get ancestorsAreMovedAlong(): boolean { return this.moveAncestorsAlong; }

    public set ancestorsAreMovedAlong(value: boolean) {
        this.moveAncestorsAlong = value;
        this.sendFormIfValueIsDefinitive();
    }

    // view elements
    private readonly body: HTMLElement;
    private readonly canvas: HTMLCanvasElement;

    // variables used during runtime
    private panInitialShift = Point.zero();
    private panInitial = Point.zero();
    private panActual = Point.zero();
    private readonly maxScale = 2;
    private readonly minScale = 0.15;
    private initialPinchDistance = 0;

    // event subscriptions
    private readonly onPanChanged = new LiteEvent<void>();
    private readonly onZoomChanged = new LiteEvent<void>();
    private readonly onColorsChanged = new LiteEvent<void>();
    private readonly onEdgeButtonsChanged = new LiteEvent<void>();
    private readonly onDoubleTouch = new LiteEvent<UIEvent>();
    private readonly onSingleTouch = new LiteEvent<UIEvent>();

    public get panChanged(): ILiteEvent<void> { return this.onPanChanged.expose(); }

    public get zoomChanged(): ILiteEvent<void> { return this.onZoomChanged.expose(); }

    public get colorsChanged(): ILiteEvent<void> { return this.onColorsChanged.expose(); }

    public get edgeButtonsChanged(): ILiteEvent<void> { return this.onEdgeButtonsChanged.expose(); }

    public get doubleTouch(): ILiteEvent<UIEvent> { return this.onDoubleTouch.expose(); }

    public get singleTouch(): ILiteEvent<UIEvent> { return this.onSingleTouch.expose(); }

    private constructor() {
        this.body = document.getElementById("body-content") as HTMLElement;
        this.canvas = document.querySelector("#goal-canvas") as HTMLCanvasElement;
    }

    /**
     * Gets the instance of Settings.
     */
    public static instance() {
        if (!this.inst.isInitialized) throw Error("The settings have not been initialized yet");
        return this.inst;
    }

    /**
     * Initializes the instance of settings so it can be used by the application.
     * @param initialSettings
     */
    public static initialize(initialSettings: ISettings): void {
        if (this.inst.isInitialized) throw Error("Settings have already been initialized.");
        this.inst.internalInitialize(initialSettings);
    }

    private internalInitialize(initialSettings: ISettings) {
        this.fillExperienceContainer(initialSettings.experienceLevels);

        this.categoryPk = Number(initialSettings.categoryPk);
        this.goalColor = Color.fromIColor(JSON.parse(initialSettings.goalsColor)[0]);
        this.canvasPan = new Point(Number(initialSettings.categoryPanX), Number(initialSettings.categoryPanY));
        this.canvasZoom = Number(initialSettings.categoryZoom);
        this.moveAncestorsAlong = Boolean(initialSettings.moveAncestorsAlong);

        this.attachEvents();
        this.isInitialized = true;
    }

    /** the current pan, in string format (for UI) */
    public get currentPanString(): string {
        return this.canvasPan.toString();
    }

    /** the current zoom, in string format (for UI) */
    public get currentZoomString(): string {
        return (Math.round(this.canvasZoom * 100) / 100).toString();
    }

    private temporarilyDeactivateEdgeButtonsIfActive() {
        if (this.areEdgeButtonsShown  && CategoryModel.instance().goalsCount > 100) {
            this.edgeButtonPressed();
            this.areEdgeButtonsTemporarilyOff = true;
        }
    }

    private reactivateEdgeButtonsIfTemporarilyDeactivated() {
        if (this.areEdgeButtonsTemporarilyOff) {
            this.edgeButtonPressed();
            this.areEdgeButtonsTemporarilyOff = false;
        }
    }

    private attachEvents() {
        this.canvas.onmousedown = (e: MouseEvent) => this.startPan(e);
        this.canvas.ontouchstart = (e: TouchEvent) => this.handleTouch(e);
        this.body.onwheel = e => this.handleWheel(e);
    }

    /**
     * Function called when the edge-mode button is pressed.
     */
    public edgeButtonPressed() {
        this.areEdgeButtonsShown = !this.areEdgeButtonsShown;
        this.onEdgeButtonsChanged.trigger();
    }

    /**
     * Given a point relative to the canvas, it removes zoom and pan to obtain an absolute point.
     * @param relative the absolute point used for the calculation.
     */
    public getAbsolutePositionFromRelativeToTheCanvas(relative: Point): Point {
        return relative
            .sum(Settings.instance().pan)
            .multiply(1 / Settings.instance().zoom);
    }

    /**
     * Given an absolute point, it scales and positions it relatively to the canvas.
     * @param absolute the absolute point used for the calculation.
     */
    public getRelativeToTheCanvasPositionFromAbsolute(absolute: Point): Point {
        return absolute
            .multiply(Settings.instance().zoom)
            .subtract(Settings.instance().pan);
    }

    private nextTouchTimeout: number | null = null;
    private lastTouchTime: number = 0;

    private handleTouch(e: TouchEvent): void
    {
        e.preventDefault();
        e.stopPropagation();
        this.onSingleTouch.trigger(e);
        if (e.touches.length === 2) {
            this.startPinch(e);
            return;
        }

        const touchTime = new Date().getTime();
        if (this.nextTouchTimeout === null || touchTime - this.lastTouchTime > 500)
        {
            //first touch, we schedule the pan action in 500ms
            this.lastTouchTime = touchTime;
            this.nextTouchTimeout = setTimeout(() =>
            {
                this.startPan(e);
                this.nextTouchTimeout = null;
            }, 500);

            // if the user lifts the finger, we automatically un-schedule the pan action
            document.ontouchend = () => clearTimeout(this.nextTouchTimeout!);

            // if the user starts moving, it is clear the desired action is to pan. 
            // We immediately initialize it and remove the scheduled action.
            document.ontouchmove = (e1) =>
            {
                if (Point.extractPagePointFromUiEvent(e).distance(Point.extractPagePointFromUiEvent(e1)) < 15) return;
                clearTimeout(this.nextTouchTimeout!);
                this.nextTouchTimeout = null;
                this.startPan(e1);
            };
        } else
        {
            // second touch, we un-schedule the pan and trigger the event.
            clearTimeout(this.nextTouchTimeout);
            this.nextTouchTimeout = null;
            document.ontouchend = null;
            document.ontouchmove = null;
            this.onDoubleTouch.trigger(e);
        }
    }

    /* #-#-# Pan Events #-#-# */

    private startPan(e: UIEvent) {
        this.panInitial = Point.extractPagePointFromUiEvent(e);
        this.panInitialShift = this.canvasPan.clone();
        this.panActual = Point.zero();

        this.canvas.onmousemove = (ev: MouseEvent) => this.continuePan(ev);
        this.canvas.ontouchmove = (ev: TouchEvent) => this.continuePan(ev);
        this.canvas.onmouseup = (ev: MouseEvent) => this.endPan(ev);
        this.canvas.ontouchend = (ev: TouchEvent) => this.endPan(ev);
    }

    private continuePan(e: UIEvent)
    {
        this.temporarilyDeactivateEdgeButtonsIfActive();
        this.panActual = this.panInitial.subtract(Point.extractPagePointFromUiEvent(e));
        this.canvasPan = this.panActual.sum(this.panInitialShift);
        this.onPanChanged.trigger();
    }

    private endPan(e: UIEvent) {
        this.canvas.onmousemove = null;
        this.canvas.ontouchmove = null;
        this.canvas.onmouseup = null;
        this.canvas.ontouchend = null;

        this.reactivateEdgeButtonsIfTemporarilyDeactivated();
        const isTouchAction = e instanceof TouchEvent;
        // if there has been no shift, no need to save.
        // strangely, sometimes the values are set back to 0 when using touch devices.
        // single touches are not triggering any pan action anyway, so there is no unnecessary communication.
        if (!isTouchAction && this.panActual.equals(Point.zero())) return;
        this.save();
    }

    /**
    * Sets the categoryPan equal to the center of the specified goal.
    */
    public centerOnGoal(goal: GoalTileModel) {
        const canvasRect = this.canvas.getBoundingClientRect();
        const canvasCenter = new Point(canvasRect.width / 2, canvasRect.height / 2);
        const goalCenter = goal.extractMeasurements().center;

        const delta = goalCenter.subtract(canvasCenter);
        this.canvasPan = this.canvasPan.sum(delta).truncate();

        this.onPanChanged.trigger();
        this.save();
    }

    /**
    * Sets the categoryPan to the default value [0,0].
    */
    public resetCategoryPan(): void {
        this.canvasPan = Point.zero();

        this.onPanChanged.trigger();
        this.save();
    }

    /* #-#-# Scale Events #-#-# */

    private handleWheel(e: WheelEvent) {
        if (e.ctrlKey)
        {
            this.temporarilyDeactivateEdgeButtonsIfActive();
            const mouseRelativeToCanvas = this.getDistanceBetweenMouseAndCanvasOrigin(e);
            const absoluteMousePosition = this.getAbsolutePositionFromRelativeToTheCanvas(mouseRelativeToCanvas);

            e.deltaY < 0 ? this.increaseScale() : this.decreaseScale();

            const mouseRelativeToCanvasAfterResize =
                this.getRelativeToTheCanvasPositionFromAbsolute(absoluteMousePosition);
            const deltaDifference = mouseRelativeToCanvasAfterResize.subtract(mouseRelativeToCanvas);
            this.canvasPan = this.canvasPan.sum(deltaDifference).truncate();

            this.onPanChanged.trigger();
            this.onZoomChanged.trigger();
            this.sendFormIfValueIsDefinitive();
            return false;
        }
        return true;
    }

    /**
     * Increases the zoom level by a unit.
     */
    public increaseZoom(): void
    {
        this.temporarilyDeactivateEdgeButtonsIfActive();
        const canvasCenter = this.getCanvasCenter();
        const absoluteCanvasCenter = this.getAbsolutePositionFromRelativeToTheCanvas(canvasCenter);

        this.increaseScale();

        const canvasCenterAfterResize = this.getRelativeToTheCanvasPositionFromAbsolute(absoluteCanvasCenter);
        const deltaDifference = canvasCenterAfterResize.subtract(canvasCenter);
        this.canvasPan = this.canvasPan.sum(deltaDifference).truncate();

        this.onPanChanged.trigger();
        this.onZoomChanged.trigger();
        this.sendFormIfValueIsDefinitive();
    }

    /**
     * Decreases the zoom level by a unit.
     */
    public decreaseZoom(): void
    {
        this.temporarilyDeactivateEdgeButtonsIfActive();
        const canvasCenter = this.getCanvasCenter();
        const absoluteCanvasCenter = this.getAbsolutePositionFromRelativeToTheCanvas(canvasCenter);

        this.decreaseScale();

        const canvasCenterAfterResize = this.getRelativeToTheCanvasPositionFromAbsolute(absoluteCanvasCenter);
        const deltaDifference = canvasCenterAfterResize.subtract(canvasCenter);
        this.canvasPan = this.canvasPan.sum(deltaDifference).truncate();

        this.onPanChanged.trigger();
        this.onZoomChanged.trigger();
        this.sendFormIfValueIsDefinitive();
    }

    private increaseScale(): void {
        const increase = (actual: number) => actual >= 1 ? actual * 1.1 : actual / 0.9;
        this.canvasZoom = Math.min(increase(this.canvasZoom), this.maxScale);
        this.canvasZoom = this.canvasZoom < 1 && this.canvasZoom > 0.92 ? 1 : this.canvasZoom; //fix approx. errors
    }

    private decreaseScale(): void {
        const decrease = (actual: number) => actual <= 1 ? actual * 0.9 : actual / 1.1;
        this.canvasZoom = Math.max(decrease(this.canvasZoom), this.minScale);
        this.canvasZoom = this.canvasZoom > 1 && this.canvasZoom < 1.09 ? 1 : this.canvasZoom; //fix approx. errors
    }

    private startPinch(e: TouchEvent) {
        if (e.touches.length === 2) {
            this.initialPinchDistance = this.getTouchDistance(e);
            this.body.ontouchmove = (e1: TouchEvent) => this.continuePinch(e1);
            this.body.ontouchend = () => this.endPinch();
        }
    }

    private continuePinch(e: TouchEvent)
    {
        this.temporarilyDeactivateEdgeButtonsIfActive();
        const pinchRelativeToCanvas = this.getCenterOfPinch(e);
        const absolutePinchPosition = this.getAbsolutePositionFromRelativeToTheCanvas(pinchRelativeToCanvas);

        const newDistance = this.getTouchDistance(e);
        const scale = this.canvasZoom * newDistance / this.initialPinchDistance;
        this.canvasZoom = scale < this.minScale ? this.minScale : scale > this.maxScale ? this.maxScale : scale;
        this.initialPinchDistance = newDistance;

        const pinchRelativeToCanvasAfterResize = this.getRelativeToTheCanvasPositionFromAbsolute(absolutePinchPosition);
        const deltaDifference = pinchRelativeToCanvasAfterResize.subtract(pinchRelativeToCanvas);
        this.canvasPan = this.canvasPan.sum(deltaDifference).truncate();

        this.onZoomChanged.trigger();
        this.onPanChanged.trigger();
        this.sendFormIfValueIsDefinitive();
    }

    private endPinch()
    {
        this.temporarilyDeactivateEdgeButtonsIfActive();
        this.body.ontouchmove = null;
        this.body.ontouchend = null;
    }

    private getTouchDistance(e: TouchEvent): number {
        return Math.hypot(
            e.touches[0].pageX - e.touches[1].pageX,
            e.touches[0].pageY - e.touches[1].pageY);
    }

    private getDistanceBetweenMouseAndCanvasOrigin(e: WheelEvent): Point {
        const mousePos = Point.extractPagePointFromUiEvent(e);
        const canvasRect = this.canvas.getBoundingClientRect();
        const canvasZero = new Point(canvasRect.x, canvasRect.y);
        return mousePos.subtract(canvasZero);
    }

    private getCenterOfPinch(e: TouchEvent): Point {
        const t1 = new Point(e.touches[0].pageX, e.touches[0].pageY);
        const t2 = new Point(e.touches[1].pageX, e.touches[1].pageY);
        const touchCenter = Point.center(t1, t2);

        const canvasRect = this.canvas.getBoundingClientRect();
        const canvasZero = new Point(canvasRect.x, canvasRect.y);
        return touchCenter.subtract(canvasZero);
    }

    private nextTimeout: number | null = null;

    private sendFormIfValueIsDefinitive() {
        if (this.nextTimeout !== null) clearTimeout(this.nextTimeout);
        this.nextTimeout = setTimeout(() => {
            this.reactivateEdgeButtonsIfTemporarilyDeactivated();
            this.save();
        }, 500);
    }

    /**
    * Sets the canvas zoom to the default value (1).
    */
    public resetCategoryZoom(): void {
        const canvasCenter = this.getCanvasCenter();
        const absoluteCanvasCenter = this.getAbsolutePositionFromRelativeToTheCanvas(canvasCenter);

        this.canvasZoom = 1;

        const canvasCenterAfterResize = this.getRelativeToTheCanvasPositionFromAbsolute(absoluteCanvasCenter);
        const deltaDifference = canvasCenterAfterResize.subtract(canvasCenter);
        this.canvasPan = this.canvasPan.sum(deltaDifference).truncate();

        this.onZoomChanged.trigger();
        this.onPanChanged.trigger();
        this.save();
    }

    private getCanvasCenter(): Point {
        const canvasRect = this.canvas.getBoundingClientRect();
        const canvasMiddle = new Point(canvasRect.width / 2, canvasRect.height / 2);
        const canvasZero = new Point(canvasRect.x, canvasRect.y);
        return canvasZero.sum(canvasMiddle).truncate();
    }

    private save() {
        const form = this.fetchAndFillSettingsForm();
        $.ajax({
            type: "POST",
            url: `/update_category_settings/${this.categoryPk}`,
            data: form.serialize(),
            error: (error) => { console.log(error); }
        });
    }

    private fetchAndFillSettingsForm(): JQuery<HTMLElement> {
        const form = $("#category-settings-form");

        const root = form[0];
        (root.querySelector("#id_panX") as HTMLInputElement).value = this.pan.x.toString();
        (root.querySelector("#id_panY") as HTMLInputElement).value = this.pan.y.toString();
        (root.querySelector("#id_scale") as HTMLInputElement).value = this.zoom.toString();
        (root.querySelector("#id_goals_color") as HTMLInputElement).value = this.goalsColor.pk.toString();
        (root.querySelector("#id_move_ancestors_along") as HTMLInputElement).value =
            this.moveAncestorsAlong.toString();

        return form;
    }


    /* #-#-# Colors #-#-# */

    /**
     * Function called whenever the color for the goals has been changed.
     * @param color the new color for the goal tiles.
     */
    public changeGoalsColor(color: Color): void {
        if (this.goalColor.pk === color.pk) return;
        this.goalColor = color;
        this.onColorsChanged.trigger();
        this.sendFormIfValueIsDefinitive();
    }

    /* #-#-# Experience #-#-# */

    private fillExperienceContainer(experienceLevels: IExperience[]): void {
        experienceLevels.forEach(e => {
            const exp = Experience.fromIExperience(e);
            this.experienceLevelsContainer[exp.level] = [e.pk, exp];
            this.experiencePksContainer[e.pk] = exp;
        });
    }

    /**
     * Given an experience level, it returns the corresponding database pk (so it can be properly _#_saved_#_).
     * @param expLevel the experience level to fetch for.
     */
    public getExperiencePkForLevel(expLevel: number): number {
        return this.experienceLevelsContainer[expLevel][0];
    }

    /**
     * Given an experience level, it returns the corresponding experience object (so it can be properly _#_displayed_#_).
     * @param expLevel the experience level to fetch for.
     */
    public getExperienceForLevel(expLevel: number): Experience {
        return this.experienceLevelsContainer[expLevel][1];
    }

    /**
     * Given a pk, it returns the experience it corresponds with.
     */
    public getExperienceFromPk(pk: number): Experience {
        return this.experiencePksContainer[pk];
    }
}