"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
class GoalTileModel {
    /**
    * Initializes a new instance of GoalTileModel using an identifier and a json containing the necessary information.
    */
    constructor(goalTileJson) {
        this.destroying = false;
        this.toggleColorOff = "#888888";
        this.toggleColorOn = () => Settings.instance().goalsColor.text;
        //variables used for drag&drop
        this.dragInitial = Point.zero();
        this.dragActual = Point.zero();
        this.onGoalMoved = new LiteEvent();
        this.nextTimeout = null;
        this.lastTouchTime = 0;
        this.getBackgroundColor = () => this.completed
            ? Settings.instance().goalsColor.c1
            : Settings.instance().goalsColor.c3;
        this.tileName = GoalTileModel.getTileName(goalTileJson.id);
        this.pk = goalTileJson.id;
        this.title = goalTileJson.title;
        this.description = goalTileJson.description;
        this.experience = Settings.instance().getExperienceFromPk(goalTileJson.experience);
        this.completed = goalTileJson.completed;
        this.pinned = goalTileJson.pinned;
        this.position = new Point(goalTileJson.posX, goalTileJson.posY);
        const template = document.getElementById("goal-tile-template")
            .content.querySelector(".goal-tile.live");
        this.goalTile = document.importNode(template, true);
        this.goalForm = this.goalTile.querySelector("form");
        this.goalTitleEdit = this.goalTile.querySelector(".goal-title-edit");
        this.goalDescriptionEdit = this.goalTile.querySelector(".goal-description-edit");
        this.goalExperienceEdit = this.goalTile.querySelector("input[name='experience-range']");
        this.goalExperiencePassive = this.goalTile.querySelector("input[name='experience']");
        this.goalExperienceLabel = this.goalTile.querySelector(".range-result");
        this.goalCompletedEdit = this.goalTile.querySelector(".toggle-input");
        this.goalDeleteBt = this.goalTile.querySelector('button[class="delete-goal-bt"');
        this.goalPinToggle = this.goalTile.querySelector('button[class="pin-goal-bt"');
        this.goalPinIcon = this.goalTile.querySelector('div[class="pin-goal-bt"');
        this.goalPinPassive = this.goalTile.querySelector('input[name="pinned"');
        this.populateTile();
        this.addTileEventListeners();
        CategoryModel.instance().canvas.appendChild(this.goalTile);
    }
    get goalMoved() { return this.onGoalMoved.expose(); }
    /**
     * Function called when some information of the goal has been changed and it needs to be refreshed.
     * @param goalTileJson the JSON containing the new information.
     */
    updateGoalInformation(goalTileJson) {
        if (this.pk !== goalTileJson.id)
            throw Error("Are you trying to substitute this goal with another one? This is not possible");
        this.title = goalTileJson.title;
        this.description = goalTileJson.description;
        this.completed = goalTileJson.completed;
        this.position = new Point(goalTileJson.posX, goalTileJson.posY);
        this.experience = Settings.instance().getExperienceFromPk(goalTileJson.experience);
        this.pinned = goalTileJson.pinned;
        this.populateTile();
        this.repaintGoalTile();
    }
    /**
     * Given a goal pk, the function returns a tile name.
     * @param pk
     */
    static getTileName(pk) {
        return `goal-tile-${pk}`;
    }
    /**
     * Returns the measurements of the goal model tile.
     */
    extractMeasurements() {
        const tile = document.getElementById(this.tileName);
        if (tile === null || tile === undefined)
            throw new Error(`Cannot find tile associated with ${this.tileName}`);
        const actualRect = tile.getBoundingClientRect();
        const adjustedScale = new Point(actualRect.width, actualRect.height);
        const adjustedPosition = new Point(actualRect.x, actualRect.y + window.scrollY)
            .subtract(CategoryModel.instance().getCanvasPosition());
        return new Rectangle(adjustedPosition, adjustedScale);
    }
    /**
     * Remove the current goal
     */
    removeGoal() {
        this.destroying = true;
        CategoryModel.instance().canvas.removeChild(this.goalTile);
    }
    addTileEventListeners() {
        this.goalTile.onmousedown = (e) => this.dragMouseDown(e);
        this.goalTile.ontouchstart = (e) => this.handleTouchOnTile(e);
        this.goalTile.ondblclick = (e) => this.setToEditMode(e);
    }
    handleTouchOnTile(e) {
        e.preventDefault();
        e.stopPropagation();
        const touchTime = new Date().getTime();
        if (this.nextTimeout === null || touchTime - this.lastTouchTime > 500) {
            //first touch, we schedule the drag action in 500ms
            this.lastTouchTime = touchTime;
            this.nextTimeout = setTimeout(() => {
                this.dragMouseDown(e);
                this.nextTimeout = null;
            }, 500);
            // if the user lifts the finger, we automatically un-schedule the drag action
            document.ontouchend = () => clearTimeout(this.nextTimeout);
            // if the user starts moving, it is clear the desired action is to drag. 
            // We immediately initialize it and remove the scheduled action.
            document.ontouchmove = (e1) => {
                if (Point.extractPagePointFromUiEvent(e).distance(Point.extractPagePointFromUiEvent(e1)) < 15)
                    return;
                clearTimeout(this.nextTimeout);
                this.nextTimeout = null;
                this.dragMouseDown(e1);
            };
        }
        else {
            // second touch, we un-schedule the drag and trigger the edit mode.
            clearTimeout(this.nextTimeout);
            this.nextTimeout = null;
            document.ontouchend = null;
            document.ontouchmove = null;
            this.setToEditMode(e);
        }
    }
    removeTileEventListeners() {
        this.goalTile.onmousedown = (e) => e.stopPropagation();
        this.goalTile.ontouchstart = (e) => e.stopPropagation();
        this.goalTile.ondblclick = (e) => e.stopImmediatePropagation();
    }
    setToEditMode(e) {
        e.preventDefault();
        e.stopPropagation();
        this.goalTile.classList.remove("live");
        this.goalTile.classList.add("edit");
        this.redrawConnectedEdges();
        this.goalTitleEdit.focus();
        const setToLiveMode = (e1) => {
            if (e1 === undefined)
                return;
            if (!this.goalTile.contains((e1.target)) || e1 instanceof KeyboardEvent) {
                document.removeEventListener("mousedown", setToLiveMode);
                document.removeEventListener("touchstart", setToLiveMode);
                Settings.instance().singleTouch.unsubscribe(setToLiveMode);
                this.goalTitleEdit.onkeydown = null;
                this.goalDescriptionEdit.onkeydown = null;
                if (this.destroying)
                    return; //the listener has been removed. We won't end up here anymore.
                this.addTileEventListeners();
                this.goalTile.classList.remove("edit");
                this.goalTile.classList.add("live");
                this.redrawConnectedEdges();
                this.updateIfValueChanged();
            }
        };
        const setToLiveOnEnter = (e1) => {
            if (e1.keyCode === 13) {
                e1.stopPropagation();
                e1.preventDefault();
                setToLiveMode(e1);
                return false;
            }
            return true;
        };
        const setToLiveOnEnterShiftSensible = (e1) => {
            if (e1.keyCode === 13 && !e1.shiftKey) {
                e1.stopPropagation();
                e1.preventDefault();
                setToLiveMode(e1);
                return false;
            }
            return true;
        };
        this.removeTileEventListeners();
        document.addEventListener("mousedown", setToLiveMode);
        document.addEventListener("touchstart", setToLiveMode);
        Settings.instance().singleTouch.subscribe(setToLiveMode);
        this.goalTitleEdit.addEventListener("keypress", setToLiveOnEnter);
        this.goalDescriptionEdit.addEventListener("keypress", setToLiveOnEnterShiftSensible);
    }
    redrawConnectedEdges() {
        CategoryModel.instance().getEdgesConnectedWith(this.pk).forEach(edge => edge.redraw());
    }
    updateIfValueChanged() {
        if (this.goalTitleEdit.value.trim() !== this.title ||
            this.goalDescriptionEdit.value.trim() !== this.description ||
            this.goalExperienceEdit.value !== String(this.experience) ||
            this.goalCompletedEdit.checked !== this.completed ||
            this.goalPinPassive.checked !== this.pinned) {
            this.updateGoal();
        }
    }
    populateTile() {
        // visible fields
        this.goalTile.id = `goal-tile-${this.pk}`;
        this.goalTile.querySelector("div[name='goal-title']").innerHTML = this.title;
        this.goalTile.querySelector("div[name='goal-description']").innerHTML = this.description;
        this.goalTile.querySelector("div[name='goal-experience']").innerHTML = this.buildExpText();
        this.goalPinIcon.style.display = this.pinned ? "block" : "none";
        this.goalForm.id = `goal-edit-form-${this.pk}`;
        this.goalTitleEdit.value = this.title;
        this.goalDescriptionEdit.value = this.description;
        this.goalTile.querySelector("#id_category").value =
            CategoryModel.instance().pk.toString();
        this.goalExperienceLabel.innerHTML = String(this.experience.level);
        this.goalExperiencePassive.value = String(Settings.instance().getExperiencePkForLevel(this.experience.level));
        this.goalExperienceEdit.value = String(this.experience.level);
        this.goalExperienceEdit.oninput = () => {
            const expLevel = Number(this.goalExperienceEdit.value);
            this.goalExperienceLabel.innerHTML = this.goalExperienceEdit.value;
            this.goalExperiencePassive.value = String(Settings.instance().getExperiencePkForLevel(expLevel));
        };
        this.goalPinPassive.checked = this.pinned;
        this.goalPinIcon.style.display = this.pinned ? "block" : "none";
        this.goalPinToggle.querySelector("svg").style.fill = this.pinned ? this.toggleColorOn() : this.toggleColorOff;
        this.goalCompletedEdit.checked = this.completed;
        this.goalCompletedEdit.id = `switch-${this.pk}`;
        this.goalTile.querySelector(".toggle-label").setAttribute("for", this.goalCompletedEdit.id);
        this.goalDeleteBt.onclick = (e) => this.deleteGoal(e, this);
        this.goalPinToggle.onclick = () => this.togglePinnedGoal();
        this.redrawGoalTile();
    }
    buildExpText() {
        const exp = this.experience;
        return `Lv: ${exp.level}, Exp: ${exp.expPoints} | <b>${exp.description}</b>`;
    }
    /**
     * This function can be called each time the tile position, canvas size or the canvas top-left point change.
     */
    redrawGoalTile() {
        const relativePosition = Settings.instance().getRelativeToTheCanvasPositionFromAbsolute(this.position);
        this.goalTile.style.left = relativePosition.x + "px";
        this.goalTile.style.top = relativePosition.y + "px";
        this.goalTile.style.transform = `scale(${Settings.instance().zoom})`;
    }
    /**
     * Re-applies the colors to a goal tile and its text. Usually called when color settings are changed.
     */
    repaintGoalTile() {
        const textColor = Settings.instance().goalsColor.text;
        this.goalTile.style.color = textColor;
        this.goalTitleEdit.style.color = textColor;
        this.goalTitleEdit.style.borderColor = textColor;
        this.goalDescriptionEdit.style.color = textColor;
        this.goalDescriptionEdit.style.borderColor = textColor;
        this.goalDeleteBt.querySelector("svg").style.fill = textColor;
        this.goalPinToggle.querySelector("svg").style.fill = this.pinned ? textColor : "#888888";
        this.goalPinIcon.querySelector("svg").style.fill = textColor;
        this.goalTile.style.background = this.getBackgroundColor();
        this.goalTile.style.textDecoration = this.completed ? "line-through" : "None";
    }
    dragMouseDown(e) {
        e = e || window.event;
        e.preventDefault();
        e.stopPropagation();
        this.dragInitial = Point.extractPagePointFromUiEvent(e);
        this.dragActual = this.dragInitial.clone();
        var connectedEdges = CategoryModel.instance().getEdgesConnectedWith(this.pk);
        var connectedButtons = CategoryModel.instance().getConnectedButtons(this.pk);
        CategoryModel.instance().goalStartsDragging(this);
        document.onmousemove = (e1) => this.elementDrag(e1, connectedEdges, connectedButtons);
        document.ontouchmove = (e1) => this.elementDrag(e1, connectedEdges, connectedButtons);
        document.onmouseup = () => this.closeDragElement();
        document.ontouchend = () => this.closeDragElement();
    }
    elementDrag(e, connectedEdges, connectedButtons) {
        const p = Point.extractPagePointFromUiEvent(e);
        const actualX = this.goalTile.offsetLeft - this.dragActual.x + p.x;
        const actualY = this.goalTile.offsetTop - this.dragActual.y + p.y;
        this.goalTile.style.left = actualX + "px";
        this.goalTile.style.top = actualY + "px";
        connectedEdges.forEach(edge => edge.redraw());
        connectedButtons.forEach(button => button.redraw());
        this.onGoalMoved.trigger(this.dragActual.subtract(p));
        this.dragActual = p;
    }
    closeDragElement() {
        document.onmouseup = null;
        document.ontouchend = null;
        document.onmousemove = null;
        document.ontouchmove = null;
        CategoryModel.instance().goalEndsDragging(this);
        if (this.dragInitial.equals(this.dragActual))
            return;
        const x = Number(this.goalTile.style.left.replace("px", ""));
        const y = Number(this.goalTile.style.top.replace("px", ""));
        this.position = Settings.instance().getAbsolutePositionFromRelativeToTheCanvas(new Point(x, y));
        this.updateGoal();
    }
    /**
     * Function called when an ancestor of this goal stars being dragged and the moveGoalChildrenAlong option is enabled.
     * @param goalToMoveAlongWith The ancestor being moved.
     */
    startMovingAlongWithOtherGoal(goalToMoveAlongWith) {
        this.goalTile.classList.add("goal-tile-active");
        goalToMoveAlongWith.goalMoved.subscribe((p) => this.moveAlongWithOtherGoal(p));
    }
    /**
     * Function called when the ancestor has been dropped.
     * Used to save the current position.
     */
    stopMovingAlongWithOtherGoal() {
        // the category already unsubscribed this goal.
        this.goalTile.classList.remove("goal-tile-active");
        const x = Number(this.goalTile.style.left.replace("px", ""));
        const y = Number(this.goalTile.style.top.replace("px", ""));
        this.position = Settings.instance().getAbsolutePositionFromRelativeToTheCanvas(new Point(x, y));
        this.updateGoal();
    }
    moveAlongWithOtherGoal(deltaMovement) {
        if (!deltaMovement)
            return;
        const actualX = this.goalTile.offsetLeft - deltaMovement.x;
        const actualY = this.goalTile.offsetTop - deltaMovement.y;
        this.goalTile.style.left = actualX + "px";
        this.goalTile.style.top = actualY + "px";
    }
    /**
     * Unpins the current goal.
     * Function called by the SidePanel.
     */
    unpin() {
        if (!this.pinned)
            return;
        this.goalPinPassive.checked = false;
        this.updateGoal();
    }
    updateGoal() {
        const form = $(`#goal-edit-form-${this.pk}`);
        this.goalTile.querySelector("#id_posX").value = String(this.position.x);
        this.goalTile.querySelector("#id_posY").value = String(this.position.y);
        // unpinning if pinned and just set as completed
        if (!this.completed && this.goalCompletedEdit.checked && this.pinned) {
            this.goalPinPassive.checked = false;
        }
        const ajaxSettings = {
            url: `/update_goal/${this.pk}`,
            type: "POST",
            data: form.serialize(),
            success: (goal) => {
                const oldPinnedStatus = this.pinned;
                this.updateGoalInformation(goal);
                this.redrawConnectedEdges();
                CategoryModel.instance().repaintAllElements();
                if (this.pinned !== oldPinnedStatus)
                    CategoryModel.instance().goalPinnedStatusUpdated();
                UserLevelTracker.instance().checkForLevelChanges();
            },
            error: (err) => {
                console.log(err);
                this.populateTile(); //this resets the input fields
            }
        };
        $.ajax(ajaxSettings);
    }
    ;
    deleteGoal(e, goal) {
        return __awaiter(this, void 0, void 0, function* () {
            e.preventDefault();
            if (!(yield PopupDialog.confirm("Are you sure you want to delete this goal?")))
                return;
            const form = $(`#goal-edit-form-${goal.pk}`);
            $.ajax({
                type: "POST",
                url: `/delete_goal/${goal.pk}`,
                data: form.serialize(),
                success: () => CategoryModel.instance().removeGoal(goal),
                error: (error) => { console.log(error); }
            });
        });
    }
    togglePinnedGoal() {
        this.goalPinPassive.checked = !this.goalPinPassive.checked;
        const toggleOn = this.goalPinPassive.checked;
        this.goalPinIcon.style.display = toggleOn ? "block" : "none";
        this.goalPinToggle.querySelector("svg").style.fill = toggleOn ? this.toggleColorOn() : this.toggleColorOff;
    }
}
