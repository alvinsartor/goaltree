"use strict";
class AddEdgeBtModel {
    constructor(goal) {
        this.connectableGoals = {};
        this.connectableRects = {};
        this.edgeTileEnd = null;
        //variables used for drag&drop
        this.dragInitial = Point.zero();
        this.lineModel = null;
        this.name = `${AddEdgeBtModel.namePrefix}${goal.pk}`;
        this.edgeTileStart = goal;
        // create from template
        const template = document.getElementById("add-edge-template")
            .content.querySelector('button[name="add-edge-bt"]');
        this.button = document.importNode(template, true);
        CategoryModel.instance().canvas.appendChild(this.button);
        this.button.onmousedown = (e) => this.startEdgeCreation(e);
        this.button.ontouchstart = (e) => this.startEdgeCreation(e);
        this.redraw();
        setTimeout(() => this.button.classList.add("bt-active"), 10);
    }
    /** @inheritdoc */
    destroy() {
        this.button.classList.remove("bt-active");
        setTimeout(() => CategoryModel.instance().canvas.removeChild(this.button), 200);
    }
    /** @inheritdoc */
    redraw() {
        const btRect = this.button.getBoundingClientRect();
        const btSize = new Point(btRect.width, btRect.height).multiply(1 / 2);
        const position = this.edgeTileStart.extractMeasurements().center.subtract(btSize);
        this.button.style.left = position.x + "px";
        this.button.style.top = position.y + "px";
    }
    startEdgeCreation(e) {
        e.preventDefault();
        e.stopPropagation();
        this.dragInitial = this.edgeTileStart.extractMeasurements().center;
        this.lineModel = new LineModel("temporaryLine", CategoryModel.instance().svgContainer);
        this.edgeTileEnd = null;
        const connGoals = CategoryModel.instance().getGoalsConnectableWith(this.edgeTileStart);
        connGoals.forEach(g => {
            this.connectableGoals[g.pk] = g;
            this.connectableRects[g.pk] = g.extractMeasurements();
        });
        document.onmousemove = e1 => this.dragAction(e1);
        document.ontouchmove = e1 => this.dragAction(e1);
        document.onmouseup = () => this.stopArrowDragging();
        document.ontouchend = () => this.stopArrowDragging();
    }
    dragAction(e) {
        const currentPos = Point.extractPagePointFromUiEvent(e).subtract(CategoryModel.instance().getCanvasPosition());
        ;
        this.lineModel.redraw(this.dragInitial, currentPos, Settings.instance().zoom);
        if (this.edgeTileEnd === null) {
            const containingRectKey = Object.keys(this.connectableRects)
                .firstOrDefault(k => this.connectableRects[Number(k)].contains(currentPos));
            if (containingRectKey !== null) {
                // The user dragged inside a goal
                this.edgeTileEnd = this.connectableGoals[Number(containingRectKey)];
                this.edgeTileEnd.goalTile.classList.add("selected");
            }
        }
        else if (!this.connectableRects[this.edgeTileEnd.pk].contains(currentPos)) {
            // A goal was selected but now it exited
            this.edgeTileEnd.goalTile.classList.remove("selected");
            this.edgeTileEnd = null;
        }
    }
    stopArrowDragging() {
        document.onmouseup = null;
        document.ontouchend = null;
        document.onmousemove = null;
        document.ontouchmove = null;
        this.connectableRects = {};
        this.connectableGoals = {};
        this.createEdgeIfDraggedOnValidGoal();
        this.lineModel.removeLine();
        this.lineModel = null;
    }
    createEdgeIfDraggedOnValidGoal() {
        const start = this.edgeTileStart;
        const end = this.edgeTileEnd;
        if (!start || !end)
            return;
        if (CategoryModel.instance().checkForCycles(start.pk, end.pk)) {
            MessageManager.error(`Could not connect '${start.title}' to '${end.title}' as it would create a cycle.`);
            return;
        }
        const formData = new FormData();
        formData.append("csrfmiddlewaretoken", $("[name=csrfmiddlewaretoken]").val());
        $.ajax({
            type: "POST",
            url: `/create_edge/${start.pk}/${end.pk}`,
            data: (formData),
            processData: false,
            contentType: false,
            success: () => {
                CategoryModel.instance().addEdge(start.pk, end.pk);
                end.goalTile.classList.remove("selected");
            },
            error: (error) => { console.log(error); }
        });
    }
    ;
}
AddEdgeBtModel.namePrefix = "add-edge-button-";
