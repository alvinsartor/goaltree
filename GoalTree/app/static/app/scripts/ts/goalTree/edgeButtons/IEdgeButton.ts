
interface IEdgeButton {

    /** The edge button name. It encodes information about the goal(s) it is related to. */
    name: string;

    /** Function called to remove the button from the view. */
    destroy(): void;

    /** Redraws the button using the goals it is connected to as reference point. */
    redraw(): void;
}