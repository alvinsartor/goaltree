
class RemoveEdgeBtModel implements IEdgeButton {
    public static readonly namePrefix = "delete-edge-button-";

    /** @inheritdoc  */
    public readonly name: string;
    private readonly button: HTMLButtonElement;
    private readonly edge: EdgeModel;

    constructor(edge: EdgeModel) {
        this.name = `${RemoveEdgeBtModel.namePrefix}${edge.name}`;
        this.edge = edge;

        // create from template
        const template = (document.getElementById("delete-edge-template") as HTMLTemplateElement)
            .content.querySelector('button[name="del-edge-bt"]');
        this.button = document.importNode<any>(template, true);

        CategoryModel.instance().canvas.appendChild(this.button);
        this.button.onclick = (e: UIEvent) => {
            e.preventDefault();
            e.stopPropagation();

            this.deleteLine();
        };
        this.redraw();

        setTimeout(() => this.button.classList.add("bt-active"), 10);
    }

    /** @inheritdoc  */
    public destroy() {
        this.button.classList.remove("bt-active");
        setTimeout(() => CategoryModel.instance().canvas.removeChild(this.button), 200);
    }

    /** @inheritdoc  */
    public redraw(): void {
        const btRect = this.button.getBoundingClientRect();
        const btSize = new Point(btRect.width, btRect.height).multiply(1/2);
        const position = this.edge.getMiddlePoint().subtract(btSize);
        
        this.button.style.left = position.x + "px";
        this.button.style.top = position.y + "px";
    }

    private deleteLine() {
        const start = this.edge.goalFrom.pk;
        const end = this.edge.goalTo.pk;

        const formData = new FormData();
        formData.append("csrfmiddlewaretoken", $("[name=csrfmiddlewaretoken]").val() as string);

        $.ajax({
            type: "POST",
            url: `/delete_edge/${start}/${end}`,
            data: ((formData) as any) as JQuery.PlainObject,
            processData: false,
            contentType: false,
            success: () => {
                CategoryModel.instance().removeEdge(this.edge);
                CategoryModel.instance().removeEdgeButton(this.name);
            },
            error: (error) => { console.log(error); }
        });
    }
}