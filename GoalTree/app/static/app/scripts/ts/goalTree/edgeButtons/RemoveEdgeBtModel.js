"use strict";
class RemoveEdgeBtModel {
    constructor(edge) {
        this.name = `${RemoveEdgeBtModel.namePrefix}${edge.name}`;
        this.edge = edge;
        // create from template
        const template = document.getElementById("delete-edge-template")
            .content.querySelector('button[name="del-edge-bt"]');
        this.button = document.importNode(template, true);
        CategoryModel.instance().canvas.appendChild(this.button);
        this.button.onclick = (e) => {
            e.preventDefault();
            e.stopPropagation();
            this.deleteLine();
        };
        this.redraw();
        setTimeout(() => this.button.classList.add("bt-active"), 10);
    }
    /** @inheritdoc  */
    destroy() {
        this.button.classList.remove("bt-active");
        setTimeout(() => CategoryModel.instance().canvas.removeChild(this.button), 200);
    }
    /** @inheritdoc  */
    redraw() {
        const btRect = this.button.getBoundingClientRect();
        const btSize = new Point(btRect.width, btRect.height).multiply(1 / 2);
        const position = this.edge.getMiddlePoint().subtract(btSize);
        this.button.style.left = position.x + "px";
        this.button.style.top = position.y + "px";
    }
    deleteLine() {
        const start = this.edge.goalFrom.pk;
        const end = this.edge.goalTo.pk;
        const formData = new FormData();
        formData.append("csrfmiddlewaretoken", $("[name=csrfmiddlewaretoken]").val());
        $.ajax({
            type: "POST",
            url: `/delete_edge/${start}/${end}`,
            data: (formData),
            processData: false,
            contentType: false,
            success: () => {
                CategoryModel.instance().removeEdge(this.edge);
                CategoryModel.instance().removeEdgeButton(this.name);
            },
            error: (error) => { console.log(error); }
        });
    }
}
RemoveEdgeBtModel.namePrefix = "delete-edge-button-";
