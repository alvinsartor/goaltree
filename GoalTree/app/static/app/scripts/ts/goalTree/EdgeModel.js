"use strict";
class EdgeModel {
    /**
     * Instantiates a new instance of EdgeModel.
     * @param goalFrom The goal the edge is starting from.
     * @param goalTo The goal the edge is directed to.
     */
    constructor(goalFrom, goalTo) {
        this.goalFrom = goalFrom;
        this.goalTo = goalTo;
        this.name = EdgeModel.getLineName(goalFrom.pk, goalTo.pk);
        this.lineModel = new LineModel(this.name, CategoryModel.instance().svgContainer);
        this.redraw();
    }
    /**
     * Redraws the current edge.
     */
    redraw() {
        const measurementFrom = this.goalFrom.extractMeasurements();
        const measurementTo = this.goalTo.extractMeasurements();
        const p1 = this.getAnchorPoint(measurementFrom, measurementTo);
        const p2 = this.getAnchorPoint(measurementTo, measurementFrom);
        this.lineModel.redraw(p1, p2, Settings.instance().zoom);
    }
    /**
     * Removes this line from the container. After this step the model can be disposed of.
     */
    removeLine() {
        this.lineModel.removeLine();
    }
    /**
     * Defines whether this edge is connected to the goal with the specified pk.
     * @param pk The pk of the goal to check the connection to.
     */
    isConnectedTo(pk) {
        return this.isExitingFrom(pk) || this.isArrivingTo(pk);
    }
    /**
     * Returns true if this edge has pk as starting point.
     * @param pk The pk of the goal to check.
     */
    isExitingFrom(pk) {
        return this.goalFrom.pk === pk;
    }
    /**
     * Returns true if this edge has pk as ending point.
     * @param pk The pk of the goal to check.
     */
    isArrivingTo(pk) {
        return this.goalTo.pk === pk;
    }
    /**
     * Returns the beginning and end points of the line.
     */
    getPoints() {
        return this.lineModel.getPoints();
    }
    /**
     * Returns the middle point of the edge.
     */
    getMiddlePoint() {
        return this.lineModel.getMiddlePoint();
    }
    /**
    * Gets the line encapsulated in this model.
    */
    get line() {
        return this.lineModel.getLine();
    }
    /**
     * Gets the name of the line starting from 'pkFrom' and directed to 'pkTo'.
     * @param pkFrom The pk of the goal the edge is starting from.
     * @param pkTo The pk of the goal the edge is directed to.
     */
    static getLineName(pkFrom, pkTo) {
        return `edge-${pkFrom}-to-${pkTo}`;
    }
    getAnchorPoint(tileFrom, tileTo) {
        if (tileFrom.topLine > tileTo.bottomLine + tileFrom.size.y / 2) {
            return tileFrom.topAnchorPoint;
        }
        if (tileFrom.bottomLine < tileTo.bottomLine - tileFrom.size.y / 2) {
            return tileFrom.bottomAnchorPoint;
        }
        if (tileFrom.leftLine > tileTo.rightLine) {
            return tileFrom.leftAnchorPoint;
        }
        if (tileFrom.rightLine < tileTo.leftLine) {
            return tileFrom.rightAnchorPoint;
        }
        return tileFrom.center;
    }
}
