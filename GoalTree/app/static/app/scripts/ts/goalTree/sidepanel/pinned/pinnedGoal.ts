
class PinnedGoal {
    private readonly parent: SidePanelPinned;
    private readonly model: GoalTileModel;
    private readonly clone: HTMLDivElement;

    public constructor(parent: SidePanelPinned, model: GoalTileModel) {
        this.parent = parent;
        this.model = model;

        this.clone = PinnedGoal.extractClone(model);
        this.parent.pinnedGoalsContainer.appendChild(this.clone);

        this.clone.onclick = () => Settings.instance().centerOnGoal(this.model);

        const pinButton = this.clone.querySelector("div[class='pin-goal-bt']") as HTMLDivElement;
        pinButton.onclick = async (e: UIEvent) => {
            e.stopPropagation();
            if (await PopupDialog.confirm("Are you sure you want to unpin this goal?"))
            {
                this.model.unpin();
            }
        }
    }

    private static extractClone(model: GoalTileModel): HTMLDivElement {
        const clone = model.goalTile.cloneNode(true) as HTMLDivElement;

        clone.classList.remove("live");
        clone.classList.remove("edit");
        clone.classList.replace("goal-tile", "cloned-goal-tile");

        const cloneEditDiv = clone.querySelector(".goal-tile-edit") as HTMLDivElement;
        clone.removeChild(cloneEditDiv);

        clone.style.transform = "scale(1)";

        return clone;
    }

    public destroy() {
        this.parent.pinnedGoalsContainer.removeChild(this.clone);
    }
}