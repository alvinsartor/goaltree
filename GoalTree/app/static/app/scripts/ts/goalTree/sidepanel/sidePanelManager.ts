
class SidePanelManager {

    private readonly sidePanelsConstructors: { new(manager: SidePanelManager, menu: HTMLDivElement): SidePanel; }[] = [
        SidePanelControl,
        SidePanelJournal,
        SidePanelPinned,
    ];

    public readonly categoryPk: number;
    private readonly sidePanelDragButton: HTMLButtonElement;
    private readonly sidePanelContainer: HTMLDivElement;
    private readonly sideMenuTemplate: HTMLTemplateElement;
    private readonly sidePanels: SidePanel[] = [];
    private activePanel: SidePanel | null = null;

    private readonly minLeftPositionValue = -280;
    private readonly maxLeftPositionValue = 10;
    private initialDrag = Point.zero();

    /**
     * Creates a new instance of SidePanelManager
     */
    public constructor(categoryPk: number) {
        this.categoryPk = categoryPk;
        this.sidePanelContainer = document.querySelector(".side-panel") as HTMLDivElement;
        this.sideMenuTemplate = document.querySelector("#side-panel-menu-template") as HTMLTemplateElement;
        this.sidePanelDragButton = document.querySelector(".side-panel-drag-button") as HTMLButtonElement;

        this.sidePanelsConstructors.forEach(m => this.createPanelFromTemplate(m));

        // show first, pre-load the other panels for quicker speedup when they are selected
        this.sidePanels[0].open();
        this.sidePanels.slice(1).forEach(p => p.preLoadContentIfNecessary());

        window.addEventListener("resize", () => { if (this.activePanel !== null) this.activePanel.resetHeight(); });
        this.addDragButtonListeners();
    }

    private createPanelFromTemplate(menuConstructor: {
        new(manager: SidePanelManager, menu: HTMLDivElement): SidePanel;
    }) {
        const template = this.sideMenuTemplate.content.querySelector("div[name='side-menu']");
        const menu = document.importNode<any>(template, true) as HTMLDivElement;
        this.sidePanelContainer.appendChild(menu);

        const sideMenu = new menuConstructor(this, menu);
        this.sidePanels.push(sideMenu);
    }

    /**
     * Function called when a panel has opened. It will shut down any other open panels.
     * @param panel the panel that has just been opened.
     */
    public panelHasActivated(panel: SidePanel) {
        if (this.activePanel != null) this.activePanel.close();
        this.activePanel = panel;
    }

    /**
     * Calculates the total available space to be filled by the side panels.
     */
    public getAvailableSpace(): string {
        const total = this.sidePanelContainer.getBoundingClientRect().height;
        const padding = Number(window.getComputedStyle(this.sidePanelContainer).padding.replace("px", "")) * 2;
        const accordions = this.sidePanels.map(p => p.getAccordionHeight())
            .reduce((accumulator, next) => accumulator + next, 0);

        return `${total - padding - accordions}px`;
    }

    private addDragButtonListeners(): void {
        this.sidePanelDragButton.ontouchstart = (e) => this.startDrag(e);
        this.sidePanelDragButton.ontouchend = (e) => this.endDrag(e);
    }

    private startDrag(e: TouchEvent)
    {
        e.preventDefault();
        e.stopPropagation();
        this.initialDrag = Point.extractPagePointFromUiEvent(e);
        this.sidePanelDragButton.ontouchmove = (e1) => this.dragMenuPanel(e1);
    }

    private dragMenuPanel(e: TouchEvent)
    {
        e.preventDefault();
        e.stopPropagation();
        const actualPosition = Point.extractPagePointFromUiEvent(e);
        const currentRight = this.getCurrentRightValue();

        let actualX = currentRight + (this.initialDrag.x - actualPosition.x) * 2;
        actualX = actualX < this.minLeftPositionValue
            ? this.minLeftPositionValue
            : actualX > this.maxLeftPositionValue
            ? this.maxLeftPositionValue
            : actualX;

        this.sidePanelContainer.style.right = actualX + "px";
        this.initialDrag = actualPosition;
    }

    private endDrag(e: TouchEvent)
    {
        e.preventDefault();
        e.stopPropagation();
        this.sidePanelDragButton.ontouchmove = null;
    }

    private getCurrentRightValue() {
        let right = this.sidePanelContainer.style.right.replace("px", "");
        if (right === "") // it has not been assigned yet. Let's take it from the css
            right = getComputedStyle(this.sidePanelContainer).right;

        return Number(right.replace("px", ""));
    }
}