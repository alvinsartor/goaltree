"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
class SidePanelJournal extends SidePanel {
    /** @inheritdoc  */
    constructor(manager, menu) {
        super(manager, menu);
        this.loadedEntries = [];
        this.entries = {};
        const template = document.getElementById("journal-template")
            .content.querySelector("div[name='journal']");
        this.journal = document.importNode(template, true);
        this.entriesContainer = this.journal.querySelector("div[name='journal-entries-container']");
        this.addEntryButton = this.journal.querySelector('button[name="add-entry-bt"]');
        this.noElementsMessage = this.journal.querySelector("div[class='no-elements-message']");
        this.menuPanel.appendChild(this.journal);
        this.addEntryButton.onclick = () => {
            JournalEntry.createEmpty(this);
            this.addEntryButton.disabled = true;
            this.noElementsMessage.style.display = "none";
        };
    }
    /** @inheritdoc  */
    name() { return "Journal"; }
    ;
    /** @inheritdoc  */
    loadContent() {
        return __awaiter(this, void 0, void 0, function* () {
            this.loadedEntries = yield $.getJSON(`/journal/${this.manager.categoryPk}`);
        });
    }
    /** @inheritdoc  */
    displayContent() {
        this.loadedEntries.forEach(e => {
            const entry = JournalEntry.createFromDb(this, e);
            this.entries[e.pk] = entry;
        });
        this.noElementsMessage.style.display = Object.keys(this.entries).length === 0 ? "block" : "none";
    }
    /** @inheritdoc  */
    removeContent() {
        Object.keys(this.entries).forEach(k => this.entries[Number(k)].destroy());
        this.entries = {};
    }
    /**
     * Removes the given entry from the DOM and the collection.
     */
    removeEntry(pk) {
        if (!this.entries[pk])
            throw new Error("Trying to remove nonexistent entry.");
        this.entries[pk].destroy();
        delete this.entries[pk];
        this.loadedEntries = this.loadedEntries.filter(e => e.pk !== pk);
        this.noElementsMessage.style.display = Object.keys(this.entries).length === 0 ? "block" : "none";
    }
    /**
     * Function called by a recently added entry.
     * @param rawEntry the raw data (used to recreate it).
     * @param entryReference the reference (used to destroy the entry).
     */
    addNewEntry(rawEntry, entryReference) {
        this.loadedEntries.push(rawEntry);
        this.entries[rawEntry.pk] = entryReference;
        this.addEntryButton.disabled = false;
        this.noElementsMessage.style.display = Object.keys(this.entries).length === 0 ? "block" : "none";
    }
    creationHasBeenAborted() {
        this.addEntryButton.disabled = false;
        this.noElementsMessage.style.display = Object.keys(this.entries).length === 0 ? "block" : "none";
    }
    /**
     * Function called by a recently updated entry.
     * @param rawEntry the raw data corresponding to the new entry.
     */
    updateEntry(rawEntry) {
        this.loadedEntries = this.loadedEntries.filter(e => e.pk !== rawEntry.pk);
        this.loadedEntries.push(rawEntry);
        this.loadedEntries = this.loadedEntries.sort((e1, e2) => e1.date - e2.date);
    }
}
