"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
class JournalEntry {
    constructor(journalPanel, journalEntry = null) {
        if (journalEntry === null) {
            this.pk = 0;
            this.text = "";
            this.date = new Date();
            this.isExistingEntry = false;
        }
        else {
            this.pk = journalEntry.pk;
            this.text = journalEntry.text;
            this.date = new Date(journalEntry.date * 1000);
            this.isExistingEntry = true;
        }
        this.isLiveMode = this.isExistingEntry;
        this.journalPanel = journalPanel;
        if (JournalEntry.entryTemplate === null) {
            JournalEntry.entryTemplate = document.getElementById("journal-entry-template");
        }
        const template = JournalEntry.entryTemplate.content.querySelector("div[name='journal-entry']");
        this.entryElement = document.importNode(template, true);
        this.journalPanel.entriesContainer.appendChild(this.entryElement);
        this.entryLiveText = this.entryElement.querySelector("div[name='journal-entry-text']");
        this.entryDate = this.entryElement.querySelector("label[class='journal-entry-date']");
        this.entryEdit = this.entryElement.querySelector("label[name='editAction']");
        this.entryDelete = this.entryElement.querySelector("label[name='deleteAction']");
        this.entryEditText = this.entryElement.querySelector("textarea");
        this.liveContainer = this.entryElement.querySelector("div[name='live']");
        this.editContainer = this.entryElement.querySelector("div[name='edit']");
        this.entrySave = this.entryElement.querySelector("label[name='saveAction']");
        this.entryCancel = this.entryElement.querySelector("label[name='cancelAction']");
        this.setFieldsAndListeners();
    }
    /**
    * Creates a new instance of JournalEntry from an existing entry.
    * @param journalPanel the panel holding the entries.
    * @param journalEntry the existing entry, fetched from the DB.
    */
    static createFromDb(journalPanel, journalEntry) {
        return new JournalEntry(journalPanel, journalEntry);
    }
    /**
    * Creates a new instance of JournalEntry ready to be filled.
    * @param journalPanel the panel holding the entries.
    */
    static createEmpty(journalPanel) {
        return new JournalEntry(journalPanel);
    }
    /**
     * Removes the entry node from its container.
     */
    destroy() {
        this.journalPanel.entriesContainer.removeChild(this.entryElement);
    }
    setFieldsAndListeners() {
        if (this.isLiveMode) {
            this.entryLiveText.innerHTML = this.text;
            this.entryDate.textContent = this.date.toLocaleString();
            this.liveContainer.style.display = "block";
            this.editContainer.style.display = "none";
            this.setLiveListeners();
        }
        else {
            this.entryEditText.value = this.text;
            this.editContainer.style.display = "block";
            this.liveContainer.style.display = "none";
            this.setEditListeners();
            this.entryEditText.focus();
        }
    }
    setLiveListeners() {
        this.entryDelete.onclick = () => __awaiter(this, void 0, void 0, function* () {
            if (yield PopupDialog.confirm("Are you sure you want to delete this entry?")) {
                this.deleteThisEntry();
            }
        });
        this.entryEdit.onclick = () => {
            this.isLiveMode = false;
            this.setFieldsAndListeners();
        };
        this.entrySave.onclick = null;
        this.entryCancel.onclick = null;
    }
    setEditListeners() {
        this.entryDelete.onclick = null;
        this.entryEdit.onclick = null;
        this.entrySave.onclick = () => this.isExistingEntry ? this.setNewTextForThisEntry() : this.sendAndCreateEntry();
        this.entryCancel.onclick = () => {
            if (this.isExistingEntry) {
                this.isLiveMode = true;
                this.setFieldsAndListeners();
            }
            else {
                this.destroy();
                this.journalPanel.creationHasBeenAborted();
            }
        };
    }
    sendAndCreateEntry() {
        this.entrySave.onclick = null;
        const text = this.entryEditText.value;
        if (text === "") {
            this.journalPanel.creationHasBeenAborted();
            this.destroy();
        }
        const formData = new FormData();
        formData.append("csrfmiddlewaretoken", document.getElementsByName("csrfmiddlewaretoken")[0].value);
        formData.append("text", text);
        formData.append("category", String(this.journalPanel.categoryPk));
        $.ajax({
            type: "POST",
            url: `/create_journal_entry/`,
            data: (formData),
            processData: false,
            contentType: false,
            success: (entry) => {
                this.pk = entry.pk;
                this.text = entry.text;
                this.isExistingEntry = true;
                this.date = new Date(entry.date * 1000);
                this.isLiveMode = true;
                this.setFieldsAndListeners();
                this.journalPanel.addNewEntry(entry, this);
            },
            error: (error) => {
                console.log(error);
                this.journalPanel.creationHasBeenAborted();
                this.destroy();
            }
        });
    }
    setNewTextForThisEntry() {
        this.entrySave.onclick = null;
        const text = this.entryEditText.value;
        if (text === null || text === "" || text === this.text) {
            this.isLiveMode = true;
            this.setFieldsAndListeners();
        }
        const formData = new FormData();
        formData.append("csrfmiddlewaretoken", document.getElementsByName("csrfmiddlewaretoken")[0].value);
        formData.append("text", text);
        formData.append("category", String(this.journalPanel.categoryPk));
        $.ajax({
            type: "POST",
            url: `/edit_journal_entry/${this.pk}`,
            data: (formData),
            processData: false,
            contentType: false,
            success: (entry) => {
                this.text = entry.text;
                this.isLiveMode = true;
                this.setFieldsAndListeners();
                this.journalPanel.updateEntry(entry);
            },
            error: (error) => {
                console.log(error);
                this.isLiveMode = true;
                this.setFieldsAndListeners();
            }
        });
    }
    deleteThisEntry() {
        const formData = new FormData();
        formData.append("csrfmiddlewaretoken", document.getElementsByName("csrfmiddlewaretoken")[0].value);
        $.ajax({
            type: "POST",
            url: `/delete_journal_entry/${this.pk}`,
            data: (formData),
            processData: false,
            contentType: false,
            success: () => this.journalPanel.removeEntry(this.pk),
            error: (error) => { console.log(error); }
        });
    }
}
JournalEntry.entryTemplate = null;
