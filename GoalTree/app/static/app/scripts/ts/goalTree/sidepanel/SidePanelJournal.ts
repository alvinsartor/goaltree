
class SidePanelJournal extends SidePanel {
    private loadedEntries: IJournalEntry[] = [];
    private entries: { [pk: number]: JournalEntry} = {};

    private readonly journal: HTMLDivElement;
    public readonly entriesContainer: HTMLDivElement;
    public readonly noElementsMessage: HTMLDivElement;
    private readonly addEntryButton: HTMLButtonElement;

    /** @inheritdoc  */
    public constructor(manager: SidePanelManager, menu: HTMLDivElement) {
        super(manager, menu);

        const template = (document.getElementById("journal-template") as HTMLTemplateElement)
            .content.querySelector("div[name='journal']");

        this.journal = document.importNode<any>(template, true) as HTMLDivElement;
        this.entriesContainer = this.journal.querySelector("div[name='journal-entries-container']") as HTMLDivElement;
        this.addEntryButton = this.journal.querySelector('button[name="add-entry-bt"]') as HTMLButtonElement;
        this.noElementsMessage = this.journal.querySelector("div[class='no-elements-message']") as HTMLDivElement;

        this.menuPanel.appendChild(this.journal);
        this.addEntryButton.onclick = () => {
            JournalEntry.createEmpty(this);
            this.addEntryButton.disabled = true;
            this.noElementsMessage.style.display = "none";
        };
    }

    /** @inheritdoc  */
    protected name() { return "Journal"; };

    /** @inheritdoc  */
    protected async loadContent(): Promise<void> {
        this.loadedEntries = await $.getJSON(`/journal/${this.manager.categoryPk}`);
    }

    /** @inheritdoc  */
    protected displayContent(): void {
        this.loadedEntries.forEach(e =>
        {
            const entry = JournalEntry.createFromDb(this, e);
            this.entries[e.pk] = entry;
        });
        this.noElementsMessage.style.display = Object.keys(this.entries).length === 0 ? "block" : "none";
    }

    /** @inheritdoc  */
    protected removeContent(): void {
        Object.keys(this.entries).forEach(k => this.entries[Number(k)].destroy());
        this.entries = {};
    }

    /**
     * Removes the given entry from the DOM and the collection.
     */
    public removeEntry(pk: number): void {
        if (!this.entries[pk]) throw new Error("Trying to remove nonexistent entry.");
        this.entries[pk].destroy();
        delete this.entries[pk];
        this.loadedEntries = this.loadedEntries.filter(e => e.pk !== pk);
        this.noElementsMessage.style.display = Object.keys(this.entries).length === 0 ? "block" : "none";
    }

    /**
     * Function called by a recently added entry.     
     * @param rawEntry the raw data (used to recreate it).
     * @param entryReference the reference (used to destroy the entry).
     */
    public addNewEntry(rawEntry: IJournalEntry, entryReference: JournalEntry) {
        this.loadedEntries.push(rawEntry);
        this.entries[rawEntry.pk] = entryReference;
        this.addEntryButton.disabled = false;
        this.noElementsMessage.style.display = Object.keys(this.entries).length === 0 ? "block" : "none";
    }

    public creationHasBeenAborted(): void {
        this.addEntryButton.disabled = false;
        this.noElementsMessage.style.display = Object.keys(this.entries).length === 0 ? "block" : "none";
    }

    /**
     * Function called by a recently updated entry.
     * @param rawEntry the raw data corresponding to the new entry.
     */
    public updateEntry(rawEntry: IJournalEntry) {
        this.loadedEntries = this.loadedEntries.filter(e => e.pk !== rawEntry.pk);
        this.loadedEntries.push(rawEntry);
        this.loadedEntries = this.loadedEntries.sort((e1, e2) => e1.date - e2.date);
    }
}