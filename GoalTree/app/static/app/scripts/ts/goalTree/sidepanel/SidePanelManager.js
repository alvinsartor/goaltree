"use strict";
class SidePanelManager {
    /**
     * Creates a new instance of SidePanelManager
     */
    constructor(categoryPk) {
        this.sidePanelsConstructors = [
            SidePanelControl,
            SidePanelJournal,
            SidePanelPinned,
        ];
        this.sidePanels = [];
        this.activePanel = null;
        this.minLeftPositionValue = -280;
        this.maxLeftPositionValue = 10;
        this.initialDrag = Point.zero();
        this.categoryPk = categoryPk;
        this.sidePanelContainer = document.querySelector(".side-panel");
        this.sideMenuTemplate = document.querySelector("#side-panel-menu-template");
        this.sidePanelDragButton = document.querySelector(".side-panel-drag-button");
        this.sidePanelsConstructors.forEach(m => this.createPanelFromTemplate(m));
        // show first, pre-load the other panels for quicker speedup when they are selected
        this.sidePanels[0].open();
        this.sidePanels.slice(1).forEach(p => p.preLoadContentIfNecessary());
        window.addEventListener("resize", () => { if (this.activePanel !== null)
            this.activePanel.resetHeight(); });
        this.addDragButtonListeners();
    }
    createPanelFromTemplate(menuConstructor) {
        const template = this.sideMenuTemplate.content.querySelector("div[name='side-menu']");
        const menu = document.importNode(template, true);
        this.sidePanelContainer.appendChild(menu);
        const sideMenu = new menuConstructor(this, menu);
        this.sidePanels.push(sideMenu);
    }
    /**
     * Function called when a panel has opened. It will shut down any other open panels.
     * @param panel the panel that has just been opened.
     */
    panelHasActivated(panel) {
        if (this.activePanel != null)
            this.activePanel.close();
        this.activePanel = panel;
    }
    /**
     * Calculates the total available space to be filled by the side panels.
     */
    getAvailableSpace() {
        const total = this.sidePanelContainer.getBoundingClientRect().height;
        const padding = Number(window.getComputedStyle(this.sidePanelContainer).padding.replace("px", "")) * 2;
        const accordions = this.sidePanels.map(p => p.getAccordionHeight())
            .reduce((accumulator, next) => accumulator + next, 0);
        return `${total - padding - accordions}px`;
    }
    addDragButtonListeners() {
        this.sidePanelDragButton.ontouchstart = (e) => this.startDrag(e);
        this.sidePanelDragButton.ontouchend = (e) => this.endDrag(e);
    }
    startDrag(e) {
        e.preventDefault();
        e.stopPropagation();
        this.initialDrag = Point.extractPagePointFromUiEvent(e);
        this.sidePanelDragButton.ontouchmove = (e1) => this.dragMenuPanel(e1);
    }
    dragMenuPanel(e) {
        e.preventDefault();
        e.stopPropagation();
        const actualPosition = Point.extractPagePointFromUiEvent(e);
        const currentRight = this.getCurrentRightValue();
        let actualX = currentRight + (this.initialDrag.x - actualPosition.x) * 2;
        actualX = actualX < this.minLeftPositionValue
            ? this.minLeftPositionValue
            : actualX > this.maxLeftPositionValue
                ? this.maxLeftPositionValue
                : actualX;
        this.sidePanelContainer.style.right = actualX + "px";
        this.initialDrag = actualPosition;
    }
    endDrag(e) {
        e.preventDefault();
        e.stopPropagation();
        this.sidePanelDragButton.ontouchmove = null;
    }
    getCurrentRightValue() {
        let right = this.sidePanelContainer.style.right.replace("px", "");
        if (right === "") // it has not been assigned yet. Let's take it from the css
            right = getComputedStyle(this.sidePanelContainer).right;
        return Number(right.replace("px", ""));
    }
}
