
class SidePanelPinned extends SidePanel {
    private pinnedGoals: PinnedGoal[];


    private readonly pinnedBoard: HTMLDivElement;
    public readonly pinnedGoalsContainer: HTMLDivElement;
    private readonly noElementsMessage: HTMLDivElement;
    private readonly refreshListFunc: () => void;

    /** @inheritdoc  */
    public constructor(manager: SidePanelManager, menu: HTMLDivElement) {
        super(manager, menu);

        const template = (document.getElementById("pinned-goals-template") as HTMLTemplateElement)
            .content.querySelector("div[name='pinned-board']");

        this.pinnedBoard = document.importNode<any>(template, true) as HTMLDivElement;
        this.pinnedGoalsContainer = this.pinnedBoard.querySelector("div[name='pinned-goals-container']") as HTMLDivElement;
        this.noElementsMessage = this.pinnedBoard.querySelector("div[class='no-elements-message']") as HTMLDivElement;

        this.menuPanel.appendChild(this.pinnedBoard);
        this.pinnedGoals = [];

        this.refreshListFunc = this.onGoalPinnedStatusChanged.bind(this);
    }

    /** @inheritdoc  */
    public name() { return "Pinned Goals"; };

    /** @inheritdoc  */
    protected async loadContent(): Promise<void> { /* nothing to load */ }

    /** @inheritdoc  */
    protected displayContent(): void {
        this.addPinnedGoals();

        CategoryModel.instance().goalPinned.subscribe(this.refreshListFunc);
    }

    /** @inheritdoc  */
    protected removeContent(): void {
        this.removePinnedGoals();
        CategoryModel.instance().goalPinned.unsubscribe(this.refreshListFunc);
    }

    private addPinnedGoals() {
        this.pinnedGoals = CategoryModel.instance().pinnedGoals.map(g => new PinnedGoal(this, g));
        this.noElementsMessage.style.display = this.pinnedGoals.length === 0 ? "block" : "none";
    }

    private removePinnedGoals() {
        this.pinnedGoals.forEach(g => g.destroy());
        this.pinnedGoals = [];
    }

    private onGoalPinnedStatusChanged() {
        this.removePinnedGoals();
        this.addPinnedGoals();
    }
}