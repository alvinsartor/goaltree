
interface IJournalEntry {
    pk: number;
    category: number;
    text: string;
    date: number;
}

class JournalEntry {

    private static entryTemplate: HTMLTemplateElement | null = null;

    private pk: number;
    private text: string;
    private date: Date;

    private readonly journalPanel: SidePanelJournal;
    private readonly entryElement: HTMLDivElement;

    private readonly liveContainer: HTMLDivElement;
    private readonly entryLiveText: HTMLDivElement;
    private readonly entryDate: HTMLLabelElement;
    private readonly entryEdit: HTMLLabelElement;
    private readonly entryDelete: HTMLLabelElement;

    private readonly editContainer: HTMLDivElement;
    private readonly entryEditText: HTMLTextAreaElement;
    private readonly entrySave: HTMLLabelElement;
    private readonly entryCancel: HTMLLabelElement;

    private isLiveMode: boolean;
    private isExistingEntry: boolean;

    /**
    * Creates a new instance of JournalEntry from an existing entry.
    * @param journalPanel the panel holding the entries.
    * @param journalEntry the existing entry, fetched from the DB.
    */
    public static createFromDb(journalPanel: SidePanelJournal, journalEntry: IJournalEntry): JournalEntry {
        return new JournalEntry(journalPanel, journalEntry);
    }

    /**
    * Creates a new instance of JournalEntry ready to be filled.
    * @param journalPanel the panel holding the entries.
    */
    public static createEmpty(journalPanel: SidePanelJournal): JournalEntry {
        return new JournalEntry(journalPanel);
    }

    public constructor(journalPanel: SidePanelJournal, journalEntry: IJournalEntry | null = null) {

        if (journalEntry === null) {
            this.pk = 0;
            this.text = "";
            this.date = new Date();
            this.isExistingEntry = false;
        } else {
            this.pk = journalEntry.pk;
            this.text = journalEntry.text;
            this.date = new Date(journalEntry.date * 1000);
            this.isExistingEntry = true;
        }
        this.isLiveMode = this.isExistingEntry;

        this.journalPanel = journalPanel;

        if (JournalEntry.entryTemplate === null) {
            JournalEntry.entryTemplate = document.getElementById("journal-entry-template") as HTMLTemplateElement;
        }

        const template = JournalEntry.entryTemplate.content.querySelector("div[name='journal-entry']");
        this.entryElement = document.importNode<any>(template, true) as HTMLDivElement;
        this.journalPanel.entriesContainer.appendChild(this.entryElement);

        this.entryLiveText = this.entryElement.querySelector("div[name='journal-entry-text']") as HTMLDivElement;
        this.entryDate = this.entryElement.querySelector("label[class='journal-entry-date']") as HTMLLabelElement;
        this.entryEdit = this.entryElement.querySelector("label[name='editAction']") as HTMLLabelElement;
        this.entryDelete = this.entryElement.querySelector("label[name='deleteAction']") as HTMLLabelElement;

        this.entryEditText = this.entryElement.querySelector("textarea") as HTMLTextAreaElement;

        this.liveContainer = this.entryElement.querySelector("div[name='live']") as HTMLDivElement;
        this.editContainer = this.entryElement.querySelector("div[name='edit']") as HTMLDivElement;
        this.entrySave = this.entryElement.querySelector("label[name='saveAction']") as HTMLLabelElement;
        this.entryCancel = this.entryElement.querySelector("label[name='cancelAction']") as HTMLLabelElement;

        this.setFieldsAndListeners();
    }

    /**
     * Removes the entry node from its container.
     */
    public destroy() {
        this.journalPanel.entriesContainer.removeChild(this.entryElement);
    }

    private setFieldsAndListeners() {
        if (this.isLiveMode) {
            this.entryLiveText.innerHTML = this.text;
            this.entryDate.textContent = this.date.toLocaleString();
            this.liveContainer.style.display = "block";
            this.editContainer.style.display = "none";
            this.setLiveListeners();
        } else {
            this.entryEditText.value = this.text;
            this.editContainer.style.display = "block";
            this.liveContainer.style.display = "none";
            this.setEditListeners();
            this.entryEditText.focus();
        }
    }

    private setLiveListeners() {
        this.entryDelete.onclick = async () => {
            if (await PopupDialog.confirm("Are you sure you want to delete this entry?")) {
                this.deleteThisEntry();
            }
        };

        this.entryEdit.onclick = () => {
            this.isLiveMode = false;
            this.setFieldsAndListeners();
        };

        this.entrySave.onclick = null;
        this.entryCancel.onclick = null;
    }

    private setEditListeners() {
        this.entryDelete.onclick = null;
        this.entryEdit.onclick = null;
        this.entrySave.onclick = () => this.isExistingEntry ? this.setNewTextForThisEntry() : this.sendAndCreateEntry();
        this.entryCancel.onclick = () => {
            if (this.isExistingEntry) {
                this.isLiveMode = true;
                this.setFieldsAndListeners();
            } else {
                this.destroy();
                this.journalPanel.creationHasBeenAborted();
            }
        };
    }

    private sendAndCreateEntry() {
        this.entrySave.onclick = null;
        const text = this.entryEditText.value;
        if (text === "") {
            this.journalPanel.creationHasBeenAborted();
            this.destroy();
        }

        const formData = new FormData();
        formData.append("csrfmiddlewaretoken",
            (document.getElementsByName("csrfmiddlewaretoken")[0] as HTMLInputElement).value);
        formData.append("text", text);
        formData.append("category", String(this.journalPanel.categoryPk));

        $.ajax({
            type: "POST",
            url: `/create_journal_entry/`,
            data: ((formData) as any) as JQuery.PlainObject,
            processData: false,
            contentType: false,
            success: (entry: IJournalEntry) => {
                this.pk = entry.pk;
                this.text = entry.text;
                this.isExistingEntry = true;
                this.date = new Date(entry.date * 1000);
                this.isLiveMode = true;
                this.setFieldsAndListeners();
                this.journalPanel.addNewEntry(entry, this);
            },
            error: (error) => {
                console.log(error);
                this.journalPanel.creationHasBeenAborted();
                this.destroy();
            }
        });
    }

    private setNewTextForThisEntry() {
        this.entrySave.onclick = null;
        const text = this.entryEditText.value;
        if (text === null || text === "" || text === this.text) {
            this.isLiveMode = true;
            this.setFieldsAndListeners();
        }

        const formData = new FormData();
        formData.append("csrfmiddlewaretoken",
            (document.getElementsByName("csrfmiddlewaretoken")[0] as HTMLInputElement).value);
        formData.append("text", text);
        formData.append("category", String(this.journalPanel.categoryPk));

        $.ajax({
            type: "POST",
            url: `/edit_journal_entry/${this.pk}`,
            data: ((formData) as any) as JQuery.PlainObject,
            processData: false,
            contentType: false,
            success: (entry: IJournalEntry) => {
                this.text = entry.text;
                this.isLiveMode = true;
                this.setFieldsAndListeners();
                this.journalPanel.updateEntry(entry);
            },
            error: (error) => {
                console.log(error);
                this.isLiveMode = true;
                this.setFieldsAndListeners();
            }
        });
    }

    private deleteThisEntry() {
        const formData = new FormData();
        formData.append("csrfmiddlewaretoken",
            (document.getElementsByName("csrfmiddlewaretoken")[0] as HTMLInputElement).value);

        $.ajax({
            type: "POST",
            url: `/delete_journal_entry/${this.pk}`,
            data: ((formData) as any) as JQuery.PlainObject,
            processData: false,
            contentType: false,
            success: () => this.journalPanel.removeEntry(this.pk),
            error: (error) => { console.log(error); }
        });
    }

}