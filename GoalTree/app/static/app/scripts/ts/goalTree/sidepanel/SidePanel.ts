
abstract class SidePanel {

    protected readonly manager: SidePanelManager;
    public readonly categoryPk: number;
    private readonly menuAccordion: HTMLButtonElement;
    protected readonly menuPanel: HTMLDivElement;
    private active: boolean = false;

    private isLoading = false;
    private hasLoaded = false;

    /**
     * Creates a new instance of SidePanel
     * @param menu the div containing accordion and panel.
     */
    protected constructor(manager: SidePanelManager, menu: HTMLDivElement) {
        this.manager = manager;
        this.categoryPk = manager.categoryPk;

        this.menuAccordion = menu.querySelector(".side-panel-menu-accordion") as HTMLButtonElement;
        this.menuAccordion.innerHTML = this.name();
        this.menuAccordion.onclick = () => { if (!this.active) this.open(); };

        this.menuPanel = menu.querySelector(".side-panel-menu") as HTMLDivElement;
    }

    /** The side panel name. It is the text displayed in the accordion. */
    protected abstract name(): string;

    /** Loads the content from the server asynchronously. */
    protected abstract loadContent(): Promise<void>;

    /** Displays the loaded data. */
    protected abstract displayContent(): void;

    /** Removes any displayed data. */
    protected abstract removeContent(): void;

    /**
    * Expands the panel to maximum size.
    */
    public async open(): Promise<void> {
        if (this.active) return;

        this.active = true;
        this.manager.panelHasActivated(this);
        this.resetHeight();
        await this.preLoadContentIfNecessary();
        this.displayContent();
    }

    public async preLoadContentIfNecessary(): Promise<void> {
        if (this.isLoading || this.hasLoaded) return;
        this.isLoading = true;
        await this.loadContent();
        this.isLoading = false;
        this.hasLoaded = true;
    }

    /**
    * Re-sets the height and the class depending on the active state.
    */
    public resetHeight(): void {
        this.menuPanel.style.height = this.active ? this.manager.getAvailableSpace() : "0px";
        this.menuAccordion.classList.toggle("side-panel-active", this.active);
    }

    /**
    * Retracts the panel to minimum size.
    * Called by the manager only when another panel is opened.
    */
    public close(): void {
        if (!this.active) return;

        this.active = false;
        this.resetHeight();
        this.removeContent();
    }

    /**
     * Returns the height of the accordion.
     * Used by the manager to calculate the total available space.
     */
    public getAccordionHeight(): number {
        return this.menuAccordion.getBoundingClientRect().height + 4; //the container has margin:2px 0;
    }
}