"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
class SidePanelControl extends SidePanel {
    /** @inheritdoc  */
    constructor(manager, menu) {
        super(manager, menu);
        const template = document.getElementById("controls-template")
            .content.querySelector("div[name='controls']");
        this.controls = document.importNode(template, true);
        this.addPanListeners();
        this.addZoomListeners();
        this.addEdgeModeListeners();
        this.addGoalColorsListeners();
        this.addChildrenMovedAlongListeners();
        this.addTutorialListeners();
        this.menuPanel.appendChild(this.controls);
    }
    /** @inheritdoc  */
    name() { return "Controls"; }
    ;
    /** @inheritdoc  */
    loadContent() {
        return __awaiter(this, void 0, void 0, function* () { });
    }
    /** @inheritdoc  */
    displayContent() { }
    /** @inheritdoc  */
    removeContent() { }
    addPanListeners() {
        const resetPanBt = this.controls.querySelector("#reset-pan-bt");
        resetPanBt.onclick = () => Settings.instance().resetCategoryPan();
        const panLabel = this.controls.querySelector("#CurrentPan-label");
        panLabel.innerText = Settings.instance().currentPanString;
        Settings.instance().panChanged.subscribe(() => panLabel.innerText = Settings.instance().currentPanString);
    }
    addZoomListeners() {
        const resetZoomBt = this.controls.querySelector("#reset-zoom-bt");
        resetZoomBt.onclick = () => Settings.instance().resetCategoryZoom();
        const increaseZoomBt = this.controls.querySelector("#increase-zoom-bt");
        increaseZoomBt.onclick = () => Settings.instance().increaseZoom();
        const decreaseZoomBt = this.controls.querySelector("#decrease-zoom-bt");
        decreaseZoomBt.onclick = () => Settings.instance().decreaseZoom();
        const zoomLabel = this.controls.querySelector("#CurrentZoom-label");
        zoomLabel.innerText = Settings.instance().currentZoomString;
        Settings.instance().zoomChanged.subscribe(() => zoomLabel.innerText = Settings.instance().currentZoomString);
    }
    addEdgeModeListeners() {
        const edgeModeCheckbox = this.controls.querySelector("#edge-mode-checkbox");
        edgeModeCheckbox.oninput = () => { Settings.instance().edgeButtonPressed(); };
    }
    addGoalColorsListeners() {
        const colorsSelect = this.controls.querySelector("select[name='goals-color']");
        const currentColor = Settings.instance().goalsColor;
        ColorsManager.instance().colors.forEach(c => this.addColorOption(c, colorsSelect, currentColor));
        colorsSelect.onchange = () => {
            const color = ColorsManager.instance().getColor(Number(colorsSelect.value));
            Settings.instance().changeGoalsColor(color);
        };
    }
    addChildrenMovedAlongListeners() {
        const moveAncestorsAlongCheckbox = this.controls.querySelector("#move-along-checkbox");
        moveAncestorsAlongCheckbox.checked = Settings.instance().ancestorsAreMovedAlong;
        moveAncestorsAlongCheckbox.oninput = () => {
            Settings.instance().ancestorsAreMovedAlong = moveAncestorsAlongCheckbox.checked;
        };
    }
    addTutorialListeners() {
        const showAgainTutorialButton = this.controls.querySelector("#show-tutorial-bt");
        showAgainTutorialButton.onclick = () => UserMessagesTracker.showTutorialTree();
    }
    addColorOption(color, select, currentColor) {
        const option = document.createElement("option");
        option.value = String(color.pk);
        option.textContent = color.name;
        if (color.pk === currentColor.pk)
            option.selected = true;
        select.appendChild(option);
    }
}
