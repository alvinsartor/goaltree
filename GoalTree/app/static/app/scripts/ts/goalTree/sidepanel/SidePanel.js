"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
class SidePanel {
    /**
     * Creates a new instance of SidePanel
     * @param menu the div containing accordion and panel.
     */
    constructor(manager, menu) {
        this.active = false;
        this.isLoading = false;
        this.hasLoaded = false;
        this.manager = manager;
        this.categoryPk = manager.categoryPk;
        this.menuAccordion = menu.querySelector(".side-panel-menu-accordion");
        this.menuAccordion.innerHTML = this.name();
        this.menuAccordion.onclick = () => { if (!this.active)
            this.open(); };
        this.menuPanel = menu.querySelector(".side-panel-menu");
    }
    /**
    * Expands the panel to maximum size.
    */
    open() {
        return __awaiter(this, void 0, void 0, function* () {
            if (this.active)
                return;
            this.active = true;
            this.manager.panelHasActivated(this);
            this.resetHeight();
            yield this.preLoadContentIfNecessary();
            this.displayContent();
        });
    }
    preLoadContentIfNecessary() {
        return __awaiter(this, void 0, void 0, function* () {
            if (this.isLoading || this.hasLoaded)
                return;
            this.isLoading = true;
            yield this.loadContent();
            this.isLoading = false;
            this.hasLoaded = true;
        });
    }
    /**
    * Re-sets the height and the class depending on the active state.
    */
    resetHeight() {
        this.menuPanel.style.height = this.active ? this.manager.getAvailableSpace() : "0px";
        this.menuAccordion.classList.toggle("side-panel-active", this.active);
    }
    /**
    * Retracts the panel to minimum size.
    * Called by the manager only when another panel is opened.
    */
    close() {
        if (!this.active)
            return;
        this.active = false;
        this.resetHeight();
        this.removeContent();
    }
    /**
     * Returns the height of the accordion.
     * Used by the manager to calculate the total available space.
     */
    getAccordionHeight() {
        return this.menuAccordion.getBoundingClientRect().height + 4; //the container has margin:2px 0;
    }
}
