
class SidePanelControl extends SidePanel {

    private readonly controls: HTMLDivElement;

    /** @inheritdoc  */
    public constructor(manager: SidePanelManager, menu: HTMLDivElement) {
        super(manager, menu);

        const template = (document.getElementById("controls-template") as HTMLTemplateElement)
            .content.querySelector("div[name='controls']");

        this.controls = document.importNode<any>(template, true) as HTMLDivElement;

        this.addPanListeners();
        this.addZoomListeners();
        this.addEdgeModeListeners();
        this.addGoalColorsListeners();
        this.addChildrenMovedAlongListeners();
        this.addTutorialListeners();

        this.menuPanel.appendChild(this.controls);
    }

    /** @inheritdoc  */
    public name() { return "Controls"; };

    /** @inheritdoc  */
    protected async loadContent(): Promise<void> { /* nothing to load */ }

    /** @inheritdoc  */
    protected displayContent(): void { /* nothing to display */ }

    /** @inheritdoc  */
    protected removeContent(): void { /* nothing to remove */ }

    private addPanListeners(): void {
        const resetPanBt = this.controls.querySelector("#reset-pan-bt") as HTMLButtonElement;
        resetPanBt.onclick = () => Settings.instance().resetCategoryPan();

        const panLabel = this.controls.querySelector("#CurrentPan-label") as HTMLLabelElement;
        panLabel.innerText = Settings.instance().currentPanString;
        Settings.instance().panChanged.subscribe(() => panLabel.innerText = Settings.instance().currentPanString);
    }

    private addZoomListeners(): void {
        const resetZoomBt = this.controls.querySelector("#reset-zoom-bt") as HTMLButtonElement;
        resetZoomBt.onclick = () => Settings.instance().resetCategoryZoom();
        const increaseZoomBt = this.controls.querySelector("#increase-zoom-bt") as HTMLButtonElement;
        increaseZoomBt.onclick = () => Settings.instance().increaseZoom();
        const decreaseZoomBt = this.controls.querySelector("#decrease-zoom-bt") as HTMLButtonElement;
        decreaseZoomBt.onclick = () => Settings.instance().decreaseZoom();

        const zoomLabel = this.controls.querySelector("#CurrentZoom-label") as HTMLLabelElement;
        zoomLabel.innerText = Settings.instance().currentZoomString;
        Settings.instance().zoomChanged.subscribe(() => zoomLabel.innerText = Settings.instance().currentZoomString);
    }

    private addEdgeModeListeners(): void {
        const edgeModeCheckbox = this.controls.querySelector("#edge-mode-checkbox") as HTMLInputElement;
        edgeModeCheckbox.oninput = () => { Settings.instance().edgeButtonPressed(); };
    }

    private addGoalColorsListeners(): void {
        const colorsSelect = this.controls.querySelector("select[name='goals-color']") as HTMLSelectElement;
        const currentColor = Settings.instance().goalsColor;
        ColorsManager.instance().colors.forEach(c => this.addColorOption(c, colorsSelect, currentColor));

        colorsSelect.onchange = () => {
            const color = ColorsManager.instance().getColor(Number(colorsSelect.value));
            Settings.instance().changeGoalsColor(color); 
        }
    }

    private addChildrenMovedAlongListeners(): void {
        const moveAncestorsAlongCheckbox = this.controls.querySelector("#move-along-checkbox") as HTMLInputElement;
        moveAncestorsAlongCheckbox.checked = Settings.instance().ancestorsAreMovedAlong;

        moveAncestorsAlongCheckbox.oninput = () => {
            Settings.instance().ancestorsAreMovedAlong = moveAncestorsAlongCheckbox.checked;
        };
    }

    private addTutorialListeners(): void
    {
        const showAgainTutorialButton = this.controls.querySelector("#show-tutorial-bt") as HTMLButtonElement;
        showAgainTutorialButton.onclick = () => UserMessagesTracker.showTutorialTree();
    }

    private addColorOption(color: Color, select: HTMLSelectElement, currentColor: Color) {
        const option = document.createElement("option");
        option.value = String(color.pk);
        option.textContent = color.name;
        if (color.pk === currentColor.pk) option.selected = true;

        select.appendChild(option);
    }
}