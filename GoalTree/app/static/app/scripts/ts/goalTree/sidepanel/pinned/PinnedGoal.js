"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
class PinnedGoal {
    constructor(parent, model) {
        this.parent = parent;
        this.model = model;
        this.clone = PinnedGoal.extractClone(model);
        this.parent.pinnedGoalsContainer.appendChild(this.clone);
        this.clone.onclick = () => Settings.instance().centerOnGoal(this.model);
        const pinButton = this.clone.querySelector("div[class='pin-goal-bt']");
        pinButton.onclick = (e) => __awaiter(this, void 0, void 0, function* () {
            e.stopPropagation();
            if (yield PopupDialog.confirm("Are you sure you want to unpin this goal?")) {
                this.model.unpin();
            }
        });
    }
    static extractClone(model) {
        const clone = model.goalTile.cloneNode(true);
        clone.classList.remove("live");
        clone.classList.remove("edit");
        clone.classList.replace("goal-tile", "cloned-goal-tile");
        const cloneEditDiv = clone.querySelector(".goal-tile-edit");
        clone.removeChild(cloneEditDiv);
        clone.style.transform = "scale(1)";
        return clone;
    }
    destroy() {
        this.parent.pinnedGoalsContainer.removeChild(this.clone);
    }
}
