"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
class SidePanelPinned extends SidePanel {
    /** @inheritdoc  */
    constructor(manager, menu) {
        super(manager, menu);
        const template = document.getElementById("pinned-goals-template")
            .content.querySelector("div[name='pinned-board']");
        this.pinnedBoard = document.importNode(template, true);
        this.pinnedGoalsContainer = this.pinnedBoard.querySelector("div[name='pinned-goals-container']");
        this.noElementsMessage = this.pinnedBoard.querySelector("div[class='no-elements-message']");
        this.menuPanel.appendChild(this.pinnedBoard);
        this.pinnedGoals = [];
        this.refreshListFunc = this.onGoalPinnedStatusChanged.bind(this);
    }
    /** @inheritdoc  */
    name() { return "Pinned Goals"; }
    ;
    /** @inheritdoc  */
    loadContent() {
        return __awaiter(this, void 0, void 0, function* () { });
    }
    /** @inheritdoc  */
    displayContent() {
        this.addPinnedGoals();
        CategoryModel.instance().goalPinned.subscribe(this.refreshListFunc);
    }
    /** @inheritdoc  */
    removeContent() {
        this.removePinnedGoals();
        CategoryModel.instance().goalPinned.unsubscribe(this.refreshListFunc);
    }
    addPinnedGoals() {
        this.pinnedGoals = CategoryModel.instance().pinnedGoals.map(g => new PinnedGoal(this, g));
        this.noElementsMessage.style.display = this.pinnedGoals.length === 0 ? "block" : "none";
    }
    removePinnedGoals() {
        this.pinnedGoals.forEach(g => g.destroy());
        this.pinnedGoals = [];
    }
    onGoalPinnedStatusChanged() {
        this.removePinnedGoals();
        this.addPinnedGoals();
    }
}
