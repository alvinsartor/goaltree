"use strict";
class Experience {
    constructor(level, expPoints, description) {
        this.level = level;
        this.expPoints = expPoints;
        this.description = description;
    }
    /**
     * Creates an Experience object starting from a IExperience, retrieved from the server.
     * @param exp The IExperience object retrieved from the server.
     */
    static fromIExperience(exp) {
        return new Experience(exp.fields.level, exp.fields.exp_points, exp.fields.description);
    }
}
