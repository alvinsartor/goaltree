
class TemporaryCategoryTileModel {
    private readonly tileName = "category-tile-Tmp";
    private readonly formName = "category-form-Tmp";
    private readonly manager: CategoriesManager;
    private canvasColor : Color;

    private readonly categoryTile: HTMLElement;
    private readonly categoryForm: HTMLFormElement;
    private readonly categoryNameEdit: HTMLInputElement;
    private readonly categoryDescriptionEdit: HTMLTextAreaElement;
    private readonly categoryCanvasColorEdit: HTMLSelectElement;

    /**
    * Initializes a new empty instance of TemporaryCategoryTileModel.
    */
    constructor(manager: CategoriesManager) {
        this.manager = manager;

        const template = (document.getElementById("category-tile-template") as HTMLTemplateElement)
            .content.querySelector(".category-tile.live");

        this.categoryTile = document.importNode<any>(template, true);
        this.categoryForm = this.categoryTile.querySelector("form") as HTMLFormElement;
        this.categoryNameEdit = this.categoryTile.querySelector(".category-name-edit") as HTMLInputElement;
        this.categoryDescriptionEdit = this.categoryTile.querySelector(".category-desc-edit") as HTMLTextAreaElement;

        this.categoryCanvasColorEdit = this.categoryTile.querySelector("select[id='id_canvas_color']") as HTMLSelectElement;
        this.categoryCanvasColorEdit.onchange = () => this.updateColorAndRepaint();

        const categoryDeleteBt = this.categoryTile.querySelector('button[id="delete-category-bt"') as HTMLButtonElement;
        categoryDeleteBt.hidden = true;
        const categoryArchiveBt = this.categoryTile.querySelector('button[id="archive-category-bt"') as HTMLButtonElement;
        categoryArchiveBt.hidden = true;
        this.canvasColor = ColorsManager.instance().defaultCategoryAndGoalColor[0];

        this.populateTile();
        this.setToEditMode();
        this.manager.canvas.insertBefore(this.categoryTile, this.manager.placeholderNode);
        this.categoryTile.scrollIntoView(true);
        this.categoryNameEdit.focus();
    }

    /**
     * Remove the current category tile
     */
    public removeTile(): void {
        this.manager.canvas.removeChild(this.categoryTile);
        this.manager.showMessageIfThereAreNoCategories();
    }

    private setToEditMode() {
        this.categoryTile.classList.remove("live");
        this.categoryTile.classList.add("edit");

        const updateOrDestroy = (e1: UIEvent) => {
            if (!this.categoryTile.contains((e1.target) as any) || e1 instanceof KeyboardEvent) {
                if (this.categoryNameEdit.value.trim() !== "") {
                    this.createCategory();
                } else {
                    this.removeTile();
                }
                document.removeEventListener("mousedown", updateOrDestroy);
                this.categoryNameEdit.onkeydown = null;
                this.categoryDescriptionEdit.onkeydown = null;
            }
        };

        const updateOrDestroyOnEnter = (e1: KeyboardEvent) =>
        {
            if (e1.keyCode === 13)
            {
                e1.preventDefault();
                e1.stopPropagation();
                updateOrDestroy(e1);
                return false;
            }
            return true;
        }

        document.addEventListener("mousedown", updateOrDestroy);
        this.categoryNameEdit.addEventListener("keydown", updateOrDestroyOnEnter);
        this.categoryDescriptionEdit.addEventListener("keydown", updateOrDestroyOnEnter);
    }

    private populateTile() {
        this.categoryTile.id = this.tileName;
        this.categoryForm.id = this.formName;
        this.categoryNameEdit.value = "";
        this.categoryDescriptionEdit.value = "";

        this.repaintCategoryTile();
    }

    private updateColorAndRepaint() {
        const selectedColor = Number(this.categoryCanvasColorEdit.value);
        const newColor = ColorsManager.instance().getColor(selectedColor);
        this.canvasColor = newColor;
        this.repaintCategoryTile();
    }

    public repaintCategoryTile(): void {
        const textColor = this.canvasColor.text;
        this.categoryTile.style.color = textColor;
        this.categoryNameEdit.style.color = textColor;
        this.categoryNameEdit.style.borderColor = textColor;
        this.categoryDescriptionEdit.style.color = textColor;
        this.categoryDescriptionEdit.style.borderColor = textColor;

        this.categoryTile.style.background = this.canvasColor.c3;
    }

    private createCategory(): void {
        const form = $(`#${this.formName}`);

        const ajaxSettings: JQueryAjaxSettings = {
            url: `/create_category/`,
            type: "POST",
            data: form.serialize(),
            success: (category: ICategoryTileJson[]) => {
                this.manager.addCategoryFromJson(category[0]);
                this.removeTile();
            },
            error: (err) => {
                console.log(err);
                this.removeTile();
            }
        };
        $.ajax(ajaxSettings);
    };
}