"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
class CategoryTileModel {
    /**
    * Initializes a new instance of CategoryTileModel using an identifier and a json containing the necessary information.
    */
    constructor(categoryTileJson, manager) {
        this.colorHasChanged = false;
        this.destroying = false;
        this.pk = categoryTileJson.pk;
        this.name = categoryTileJson.fields.name;
        this.description = categoryTileJson.fields.description;
        this.archived = categoryTileJson.fields.archived;
        this.manager = manager;
        const template = document.getElementById("category-tile-template")
            .content.querySelector(".category-tile.live");
        this.categoryTile = document.importNode(template, true);
        this.categoryForm = this.categoryTile.querySelector("form");
        this.categoryNameEdit = this.categoryTile.querySelector(".category-name-edit");
        this.categoryDescriptionEdit = this.categoryTile.querySelector(".category-desc-edit");
        this.categoryCanvasColorEdit =
            this.categoryTile.querySelector("select[id='id_canvas_color']");
        this.categoryDeleteBt = this.categoryTile.querySelector('button[id="delete-category-bt"');
        this.categoryArchiveBt =
            this.categoryTile.querySelector('button[id="archive-category-bt"');
        this.categoryEditBt = this.categoryTile.querySelector('button[id="edit-category-bt"');
        this.categoryArchivedEdit = this.categoryTile.querySelector('input[name="archived"');
        this.canvasColor = ColorsManager.instance().getColor(categoryTileJson.fields.canvas_color);
        this.populateTile();
        this.addTileEventListeners();
        this.manager.canvas.insertBefore(this.categoryTile, this.manager.placeholderNode);
    }
    /**
     * Function called when some information of the category has been changed and it needs to be refreshed.
     * @param categoryTileJson the JSON containing the new information.
     */
    updateCategoryInformation(categoryTileJson) {
        if (this.pk !== categoryTileJson.pk)
            throw Error("Are you trying to substitute this category with another one? This is not possible");
        this.colorHasChanged = false;
        this.name = categoryTileJson.fields.name;
        this.description = categoryTileJson.fields.description;
        this.populateTile();
    }
    /**
     * Remove the current category tile
     */
    removeTile() {
        this.destroying = true;
        this.manager.canvas.removeChild(this.categoryTile);
    }
    addTileEventListeners() {
        this.categoryTile.onclick = () => window.location.href = `/tree/${this.pk}`;
    }
    removeTileEventListeners() {
        this.categoryTile.onmousedown = (e) => e.stopPropagation();
        this.categoryTile.onclick = null;
    }
    setToEditMode(e) {
        e.preventDefault();
        e.stopPropagation();
        this.categoryTile.classList.remove("live");
        this.categoryTile.classList.add("edit");
        this.categoryNameEdit.focus();
        const setToLiveMode = (e1) => {
            if (!this.categoryTile.contains((e1.target)) || e1 instanceof KeyboardEvent) {
                document.removeEventListener("mousedown", setToLiveMode);
                this.categoryNameEdit.onkeydown = null;
                this.categoryDescriptionEdit.onkeydown = null;
                if (this.destroying)
                    return; //the listener has been removed. We won't end up here anymore.
                this.addTileEventListeners();
                this.categoryTile.classList.remove("edit");
                this.categoryTile.classList.add("live");
                this.updateIfValueChanged();
            }
        };
        const setToLiveOnEnter = (e1) => {
            if (e1.keyCode === 13) {
                e1.preventDefault();
                e1.stopPropagation();
                setToLiveMode(e1);
                return false;
            }
            return true;
        };
        this.removeTileEventListeners();
        document.addEventListener("mousedown", setToLiveMode);
        this.categoryNameEdit.addEventListener("keydown", setToLiveOnEnter);
        this.categoryDescriptionEdit.addEventListener("keydown", setToLiveOnEnter);
    }
    updateIfValueChanged() {
        if (this.categoryNameEdit.value.trim() !== this.name ||
            this.categoryDescriptionEdit.value.trim() !== this.description ||
            this.colorHasChanged) {
            this.updateCategory();
        }
    }
    populateTile() {
        // visible fields
        this.categoryTile.id = `category-tile-${this.pk}`;
        this.categoryTile.querySelector(".category-name").innerHTML = this.name;
        this.categoryTile.querySelector(".category-description").innerHTML = this.description;
        this.categoryForm.id = `category-edit-form-${this.pk}`;
        this.categoryNameEdit.value = this.name;
        this.categoryDescriptionEdit.value = this.description;
        this.categoryCanvasColorEdit.value = String(this.canvasColor.pk);
        this.categoryArchivedEdit.value = this.archived.toString();
        this.categoryDeleteBt.onclick = (e) => this.deleteCategory(e);
        this.categoryArchiveBt.onclick = (e) => this.toggleArchivedStatus(e);
        this.categoryEditBt.onclick = (e) => this.setToEditMode(e);
        this.categoryCanvasColorEdit.onchange = () => this.updateColorAndRepaint();
        this.repaintCategoryTile();
    }
    toggleArchivedStatus(e) {
        return __awaiter(this, void 0, void 0, function* () {
            e.preventDefault();
            e.stopPropagation();
            const questionMessage = this.archived
                ? `Are you sure you want to move '${this.name}' out of the archive?`
                : `Are you sure you want to archive '${this.name}'?`;
            const confirmationMessage = this.archived
                ? `'${this.name}' has been removed from the archive.`
                : `'${this.name}' has been archived.`;
            if (!(yield PopupDialog.confirm(questionMessage)))
                return;
            this.archived = !this.archived;
            this.categoryArchivedEdit.value = this.archived.toString();
            this.updateCategory();
            this.manager.removeCategory(this);
            MessageManager.info(confirmationMessage);
        });
    }
    updateColorAndRepaint() {
        const selectedColor = Number(this.categoryCanvasColorEdit.value);
        const newColor = ColorsManager.instance().getColor(selectedColor);
        if (newColor.c5 !== this.canvasColor.c5) {
            this.canvasColor = newColor;
            this.colorHasChanged = true;
            this.repaintCategoryTile();
        }
    }
    repaintCategoryTile() {
        const textColor = this.canvasColor.text;
        this.categoryTile.style.color = textColor;
        this.categoryNameEdit.style.color = textColor;
        this.categoryNameEdit.style.borderColor = textColor;
        this.categoryDescriptionEdit.style.color = textColor;
        this.categoryDescriptionEdit.style.borderColor = textColor;
        this.categoryDeleteBt.querySelector("svg").style.fill = textColor;
        this.categoryArchiveBt.querySelector("svg").style.fill = textColor;
        this.categoryEditBt.querySelector("svg").style.fill = textColor;
        this.categoryTile.style.background = this.canvasColor.c3;
    }
    updateCategory() {
        const form = $(`#category-edit-form-${this.pk}`);
        const ajaxSettings = {
            url: `/update_category/${this.pk}`,
            type: "POST",
            data: form.serialize(),
            success: (category) => {
                this.updateCategoryInformation(category[0]);
                this.repaintCategoryTile();
            },
            error: (err) => {
                console.log(err);
                this.populateTile(); //this resets the input fields
            }
        };
        $.ajax(ajaxSettings);
    }
    ;
    deleteCategory(e) {
        return __awaiter(this, void 0, void 0, function* () {
            e.preventDefault();
            e.stopPropagation();
            if (!(yield PopupDialog.confirm("Are you sure you want to delete this category?")))
                return;
            if (!(yield PopupDialog.confirm("Really sure? All goals in it contained, progress and experience will be lost forever!")))
                return;
            const form = $(`#category-edit-form-${this.pk}`);
            $.ajax({
                type: "POST",
                url: `/delete_category/${this.pk}`,
                data: form.serialize(),
                success: () => {
                    this.manager.removeCategory(this);
                    UserLevelTracker.instance().checkForLevelChanges(true);
                },
                error: (error) => { console.log(error); }
            });
        });
    }
}
