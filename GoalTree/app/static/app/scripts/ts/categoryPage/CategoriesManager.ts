
class CategoriesManager {
    private categoriesContainer: { [pk: number]: CategoryTileModel; } = {};

    public readonly banner: HTMLElement;
    public readonly canvas: HTMLElement;
    public readonly placeholderNode: Node;
    public readonly isArchiveMode: boolean;

    private readonly nothingHereMessage: HTMLDivElement;

    /**
     * Creates a new instance of CategoriesManager.
     * @param categoriesJson The categories, in JSON format.
     */
    constructor(categoriesJson: ICategoryTileJson[]) {
        this.banner = document.querySelector("#banner") as HTMLElement;
        this.canvas = document.querySelector("#canvas") as HTMLCanvasElement;
        this.placeholderNode = document.querySelector("#categories-placeholder") as Node;
        this.nothingHereMessage = document.querySelector(".nothing-here-message") as HTMLDivElement;

        categoriesJson.forEach(category => this.addCategoryFromJson(category));

        const addCategoryButton = document.getElementById("add-category-bt") as HTMLButtonElement;
        this.isArchiveMode = addCategoryButton === null;
        if (addCategoryButton !== null) addCategoryButton!.onclick = () => {
            const _ = new TemporaryCategoryTileModel(this);
            this.nothingHereMessage.style.display = "none";
        };

        this.showMessageIfThereAreNoCategories();
    }

    /**
    * Gets the canvas position.
    */
    public getCanvasPosition(): Point {
        return new Point(this.canvas.offsetLeft, this.canvas.offsetTop);
    }

    /**
    * Adds a new category from a Json. This function is called when the category is added dynamically (using Ajax)
    */
    public addCategoryFromJson(category: ICategoryTileJson) {
        if (this.categoriesContainer[category.pk]) throw new Error(`Category with id '${category.pk}' already exists.`);
        this.categoriesContainer[category.pk] = new CategoryTileModel(category, this);
    }

    /**
    * Removes the category. This function is called when the category is removed dynamically (using Ajax)
    */
    public removeCategory(category: CategoryTileModel) {
        if (!this.categoriesContainer[category.pk]) throw new Error("Trying to remove nonexistent category.");

        this.categoriesContainer[category.pk].removeTile();
        delete this.categoriesContainer[category.pk];
        this.showMessageIfThereAreNoCategories();
    }

    /** Displays the placeholder message if there are no categories. */
    public showMessageIfThereAreNoCategories() {
        this.nothingHereMessage.style.display = Object.keys(this.categoriesContainer).length === 0 ? "flex" : "none";
    }
}