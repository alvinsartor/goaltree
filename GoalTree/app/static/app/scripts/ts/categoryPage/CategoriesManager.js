"use strict";
class CategoriesManager {
    /**
     * Creates a new instance of CategoriesManager.
     * @param categoriesJson The categories, in JSON format.
     */
    constructor(categoriesJson) {
        this.categoriesContainer = {};
        this.banner = document.querySelector("#banner");
        this.canvas = document.querySelector("#canvas");
        this.placeholderNode = document.querySelector("#categories-placeholder");
        this.nothingHereMessage = document.querySelector(".nothing-here-message");
        categoriesJson.forEach(category => this.addCategoryFromJson(category));
        const addCategoryButton = document.getElementById("add-category-bt");
        this.isArchiveMode = addCategoryButton === null;
        if (addCategoryButton !== null)
            addCategoryButton.onclick = () => {
                const _ = new TemporaryCategoryTileModel(this);
                this.nothingHereMessage.style.display = "none";
            };
        this.showMessageIfThereAreNoCategories();
    }
    /**
    * Gets the canvas position.
    */
    getCanvasPosition() {
        return new Point(this.canvas.offsetLeft, this.canvas.offsetTop);
    }
    /**
    * Adds a new category from a Json. This function is called when the category is added dynamically (using Ajax)
    */
    addCategoryFromJson(category) {
        if (this.categoriesContainer[category.pk])
            throw new Error(`Category with id '${category.pk}' already exists.`);
        this.categoriesContainer[category.pk] = new CategoryTileModel(category, this);
    }
    /**
    * Removes the category. This function is called when the category is removed dynamically (using Ajax)
    */
    removeCategory(category) {
        if (!this.categoriesContainer[category.pk])
            throw new Error("Trying to remove nonexistent category.");
        this.categoriesContainer[category.pk].removeTile();
        delete this.categoriesContainer[category.pk];
        this.showMessageIfThereAreNoCategories();
    }
    /** Displays the placeholder message if there are no categories. */
    showMessageIfThereAreNoCategories() {
        this.nothingHereMessage.style.display = Object.keys(this.categoriesContainer).length === 0 ? "flex" : "none";
    }
}
