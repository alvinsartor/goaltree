"use strict";
class TemporaryCategoryTileModel {
    /**
    * Initializes a new empty instance of TemporaryCategoryTileModel.
    */
    constructor(manager) {
        this.tileName = "category-tile-Tmp";
        this.formName = "category-form-Tmp";
        this.manager = manager;
        const template = document.getElementById("category-tile-template")
            .content.querySelector(".category-tile.live");
        this.categoryTile = document.importNode(template, true);
        this.categoryForm = this.categoryTile.querySelector("form");
        this.categoryNameEdit = this.categoryTile.querySelector(".category-name-edit");
        this.categoryDescriptionEdit = this.categoryTile.querySelector(".category-desc-edit");
        this.categoryCanvasColorEdit = this.categoryTile.querySelector("select[id='id_canvas_color']");
        this.categoryCanvasColorEdit.onchange = () => this.updateColorAndRepaint();
        const categoryDeleteBt = this.categoryTile.querySelector('button[id="delete-category-bt"');
        categoryDeleteBt.hidden = true;
        const categoryArchiveBt = this.categoryTile.querySelector('button[id="archive-category-bt"');
        categoryArchiveBt.hidden = true;
        this.canvasColor = ColorsManager.instance().defaultCategoryAndGoalColor[0];
        this.populateTile();
        this.setToEditMode();
        this.manager.canvas.insertBefore(this.categoryTile, this.manager.placeholderNode);
        this.categoryTile.scrollIntoView(true);
        this.categoryNameEdit.focus();
    }
    /**
     * Remove the current category tile
     */
    removeTile() {
        this.manager.canvas.removeChild(this.categoryTile);
        this.manager.showMessageIfThereAreNoCategories();
    }
    setToEditMode() {
        this.categoryTile.classList.remove("live");
        this.categoryTile.classList.add("edit");
        const updateOrDestroy = (e1) => {
            if (!this.categoryTile.contains((e1.target)) || e1 instanceof KeyboardEvent) {
                if (this.categoryNameEdit.value.trim() !== "") {
                    this.createCategory();
                }
                else {
                    this.removeTile();
                }
                document.removeEventListener("mousedown", updateOrDestroy);
                this.categoryNameEdit.onkeydown = null;
                this.categoryDescriptionEdit.onkeydown = null;
            }
        };
        const updateOrDestroyOnEnter = (e1) => {
            if (e1.keyCode === 13) {
                e1.preventDefault();
                e1.stopPropagation();
                updateOrDestroy(e1);
                return false;
            }
            return true;
        };
        document.addEventListener("mousedown", updateOrDestroy);
        this.categoryNameEdit.addEventListener("keydown", updateOrDestroyOnEnter);
        this.categoryDescriptionEdit.addEventListener("keydown", updateOrDestroyOnEnter);
    }
    populateTile() {
        this.categoryTile.id = this.tileName;
        this.categoryForm.id = this.formName;
        this.categoryNameEdit.value = "";
        this.categoryDescriptionEdit.value = "";
        this.repaintCategoryTile();
    }
    updateColorAndRepaint() {
        const selectedColor = Number(this.categoryCanvasColorEdit.value);
        const newColor = ColorsManager.instance().getColor(selectedColor);
        this.canvasColor = newColor;
        this.repaintCategoryTile();
    }
    repaintCategoryTile() {
        const textColor = this.canvasColor.text;
        this.categoryTile.style.color = textColor;
        this.categoryNameEdit.style.color = textColor;
        this.categoryNameEdit.style.borderColor = textColor;
        this.categoryDescriptionEdit.style.color = textColor;
        this.categoryDescriptionEdit.style.borderColor = textColor;
        this.categoryTile.style.background = this.canvasColor.c3;
    }
    createCategory() {
        const form = $(`#${this.formName}`);
        const ajaxSettings = {
            url: `/create_category/`,
            type: "POST",
            data: form.serialize(),
            success: (category) => {
                this.manager.addCategoryFromJson(category[0]);
                this.removeTile();
            },
            error: (err) => {
                console.log(err);
                this.removeTile();
            }
        };
        $.ajax(ajaxSettings);
    }
    ;
}
