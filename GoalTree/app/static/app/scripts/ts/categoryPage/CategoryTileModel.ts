
interface ICategoryTileJson {
    pk: number;
    fields: {
        name: string;
        description: string;
        canvas_color: number;
        archived: boolean;
    };
}

class CategoryTileModel {
    public readonly pk: number;
    private readonly manager: CategoriesManager;
    private name: string;
    private description: string;
    private canvasColor: Color;
    private colorHasChanged = false;
    private archived: boolean;
    private destroying = false;

    private readonly categoryTile: HTMLElement;
    private readonly categoryForm: HTMLFormElement;
    private readonly categoryNameEdit: HTMLInputElement;
    private readonly categoryDescriptionEdit: HTMLTextAreaElement;
    private readonly categoryCanvasColorEdit: HTMLSelectElement;
    private readonly categoryDeleteBt: HTMLButtonElement;
    private readonly categoryArchiveBt: HTMLButtonElement;
    private readonly categoryEditBt: HTMLButtonElement;
    private readonly categoryArchivedEdit: HTMLInputElement;

    /**
    * Initializes a new instance of CategoryTileModel using an identifier and a json containing the necessary information.
    */
    constructor(categoryTileJson: ICategoryTileJson, manager: CategoriesManager) {
        this.pk = categoryTileJson.pk;
        this.name = categoryTileJson.fields.name;
        this.description = categoryTileJson.fields.description;
        this.archived = categoryTileJson.fields.archived;
        this.manager = manager;

        const template = (document.getElementById("category-tile-template") as HTMLTemplateElement)
            .content.querySelector(".category-tile.live");

        this.categoryTile = document.importNode<any>(template, true);
        this.categoryForm = this.categoryTile.querySelector("form") as HTMLFormElement;
        this.categoryNameEdit = this.categoryTile.querySelector(".category-name-edit") as HTMLInputElement;
        this.categoryDescriptionEdit = this.categoryTile.querySelector(".category-desc-edit") as HTMLTextAreaElement;
        this.categoryCanvasColorEdit =
            this.categoryTile.querySelector("select[id='id_canvas_color']") as HTMLSelectElement;
        this.categoryDeleteBt = this.categoryTile.querySelector('button[id="delete-category-bt"') as HTMLButtonElement;
        this.categoryArchiveBt =
            this.categoryTile.querySelector('button[id="archive-category-bt"') as HTMLButtonElement;
        this.categoryEditBt = this.categoryTile.querySelector('button[id="edit-category-bt"') as HTMLButtonElement;
        this.categoryArchivedEdit = this.categoryTile.querySelector('input[name="archived"') as HTMLInputElement;

        this.canvasColor = ColorsManager.instance().getColor(categoryTileJson.fields.canvas_color);

        this.populateTile();
        this.addTileEventListeners();
        this.manager.canvas.insertBefore(this.categoryTile, this.manager.placeholderNode);
    }

    /**
     * Function called when some information of the category has been changed and it needs to be refreshed.
     * @param categoryTileJson the JSON containing the new information.
     */
    public updateCategoryInformation(categoryTileJson: ICategoryTileJson) {
        if (this.pk !== categoryTileJson.pk)
            throw Error("Are you trying to substitute this category with another one? This is not possible");

        this.colorHasChanged = false;
        this.name = categoryTileJson.fields.name;
        this.description = categoryTileJson.fields.description;
        this.populateTile();
    }

    /**
     * Remove the current category tile
     */
    public removeTile(): void {
        this.destroying = true;
        this.manager.canvas.removeChild(this.categoryTile);
    }

    private addTileEventListeners(): void {
        this.categoryTile.onclick = () => window.location.href = `/tree/${this.pk}`;
    }

    private removeTileEventListeners(): void {
        this.categoryTile.onmousedown = (e: MouseEvent) => e.stopPropagation();
        this.categoryTile.onclick = null;
    }

    private setToEditMode(e: UIEvent) {
        e.preventDefault();
        e.stopPropagation();
        this.categoryTile.classList.remove("live");
        this.categoryTile.classList.add("edit");
        this.categoryNameEdit.focus();

        const setToLiveMode = (e1: UIEvent) => {
            if (!this.categoryTile.contains((e1.target) as any) || e1 instanceof KeyboardEvent) {
                document.removeEventListener("mousedown", setToLiveMode);
                this.categoryNameEdit.onkeydown = null;
                this.categoryDescriptionEdit.onkeydown = null;
                if (this.destroying) return; //the listener has been removed. We won't end up here anymore.
                this.addTileEventListeners();
                this.categoryTile.classList.remove("edit");
                this.categoryTile.classList.add("live");
                this.updateIfValueChanged();
            }
        };

        const setToLiveOnEnter = (e1: KeyboardEvent) => {
            if (e1.keyCode === 13) {
                e1.preventDefault();
                e1.stopPropagation();
                setToLiveMode(e1);
                return false;
            }
            return true;
        };

        this.removeTileEventListeners();
        document.addEventListener("mousedown", setToLiveMode);
        this.categoryNameEdit.addEventListener("keydown", setToLiveOnEnter);
        this.categoryDescriptionEdit.addEventListener("keydown", setToLiveOnEnter);
    }

    private updateIfValueChanged(): void {
        if (this.categoryNameEdit.value.trim() !== this.name ||
            this.categoryDescriptionEdit.value.trim() !== this.description ||
            this.colorHasChanged) {
            this.updateCategory();
        }
    }

    private populateTile() {
        // visible fields
        this.categoryTile.id = `category-tile-${this.pk}`;
        this.categoryTile.querySelector(".category-name")!.innerHTML = this.name;
        this.categoryTile.querySelector(".category-description")!.innerHTML = this.description;

        this.categoryForm.id = `category-edit-form-${this.pk}`;
        this.categoryNameEdit.value = this.name;
        this.categoryDescriptionEdit.value = this.description;
        this.categoryCanvasColorEdit.value = String(this.canvasColor.pk);
        this.categoryArchivedEdit.value = this.archived.toString();

        this.categoryDeleteBt.onclick = (e: UIEvent) => this.deleteCategory(e);
        this.categoryArchiveBt.onclick = (e: UIEvent) => this.toggleArchivedStatus(e);
        this.categoryEditBt.onclick = (e: UIEvent) => this.setToEditMode(e);
        this.categoryCanvasColorEdit.onchange = () => this.updateColorAndRepaint();
        this.repaintCategoryTile();
    }

    private async toggleArchivedStatus(e: UIEvent): Promise<void> {
        e.preventDefault();
        e.stopPropagation();
        const questionMessage = this.archived
            ? `Are you sure you want to move '${this.name}' out of the archive?`
            : `Are you sure you want to archive '${this.name}'?`;
        const confirmationMessage = this.archived
            ? `'${this.name}' has been removed from the archive.`
            : `'${this.name}' has been archived.`;

        if (!await PopupDialog.confirm(questionMessage)) return;
        this.archived = !this.archived;
        this.categoryArchivedEdit.value = this.archived.toString();
        this.updateCategory();
        this.manager.removeCategory(this);
        MessageManager.info(confirmationMessage);
    }

    private updateColorAndRepaint() {
        const selectedColor = Number(this.categoryCanvasColorEdit.value);
        const newColor = ColorsManager.instance().getColor(selectedColor);

        if (newColor.c5 !== this.canvasColor.c5) {
            this.canvasColor = newColor;
            this.colorHasChanged = true;
            this.repaintCategoryTile();
        }
    }

    public repaintCategoryTile(): void {
        const textColor = this.canvasColor.text;
        this.categoryTile.style.color = textColor;
        this.categoryNameEdit.style.color = textColor;
        this.categoryNameEdit.style.borderColor = textColor;
        this.categoryDescriptionEdit.style.color = textColor;
        this.categoryDescriptionEdit.style.borderColor = textColor;
        this.categoryDeleteBt.querySelector("svg")!.style.fill = textColor;
        this.categoryArchiveBt.querySelector("svg")!.style.fill = textColor;
        this.categoryEditBt.querySelector("svg")!.style.fill = textColor;

        this.categoryTile.style.background = this.canvasColor.c3;
    }

    private updateCategory(): void {
        const form = $(`#category-edit-form-${this.pk}`);

        const ajaxSettings: JQueryAjaxSettings = {
            url: `/update_category/${this.pk}`,
            type: "POST",
            data: form.serialize(),
            success: (category: ICategoryTileJson[]) => {
                this.updateCategoryInformation(category[0]);
                this.repaintCategoryTile();
            },
            error: (err) => {
                console.log(err);
                this.populateTile(); //this resets the input fields
            }
        };
        $.ajax(ajaxSettings);
    };

    private async deleteCategory(e: UIEvent): Promise<void> {
        e.preventDefault();
        e.stopPropagation();
        if (!await PopupDialog.confirm("Are you sure you want to delete this category?")) return;
        if (!await PopupDialog.confirm("Really sure? All goals in it contained, progress and experience will be lost forever!")) return;

        const form = $(`#category-edit-form-${this.pk}`);
        $.ajax({
            type: "POST",
            url: `/delete_category/${this.pk}`,
            data: form.serialize(),
            success: () => {
                this.manager.removeCategory(this);
                UserLevelTracker.instance().checkForLevelChanges(true);
            },
            error: (error) => { console.log(error); }
        });
    }
}