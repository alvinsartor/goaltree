"use strict";
class DashboardManager {
    constructor() {
        this.panels = {};
        // add user panel
        const userStatsContainer = document.querySelector("div[tag='user']");
        const userId = userStatsContainer.getAttribute("data-id");
        this.userPanel = this.addUserPanel(userStatsContainer, Number(userId));
        this.userPanel.activate();
        // add categories panels
        const categoriesStatsContainers = document.querySelectorAll("div[tag='category']");
        Array.from(categoriesStatsContainers).forEach(container => {
            const categoryId = container.getAttribute("data-id");
            const categoryColorId = container.getAttribute("data-color");
            const categoryPanel = this.addCategoryPanel(container, Number(categoryId), Number(categoryColorId));
            categoryPanel.runPreAnalysis();
        });
    }
    addUserPanel(panelDiv, id) {
        const panel = new UserStatsPanel(this, panelDiv, id);
        this.panels[panel.id] = panel;
        return panel;
    }
    addCategoryPanel(panelDiv, id, colorId) {
        const panel = new CategoryStatsPanel(this, panelDiv, id, colorId);
        this.panels[panel.id] = panel;
        return panel;
    }
    /**
     * Function called by the panels when they are clicked on.
     * It deactivates all the other panels.
     * @param panelId the identifier of the panel that has just been activated.
     */
    panelHasBeenActivated(panelId) {
        Object.keys(this.panels)
            .filter(key => key !== panelId)
            .forEach(key => this.panels[key].deactivate());
    }
}
