"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
class StatsPanel {
    /**
     * Creates a new instance of StatsPanel
     * @param panel the dashboard manager.
     * @param panel the div element that will contain the statistics.
     */
    constructor(manager, container, identifier, colorIdentifier = 8) {
        this.timestampExpirationTime = 5000;
        this.showLoader = () => {
            this.loader.style.display = "block";
            this.analysisResultIcon.style.display = "none";
            this.panelTitle.onclick = () => { };
        };
        this.hideLoader = () => {
            this.loader.style.display = "none";
            this.analysisResultIcon.style.display = "block";
            this.panelTitle.onclick = () => this.panelClick();
        };
        this.manager = manager;
        this.container = container;
        this.panelTitle = container.querySelector("div[class='statistics-panel-title']");
        this.panelContent = container.querySelector("div[class='statistics-panel-content']");
        this.analysisResultIcon = container.querySelector("div[class='panel-analysis-result-icon']");
        this.loader = container.querySelector("img[class='panel-loader']");
        this.pk = identifier;
        this.colorIdentifier = colorIdentifier;
        this.isActive = false;
        this.canvases = new Array();
        this.panelTitle.onclick = () => this.panelClick();
        this.analysisResultIcon.onclick = (e) => e.stopImmediatePropagation();
        this.setColor();
    }
    panelClick() {
        if (this.isActive) {
            this.deactivate();
        }
        else {
            this.activate();
        }
    }
    /**
     * Activates this statistics panel.
     */
    runPreAnalysis() {
        return __awaiter(this, void 0, void 0, function* () {
            this.showLoader();
            const result = yield this.fetchDataAndRunAnalysis();
            this.setAnalysisResult(result);
            this.hideLoader();
        });
    }
    /**
     * Activates this statistics panel.
     */
    activate() {
        return __awaiter(this, void 0, void 0, function* () {
            if (this.isActive)
                return;
            this.showLoader();
            this.isActive = true;
            const result = yield this.fetchDataAddCanvasesAndRunAnalysis();
            this.setAnalysisResult(result);
            this.panelContent.style.maxHeight = this.container.scrollHeight + "px";
            this.container.parentElement.onresize = () => this.panelContent.style.maxHeight = this.container.scrollHeight + "px";
            this.manager.panelHasBeenActivated(this.id);
            this.hideLoader();
        });
    }
    /**
    * Deactivates this statistics panel.
    */
    deactivate() {
        if (!this.isActive)
            return;
        this.isActive = false;
        this.panelContent.style.maxHeight = "0";
        this.container.parentElement.onresize = null;
        this.removeAllCanvasesFromPanel();
    }
    setColor() {
        const color = ColorsManager.instance().getColor(this.colorIdentifier);
        this.container.style.color = color.text;
        this.panelTitle.style.background = color.c3;
        this.panelTitle.style.color = color.text;
        this.panelContent.style.background = color.c1;
        this.panelContent.style.color = "#242424";
    }
    setAnalysisResult(result) {
        this.analysisResultIcon.innerHTML = result.icon;
        if (result.title === "")
            return;
        const tooltip = document.createElement("span");
        tooltip.innerHTML = `<span>${result.title.replace("\n", "<br>")}</span>`;
        tooltip.classList.add("tooltip-block");
        tooltip.style.width = "max-content";
        tooltip.style.textAlign = "left";
        this.analysisResultIcon.appendChild(tooltip);
    }
    /**
     * Given a timestamp in seconds, it checks whether it is too old and the data needs to be reloaded.
     * @param timestamp the timestamp in seconds that has to be checked.
     */
    isTimestampExpired(timestamp) {
        const now = Math.floor(Date.now() / 1000);
        return timestamp < now - this.timestampExpirationTime;
    }
    /**
     * Adds the given canvas to the panel collection.
     */
    addCanvasToPanel(canvas) { this.canvases.push(canvas); }
    /**
     * Removes the canvases from the DOM and cleans the container referencing them.
     */
    removeAllCanvasesFromPanel() {
        this.canvases.forEach(c => c.remove());
        this.canvases = new Array();
    }
}
