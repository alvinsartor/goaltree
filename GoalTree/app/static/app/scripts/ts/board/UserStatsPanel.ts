
class UserStatsPanel extends StatsPanel {

    private data: IUserStats | null = null;
    private userStatsExtractors: IUserStatsExtractor[] = [
        new UserLevelStatsExtractor(),
        new ExperienceStatsExtractor(),
        new HotCategoriesStatsExtractor(),
    ];
    private goalStatsExtractors: IGoalsStatsExtractor[] = [
        new NoGoalsStatsExtractor(),
        new CompletionStatsExtractor(),
        new AdditionTrendExtractor(),
        new CompletionTrendExtractor(),
        new DistributionStatsExtractor(),
        new LatestGoalsExtractor(),
    ];

    /** @inheritDoc */
    public get id(): string { return `usr${this.pk}`; }

    /**
     * Creates a new instance of UserStatsPanel.
     * @param manager the dashboard manager.
     * @param panel the panel this statistic is bind to.
     * @param userPk the identifier of the user. Used for data retrieval.
     */
    public constructor(manager: DashboardManager, panel: HTMLDivElement, userPk: number) {
        super(manager, panel, userPk);
    }

    /** @inheritDoc */
    protected async fetchDataAndRunAnalysis(): Promise<AnalysisResult> {
        if (this.data === null || this.isTimestampExpired(this.data.timestamp)) {
            this.data = await $.getJSON(`user_stats/${this.pk}`);
        }

        const results1 = this.userStatsExtractors.map(e => e.extractStatistics(this.data!));
        const results2 = this.goalStatsExtractors.map(e => e.extractStatistics(this.data!));
        return AnalysisResult.fromResultCollection(results1.concat(results2));
    }

    /** @inheritDoc */
    protected async fetchDataAddCanvasesAndRunAnalysis(): Promise<AnalysisResult> {
        if (this.data === null || this.isTimestampExpired(this.data.timestamp)) {
            this.data = await $.getJSON(`user_stats/${this.pk}`);
        }

        const userCharts = this.userStatsExtractors
            .filter(e => e.isApplicable(this.data!))
            .map(e => e.createChart(this.panelContent, this.data!));
        userCharts.forEach(c => this.addCanvasToPanel(c));

        const goalCharts = this.goalStatsExtractors
            .filter(e => e.isApplicable(this.data!))
            .map(e => e.createChart(this.panelContent, this.data!));
        goalCharts.forEach(c => this.addCanvasToPanel(c));

        const results = userCharts.map(c => c.result).concat(goalCharts.map(c => c.result));
        return AnalysisResult.fromResultCollection(results);
    };


}