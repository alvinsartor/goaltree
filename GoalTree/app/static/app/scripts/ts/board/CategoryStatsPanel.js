"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
class CategoryStatsPanel extends StatsPanel {
    /**
     * Creates a new instance of CategoryStatsPanel.
     * @param manager the dashboard manager.
     * @param panel the panel this statistic is bind to.
     * @param categoryPk the identifier of the category. Used for data retrieval.
     * @param categoryColorPk the identifier of the category color. Used to set the panel background.
     */
    constructor(manager, panel, categoryPk, categoryColorPk) {
        super(manager, panel, categoryPk, categoryColorPk);
        this.data = null;
        this.goalStatsExtractors = [
            new NoGoalsStatsExtractor(),
            new CompletionStatsExtractor(),
            new AdditionTrendExtractor(),
            new CompletionTrendExtractor(),
            new DistributionStatsExtractor(),
            new LatestGoalsExtractor(),
        ];
    }
    /** @inheritDoc */
    get id() { return `cat${this.pk}`; }
    /** @inheritDoc */
    fetchDataAndRunAnalysis() {
        return __awaiter(this, void 0, void 0, function* () {
            if (this.data === null || this.isTimestampExpired(this.data.timestamp)) {
                this.data = yield $.getJSON(`category_stats/${this.pk}`);
            }
            const results = this.goalStatsExtractors.map(e => e.extractStatistics(this.data));
            return AnalysisResult.fromResultCollection(results);
        });
    }
    /** @inheritDoc */
    fetchDataAddCanvasesAndRunAnalysis() {
        return __awaiter(this, void 0, void 0, function* () {
            if (this.data === null || this.isTimestampExpired(this.data.timestamp)) {
                this.data = yield $.getJSON(`category_stats/${this.pk}`);
            }
            const charts = this.goalStatsExtractors
                .filter(e => e.isApplicable(this.data))
                .map(e => e.createChart(this.panelContent, this.data));
            charts.forEach(c => this.addCanvasToPanel(c));
            const results = charts.map(c => c.result);
            return AnalysisResult.fromResultCollection(results);
        });
    }
    ;
}
