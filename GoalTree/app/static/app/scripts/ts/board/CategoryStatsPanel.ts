
class CategoryStatsPanel extends StatsPanel {

    private data: IGoalsStats | null = null;
    private goalStatsExtractors: IGoalsStatsExtractor[] = [
        new NoGoalsStatsExtractor(),
        new CompletionStatsExtractor(),
        new AdditionTrendExtractor(),
        new CompletionTrendExtractor(),
        new DistributionStatsExtractor(),
        new LatestGoalsExtractor(),
    ];

    /** @inheritDoc */
    public get id(): string { return `cat${this.pk}`; }

    /**
     * Creates a new instance of CategoryStatsPanel.
     * @param manager the dashboard manager.
     * @param panel the panel this statistic is bind to.
     * @param categoryPk the identifier of the category. Used for data retrieval.
     * @param categoryColorPk the identifier of the category color. Used to set the panel background.
     */
    public constructor(manager: DashboardManager, panel: HTMLDivElement, categoryPk: number, categoryColorPk: number) {
        super(manager, panel, categoryPk, categoryColorPk);
    }

    /** @inheritDoc */
    protected async fetchDataAndRunAnalysis(): Promise<AnalysisResult> {
        if (this.data === null || this.isTimestampExpired(this.data.timestamp)) {
            this.data = await $.getJSON(`category_stats/${this.pk}`);
        }

        const results = this.goalStatsExtractors.map(e => e.extractStatistics(this.data!));
        return AnalysisResult.fromResultCollection(results);
    }

    /** @inheritDoc */
    protected async fetchDataAddCanvasesAndRunAnalysis(): Promise<AnalysisResult> {
        if (this.data === null || this.isTimestampExpired(this.data.timestamp)) {
            this.data = await $.getJSON(`category_stats/${this.pk}`);
        }

        const charts = this.goalStatsExtractors
            .filter(e => e.isApplicable(this.data!))
            .map(e => e.createChart(this.panelContent, this.data!));
        charts.forEach(c => this.addCanvasToPanel(c));

        const results = charts.map(c => c.result);
        return AnalysisResult.fromResultCollection(results);
    };

}