
/**
 * Extracts the data from the IGoalsStats and returns a panel containing a line chart
 * describing the trend of goal completion over time.
 */
class CompletionTrendExtractor implements IGoalsStatsExtractor {

    /** @inheritdoc  */
    public isApplicable(goalStats: IGoalsStats): boolean {
        return goalStats.completed > 0;
    }

    /** @inheritdoc  */
    public extractStatistics(goalStats: IGoalsStats): AnalysisResult {
        if (!this.isApplicable(goalStats))
            return AnalysisResult.none();

        const dataPoints = goalStats.goals_completion_sum.length;
        const goalsToday = goalStats.goals_completion_sum[dataPoints - 1][1];
        const goals1MonthAgo = goalStats.goals_completion_sum[Math.floor(dataPoints / 2)][1];

        if (goalsToday === goals1MonthAgo)
            return AnalysisResult.warning("Stagnating situation - completion",
                "No goals have been completed in the last month. Splitting goals to smaller ones makes it easier to achieve them.\n\nIf you do not plan working on this category, consider archiving it.");
        else
            return AnalysisResult.success();
    }

    /** @inheritdoc  */
    public createChart(canvasFrame: HTMLElement, goalStats: IGoalsStats): StatsCanvas {
        const canvas = new StatsCanvas(canvasFrame, "Completion Trend");

        const data = {
            datasets: [
                {
                    data: goalStats.goals_completion_sum.map(x => x[1]),
                    borderColor: "#66bb6a",
                    backgroundColor: "rgba(102, 187, 106, 0.5)",
                    borderWidth: 3,
                    pointRadius: 2,
                    tension: 0,
                }
            ],
            labels: goalStats.goals_completion_sum.map(x => x[0]),
        };
        const options = {
            legend: { display: false },
        };

        const chart = new Chart(canvas.chartCanvas, { type: "line", data: data, options: options });
        canvas.setAnalysisResult(this.extractStatistics(goalStats));
        return canvas;
    }
}