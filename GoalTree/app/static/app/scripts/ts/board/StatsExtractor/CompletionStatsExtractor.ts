
/**
 * Extracts the data from the IGoalsStats and returns a panel containing a doughnut chart
 * describing the general completion state for a category or a group of categories.
 */
class CompletionStatsExtractor implements IGoalsStatsExtractor {

    /** @inheritdoc  */
    public isApplicable(goalStats: IGoalsStats): boolean {
        return goalStats.goals_count > 0;
    }

    /** @inheritdoc  */
    public extractStatistics(goalStats: IGoalsStats): AnalysisResult {
        if (!this.isApplicable(goalStats))
            return AnalysisResult.none();

        if (goalStats.open > goalStats.goals_count * 0.1 || goalStats.open > 10)
            return AnalysisResult.success();

        return AnalysisResult.warning("Too little choice",
            "It seems you do not have many open goals, try add some more.");
    }

    /** @inheritdoc  */
    public createChart(canvasFrame: HTMLElement, goalStats: IGoalsStats): StatsCanvas {
        const canvas = new StatsCanvas(canvasFrame, "Completion");

        const data = {
            datasets: [
                {
                    data: [goalStats.completed, goalStats.open],
                    backgroundColor: ["#66bb6a", "#4fc3f7"]
                }
            ],
            labels: ["Completed", "Open",]
        };

        const chart = new Chart(canvas.chartCanvas, { type: "doughnut", data: data, });
        canvas.setAnalysisResult(this.extractStatistics(goalStats));
        return canvas;
    }
}