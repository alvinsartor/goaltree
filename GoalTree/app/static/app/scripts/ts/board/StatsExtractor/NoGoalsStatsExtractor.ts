
/**
 * This extractor is meant to be shown as a placeholder when no goals have been added,
 * thus other charts cannot be created.
 */
class NoGoalsStatsExtractor implements IGoalsStatsExtractor {

    /** @inheritdoc  */
    public isApplicable(goalStats: IGoalsStats): boolean {
        return goalStats.goals_count === 0;
    }

    /** @inheritdoc  */
    public extractStatistics(goalStats: IGoalsStats): AnalysisResult {
        if (!this.isApplicable(goalStats))
            return AnalysisResult.none();

        return AnalysisResult.warning("Category still empty",
            "No goals have been added to this category.");
    }

    /** @inheritdoc  */
    public createChart(canvasFrame: HTMLElement, goalStats: IGoalsStats): StatsCanvas {
        const canvas = new StatsCanvas(canvasFrame, "Empty Category");
        const container = canvas.chartCanvasContainer;
        container.removeChild(canvas.chartCanvas);

        container.innerHTML =
            ` 
<div class="no-chart-canvas">
    <div class="font-weight-normal">
        <label>No goals here.</label>
    </div>
    <div class="font-weight-normal">
        <label>Add some goals to be able to see more graphs.</label>
    </div>    
    <div class="font-weight-normal">
        <img width="150px" src="https://i.imgur.com/Wuz9ujm.png" alt="empty-category" />
    </div>
</div>`;

        canvas.setAnalysisResult(this.extractStatistics(goalStats));
        return canvas;
    }
}