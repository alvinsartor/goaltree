class LatestGoalsExtractor implements  IGoalsStatsExtractor {

    /** @inheritdoc  */
    public isApplicable(goalStats: IGoalsStats): boolean
    {
        return goalStats.completed > 0;
    }

    /** @inheritdoc  */
    public extractStatistics(goalStats: IGoalsStats): AnalysisResult
    {
        if (!this.isApplicable(goalStats))
            return AnalysisResult.none();

        return AnalysisResult.success();
    }

    /** @inheritdoc  */
    public createChart(canvasFrame: HTMLElement, goalStats: IGoalsStats): StatsCanvas
    {
        const canvas = new StatsCanvas(canvasFrame, "Latest Completed Goals");
        const container = canvas.chartCanvasContainer;
        container.removeChild(canvas.chartCanvas);
        container.classList.add("latest-completed-canvas");

        this.addUntilFull(goalStats.latest_completed_goals, container);

        canvas.setAnalysisResult(this.extractStatistics(goalStats));
        return canvas;
    }

    private addUntilFull(goals: ICompletedGoal[], container: HTMLDivElement): void {
        let tile: HTMLDivElement | null = null;
        for (let i = 0; i < goals.length && !this.isContainerFull(tile); i++)
        {
            tile = this.addCompletedGoalTile(goals[i]);
            container.appendChild(tile);
        }

        // if we added to many tiles we remove the latest
        if (this.isContainerFull(tile)) container.removeChild(tile!);
    }

    private isContainerFull(tile: HTMLElement | null)
    {
        return tile !== null && tile.offsetLeft > 400;
    }

    private addCompletedGoalTile(goal: ICompletedGoal): HTMLDivElement {
        const completedGoalTile = document.createElement("div") as HTMLDivElement;
        completedGoalTile.classList.add("completed-goal-tile");
        completedGoalTile.style.background = goal.backgroundColor;
        completedGoalTile.style.color = goal.textColor;

        const goalTitleSpan = document.createElement("span") as HTMLSpanElement;
        goalTitleSpan.innerText = this.clampText(goal.title, 50);

        completedGoalTile.appendChild(goalTitleSpan);
        return completedGoalTile;
    }

    private clampText(text: string, maxLength: number): string {
        return text.length <= maxLength
            ? text
            : text.substr(0, maxLength - 2) + "..";
    }
}