
enum AnalysisResultState { Success, Warning, None }

class AnalysisResult {

    private static readonly okIcon = `
<svg x="0px" y="0px" viewBox="0 0 504 504" xml:space="preserve">
    <circle style="fill:#3DB39E;" cx="252.06" cy="252.06" r="252.06"/>
    <path style="fill:#37A18E;" d="M463.163,114.609L240.246,345.403l0.394,24.812h10.24l241.428-194.56 C485.218,153.994,475.372,133.12,463.163,114.609z"/>
    <path style="fill:#F2F1EF;" d="M499.397,103.582l-44.505-44.111c-5.908-5.908-15.754-5.908-22.055,0L242.609,256l-82.314-81.132 c-5.908-5.908-15.754-5.908-22.055,0l-39.385,38.991c-5.908,5.908-5.908,15.754,0,21.662L230.4,365.883 c3.545,3.545,8.271,4.726,12.997,4.332c4.726,0.394,9.452-0.788,12.997-4.332l243.003-240.246 C505.305,119.335,505.305,109.489,499.397,103.582z"/>
    <path style="fill:#E6E5E3;" d="M256.394,365.883l243.003-240.246c5.908-5.908,5.908-15.754,0-21.662l-7.089-6.695L243.003,342.252 L105.157,207.951l-5.908,5.908c-5.908,5.908-5.908,15.754,0,21.662l131.545,130.363c3.545,3.545,8.271,4.726,12.997,4.332 C248.123,370.609,252.849,369.428,256.394,365.883z"/>
</svg> `;

    public static readonly warningIcon = `
<svg viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
    <path style="fill:#3B4145;" d="M322.939,62.642l178.737,309.583C508.231,383.578,512,396.74,512,410.791 c0,42.67-34.592,77.264-77.264,77.264H256L194.189,256L256,23.946C284.62,23.946,309.587,39.519,322.939,62.642z"/>
    <path style="fill:#525A61;" d="M189.061,62.642L10.323,372.225C3.769,383.578,0,396.74,0,410.791 c0,42.67,34.592,77.264,77.264,77.264H256V23.946C227.38,23.946,202.413,39.519,189.061,62.642z"/>
    <path style="fill:#FFB751;" d="M474.913,387.678L296.177,78.098c-8.056-13.959-22.849-22.767-38.848-23.22l152.869,402.275h24.539 c25.559,0,46.358-20.798,46.358-46.358C481.095,402.677,478.952,394.683,474.913,387.678z"/>
    <path style="fill:#FFD764;" d="M444.853,387.678c3.492,7.005,5.336,14.999,5.336,23.117c0,25.559-17.935,46.358-39.992,46.358 H77.264c-25.559,0-46.358-20.799-46.358-46.358c0-8.118,2.143-16.112,6.181-23.117l178.736-309.58 c8.283-14.34,23.674-23.251,40.177-23.251c0.443,0,0.886,0.01,1.329,0.031c13.732,0.536,26.414,9.323,33.326,23.22L444.853,387.678z "/>
    <path style="fill:#3B4145;" d="M256,354.131v51.509c14.227,0,25.755-11.528,25.755-25.755 C281.755,365.659,270.227,354.131,256,354.131z"/>
    <path style="fill:#525A61;" d="M256,354.131c2.843,0,5.151,11.528,5.151,25.755c0,14.227-2.308,25.755-5.151,25.755 c-14.227,0-25.755-11.528-25.755-25.755C230.245,365.659,241.773,354.131,256,354.131z"/>
    <path style="fill:#3B4145;" d="M256,132.646V323.23c14.227,0,25.755-11.538,25.755-25.755V158.401 C281.755,144.174,270.227,132.646,256,132.646z"/>
    <path style="fill:#525A61;" d="M256,132.646c2.843,0,5.151,11.528,5.151,25.755v139.074c0,14.216-2.308,25.755-5.151,25.755 c-14.227,0-25.755-11.538-25.755-25.755V158.401C230.245,144.174,241.773,132.646,256,132.646z"/>
</svg> `;

    /** The state resulting from the analysis. */
    public readonly state: AnalysisResultState;

    /** The text associated with the analysis result. */
    public readonly title: string;

    /** The text describing the analysis outcome. */
    public readonly text: string;

    /** The icon corresponding to the analysis state. */
    public get icon(): string {
        switch (this.state) {
        case AnalysisResultState.Success:
            return AnalysisResult.okIcon;
        case AnalysisResultState.Warning:
            return AnalysisResult.warningIcon;
        case AnalysisResultState.None:
            return "";
        default:
            throw new Error(`The state '${this.state}' is unknown and its icon cannot be retrieved.`);
        }
    }

    private constructor(state: AnalysisResultState, title: string, text: string) {
        this.state = state;
        this.title = title;
        this.text = text;
    }

    public combine(other: AnalysisResult): AnalysisResult
    {
        // if any of the two is none, we return the other one.
        if (this.state === AnalysisResultState.None) return other;
        if (other.state === AnalysisResultState.None) return this;

        // if both are success, we return success
        if (this.state === AnalysisResultState.Success && other.state === this.state) return this;

        // if both are warning, we join the messages
        if (this.state === AnalysisResultState.Warning && other.state === this.state) {
            const title = this.title + "\n" + other.title;
            return AnalysisResult.warning(title, "");
        }

        // one of the two must be warning. We return it.
        if (this.state === AnalysisResultState.Warning) return this;
        else return other;
    }

    /**
     * Combines a collection of results.
     * @param results the collection of results.
     */
    public static fromResultCollection(results: AnalysisResult[]): AnalysisResult {
        let result = AnalysisResult.none();
        results.forEach(r => result = result.combine(r));
        return result;
    } 

    /**
     * Creates an instance of AnalysisResult with 'None' as state.
     */
    public static none(): AnalysisResult {
        return new AnalysisResult(AnalysisResultState.None, "", "");
    }

    /**
     * Creates an instance of AnalysisResult with 'Warning' as state.
     * @param title the result title. Used to sum up the problem.
     * @param text the text to show to the user.
     */
    public static warning(title: string, text: string): AnalysisResult {
        return new AnalysisResult(AnalysisResultState.Warning, title, text);
    }

    /**
     * Creates an instance of AnalysisResult with 'Success' as state.
     */
    public static success(): AnalysisResult
    {
        return new AnalysisResult(AnalysisResultState.Success, "", "");
    }

}