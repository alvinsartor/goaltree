"use strict";
/**
 * Extracts the data from the IUserStats and returns a panel containing a doughnut chart
 * describing how much experience points are available and how much have been earned until now.
 */
class ExperienceStatsExtractor {
    /** @inheritdoc  */
    isApplicable(userStats) {
        return userStats.experience_available > 0;
    }
    /** @inheritdoc  */
    extractStatistics(userStats) {
        if (!this.isApplicable(userStats))
            return AnalysisResult.none();
        if (userStats.experience_available > userStats.experience_gained * 2 / 3)
            return AnalysisResult.success();
        return AnalysisResult.warning("Not much experience left to gain", "There should always be plenty of experience ahead of you.\n\nTry adding more ambitious goals to make the game more interesting!");
    }
    /** @inheritdoc  */
    createChart(canvasFrame, userStats) {
        const canvas = new StatsCanvas(canvasFrame, "Experience");
        const data = {
            datasets: [
                {
                    data: [userStats.experience_gained, userStats.experience_available],
                    backgroundColor: ["#ffa726", "#ef5350"]
                }
            ],
            labels: ["Gained", "Available",]
        };
        const options = {
            cutoutPercentage: 50,
        };
        const chart = new Chart(canvas.chartCanvas, { type: "doughnut", data: data, options: options });
        canvas.setAnalysisResult(this.extractStatistics(userStats));
        return canvas;
    }
}
