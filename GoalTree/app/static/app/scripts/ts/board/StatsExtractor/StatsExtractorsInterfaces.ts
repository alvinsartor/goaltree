
interface ICompletedGoal {
    title: string,
    backgroundColor: string;
    textColor: string;
}


/**
 * Interface describing goal statistics.
 * It can refer to a specific category or to all categories together.
 */
interface IGoalsStats {
    timestamp: number;
    name: string;
    goals_count: number;
    completed: number;
    open: number;
    experience_gained: number;
    experience_available: number;
    goal_distribution: [number, number][];
    goals_completion_sum: [string, number][];
    goals_addition_sum: [string, number][];
    latest_completed_goals: ICompletedGoal[];
}

/**
 * Interface describing user level and experience statistics.
 */
interface IExperienceStats {
    level: number;
    experience: number;
    exp_needed_to_lv_up: number;
    level_completion_percentage: number;
}

/**
 * Interface describing user statistics.
 * It contains info about all the categories and some user-only data like level.
 */
interface IUserStats extends IExperienceStats, IGoalsStats {
    nr_categories: number;
    hot_categories: [string, number, number][];
}

/**
 * Extractor handling goal statistics.
 */
interface IGoalsStatsExtractor {

    /**
     * Defines whether it is possible to extract information from the given statistics.
     * @param goalStats the statistics to analyze.
     */
    isApplicable(goalStats: IGoalsStats): boolean;

    /**
     * Analyzes the given GoalStats and returns a result.
     * @param goalStats the statistics to analyze.
     */
    extractStatistics(goalStats: IGoalsStats): AnalysisResult;

    /**
     * Creates a chart using the given GoalStats and inject the result into it.
     * @param canvasFrame the container for the StatsCanvas that will be created.
     * @param goalStats the statistics used to create the chart and to perform the analysis.
     */
    createChart(canvasFrame: HTMLElement, goalStats: IGoalsStats): StatsCanvas;
}

/**
 * Extractor handling user statistics.
 */
interface IUserStatsExtractor {

    /**
     * Defines whether it is possible to extract information from the given statistics.
     * @param goalStats the statistics to analyze.
     */
    isApplicable(goalStats: IGoalsStats): boolean;

    /**
    * Analyzes the given UserStats and returns a result.
    * @param userStats the statistics to analyze.
    */
    extractStatistics(userStats: IUserStats): AnalysisResult;

    /**
     * Creates a chart using the given UserStats and inject the result into it.
     * @param canvasFrame the container for the StatsCanvas that will be created.
     * @param userStats the statistics used to create the chart and to perform the analysis.
     */
    createChart(canvasFrame: HTMLElement, userStats: IUserStats): StatsCanvas;
}