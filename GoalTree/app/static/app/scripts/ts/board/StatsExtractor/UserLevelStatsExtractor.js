"use strict";
/**
 * Extracts the data from the IUserStats and returns a panel containing the statistics
 * about the user level and its experience.
 */
class UserLevelStatsExtractor {
    /** @inheritdoc  */
    isApplicable(goalStats) { return true; }
    /** @inheritdoc  */
    extractStatistics(userStats) { return AnalysisResult.none(); }
    /** @inheritdoc  */
    createChart(canvasFrame, userStats) {
        const canvas = new StatsCanvas(canvasFrame, "General");
        const container = canvas.chartCanvasContainer;
        container.removeChild(canvas.chartCanvas);
        container.innerHTML =
            ` 
<div class="experience-canvas">
    <div class="experience-block level">
        <label>Level:</label>
        <label>${userStats.level}</label>
    </div>
  
    <div>
        <div class="progressbar experience-block">
            <div name='bar' style="width:0%"></div>
        </div>
    </div>
    <br>
    <div class="experience-block font-weight-normal">
        <label>Experience needed to level up:</label>
        <label>${userStats.exp_needed_to_lv_up}</label>
    </div>
    <div class="experience-block font-weight-normal">
        <label>Total gained experience:</label>
        <label>${userStats.experience_gained}</label>
    </div>
  
</div>`;
        // this setting the value with some delay activates the animation
        setTimeout(() => {
            const progressbar = container.querySelector("div[name='bar']");
            progressbar.style.width = `${userStats.level_completion_percentage * 100}%`;
        }, 50);
        canvas.setAnalysisResult(this.extractStatistics(userStats));
        return canvas;
    }
}
