"use strict";
/**
 * Extracts the data from the IUserStats and returns a panel containing a bar chart
 * describing the how many goals each category contains.
 */
class HotCategoriesStatsExtractor {
    /** @inheritdoc  */
    isApplicable(userStats) {
        return userStats.nr_categories >= 2;
    }
    /** @inheritdoc  */
    extractStatistics(userStats) {
        return AnalysisResult.success();
    }
    /** @inheritdoc  */
    createChart(canvasFrame, userStats) {
        const canvas = new StatsCanvas(canvasFrame, "Hot Categories");
        const data = {
            datasets: [
                {
                    label: "Open",
                    data: userStats.hot_categories.map(d => d[1]),
                    backgroundColor: "#4fc3f7",
                },
                {
                    label: "Completed",
                    data: userStats.hot_categories.map(d => d[2]),
                    backgroundColor: "#66bb6a",
                }
            ],
            labels: userStats.hot_categories.map(d => d[0]),
        };
        const options = {
            //legend: { display: false },
            scales: {
                xAxes: [{
                        stacked: true,
                    }],
                yAxes: [{
                        stacked: true
                    }]
            },
        };
        const chart = new Chart(canvas.chartCanvas, { type: "horizontalBar", data: data, options: options });
        canvas.setAnalysisResult(this.extractStatistics(userStats));
        return canvas;
    }
}
