
/**
 * Extracts the data from the IGoalsStats and returns a panel containing a line chart
 * describing the trend of goal addition over time.
 */
class AdditionTrendExtractor implements IGoalsStatsExtractor {

    /** @inheritdoc  */
    public isApplicable(goalStats: IGoalsStats): boolean {
        return goalStats.goals_count > 0;
    }

    /** @inheritdoc  */
    public extractStatistics(goalStats: IGoalsStats): AnalysisResult {
        if (!this.isApplicable(goalStats))
            return AnalysisResult.none();

        const dataPoints = goalStats.goals_addition_sum.length;
        const goalsToday = goalStats.goals_addition_sum[dataPoints - 1][1];
        const goals1MonthAgo = goalStats.goals_addition_sum[Math.floor(dataPoints / 2)][1];

        if (goalsToday === goals1MonthAgo)
            return AnalysisResult.warning("Stagnating situation - addition",
                "No goals have been added in the last month. Splitting goals to smaller ones makes it easier to achieve them.\n\nIf you do not plan working on this category, consider archiving it.");
        else
            return AnalysisResult.success();
    }

    /** @inheritdoc  */
    public createChart(canvasFrame: HTMLElement, goalStats: IGoalsStats): StatsCanvas {
        const canvas = new StatsCanvas(canvasFrame, "Addition Trend");

        const data = {
            datasets: [
                {
                    data: goalStats.goals_addition_sum.map(x => x[1]),
                    borderColor: "#4fc3f7",
                    backgroundColor: "rgba(79, 195, 247, 0.5)",
                    borderWidth: 3,
                    pointRadius: 2,
                    tension: 0,
                }
            ],
            labels: goalStats.goals_addition_sum.map(x => x[0]),
        };
        const options = {
            legend: { display: false },
        };

        const chart = new Chart(canvas.chartCanvas, { type: "line", data: data, options: options });
        canvas.setAnalysisResult(this.extractStatistics(goalStats));
        return canvas;
    }
}