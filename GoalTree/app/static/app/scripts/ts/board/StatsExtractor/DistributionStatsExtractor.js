"use strict";
/**
 * Extracts the data from the IGoalsStats and returns a panel containing a bar chart
 * describing the level distribution amongst the goals.
 */
class DistributionStatsExtractor {
    /** @inheritdoc  */
    isApplicable(goalStats) {
        return goalStats.goals_count > 0;
    }
    /** @inheritdoc  */
    extractStatistics(goalStats) {
        if (!this.isApplicable(goalStats))
            return AnalysisResult.none();
        const dist = goalStats.goal_distribution.map(x => x[1]);
        const group1 = dist[0] + dist[1] + dist[2]; // difficulty 1,2,3
        const group2 = dist[3] + dist[4] + dist[5]; // difficulty 4,5,6
        const group3 = dist[6] + dist[7] + dist[8]; // difficulty 7,8,9
        const group4 = dist[9]; // difficulty 10
        if (group4 > 0 && group4 >= group3 || group3 > 0 && group3 >= group2 || group2 > 0 && group2 >= group1) {
            return AnalysisResult.warning("Goals are not balanced", "Ideally you should have many goals of low levels and few ones of high levels. " +
                "Try to split big goals in smaller ones!");
        }
        return AnalysisResult.success();
    }
    /** @inheritdoc  */
    createChart(canvasFrame, goalStats) {
        const canvas = new StatsCanvas(canvasFrame, "Goals Levels Distribution");
        const data = {
            datasets: [
                {
                    data: goalStats.goal_distribution.map(d => d[1]),
                    backgroundColor: [
                        "#80cbc4", "#a5d6a7", "#c5e1a5", "#e6ee9c", "#fff59d", "#ffe082", "#ffcc80", "#ffab91",
                        "#ef9a9a", "#f48fb1", "#ce93d8"
                    ]
                }
            ],
            labels: goalStats.goal_distribution.map(d => d[0])
        };
        const options = {
            legend: { display: false },
            tooltips: { enabled: false },
        };
        const chart = new Chart(canvas.chartCanvas, { type: "bar", data: data, options: options });
        canvas.setAnalysisResult(this.extractStatistics(goalStats));
        return canvas;
    }
}
