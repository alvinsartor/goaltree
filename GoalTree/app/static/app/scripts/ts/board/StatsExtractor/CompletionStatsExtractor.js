"use strict";
/**
 * Extracts the data from the IGoalsStats and returns a panel containing a doughnut chart
 * describing the general completion state for a category or a group of categories.
 */
class CompletionStatsExtractor {
    /** @inheritdoc  */
    isApplicable(goalStats) {
        return goalStats.goals_count > 0;
    }
    /** @inheritdoc  */
    extractStatistics(goalStats) {
        if (!this.isApplicable(goalStats))
            return AnalysisResult.none();
        if (goalStats.open > goalStats.goals_count * 0.1 || goalStats.open > 10)
            return AnalysisResult.success();
        return AnalysisResult.warning("Too little choice", "It seems you do not have many open goals, try add some more.");
    }
    /** @inheritdoc  */
    createChart(canvasFrame, goalStats) {
        const canvas = new StatsCanvas(canvasFrame, "Completion");
        const data = {
            datasets: [
                {
                    data: [goalStats.completed, goalStats.open],
                    backgroundColor: ["#66bb6a", "#4fc3f7"]
                }
            ],
            labels: ["Completed", "Open",]
        };
        const chart = new Chart(canvas.chartCanvas, { type: "doughnut", data: data, });
        canvas.setAnalysisResult(this.extractStatistics(goalStats));
        return canvas;
    }
}
