"use strict";
class LatestGoalsExtractor {
    /** @inheritdoc  */
    isApplicable(goalStats) {
        return goalStats.completed > 0;
    }
    /** @inheritdoc  */
    extractStatistics(goalStats) {
        if (!this.isApplicable(goalStats))
            return AnalysisResult.none();
        return AnalysisResult.success();
    }
    /** @inheritdoc  */
    createChart(canvasFrame, goalStats) {
        const canvas = new StatsCanvas(canvasFrame, "Latest Completed Goals");
        const container = canvas.chartCanvasContainer;
        container.removeChild(canvas.chartCanvas);
        container.classList.add("latest-completed-canvas");
        this.addUntilFull(goalStats.latest_completed_goals, container);
        canvas.setAnalysisResult(this.extractStatistics(goalStats));
        return canvas;
    }
    addUntilFull(goals, container) {
        let tile = null;
        for (let i = 0; i < goals.length && !this.isContainerFull(tile); i++) {
            tile = this.addCompletedGoalTile(goals[i]);
            container.appendChild(tile);
        }
        // if we added to many tiles we remove the latest
        if (this.isContainerFull(tile))
            container.removeChild(tile);
    }
    isContainerFull(tile) {
        return tile !== null && tile.offsetLeft > 400;
    }
    addCompletedGoalTile(goal) {
        const completedGoalTile = document.createElement("div");
        completedGoalTile.classList.add("completed-goal-tile");
        completedGoalTile.style.background = goal.backgroundColor;
        completedGoalTile.style.color = goal.textColor;
        const goalTitleSpan = document.createElement("span");
        goalTitleSpan.innerText = this.clampText(goal.title, 50);
        completedGoalTile.appendChild(goalTitleSpan);
        return completedGoalTile;
    }
    clampText(text, maxLength) {
        return text.length <= maxLength
            ? text
            : text.substr(0, maxLength - 2) + "..";
    }
}
