"use strict";
class StatsCanvas {
    /**
     * Creates a new instance of StatsCanvas and attaches it on the DOM.
     * @param frame the container of the canvas.
     */
    constructor(frame, title) {
        this.analysisResult = AnalysisResult.none();
        const canvasTemplate = document.getElementById("stats-canvas-template")
            .content.querySelector(".stats-canvas");
        this.frame = frame;
        this.canvasContainer = document.importNode(canvasTemplate, true);
        this.canvasTitle = this.canvasContainer.querySelector("label");
        this.canvasContent = this.canvasContainer.querySelector(".stats-canvas-content");
        this.canvas = this.canvasContainer.querySelector("canvas");
        this.analysisResultIcon = this.canvasContainer.querySelector("div[class='analysis-result-icon']");
        this.canvasTitle.innerText = title;
        frame.appendChild(this.canvasContainer);
    }
    get result() { return this.analysisResult; }
    /**
     * Gets the canvas where charts should be added.
     */
    get chartCanvas() { return this.canvas; }
    /**
     * Gets the canvas parent.
     */
    get chartCanvasContainer() { return this.canvasContent; }
    /**
     * Removes the current canvas from the DOM.
     */
    remove() {
        this.frame.removeChild(this.canvasContainer);
    }
    /**
     * Sets the result and the text associated with the analysis of the data displayed on this canvas.
     * @param result the analysis result.
     * @param text the text that will be displayed in a tooltip.
     */
    setAnalysisResult(result) {
        this.analysisResult = result;
        this.analysisResultIcon.innerHTML = result.icon;
        if (result.title === "")
            return;
        const tooltip = document.createElement("div");
        tooltip.innerHTML = `<span class="tooltip-block-title">${result.title}</span><br><br><span>${result.text}</span>`;
        tooltip.classList.add("tooltip-block");
        this.analysisResultIcon.appendChild(tooltip);
    }
}
