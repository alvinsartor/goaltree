
class StatsCanvas
{
    private readonly frame: HTMLElement;
    private readonly canvasContainer : HTMLDivElement;
    private readonly canvasTitle : HTMLLabelElement;
    private readonly canvasContent : HTMLDivElement;
    private readonly canvas : HTMLCanvasElement;
    private readonly analysisResultIcon: HTMLDivElement;

    private analysisResult = AnalysisResult.none();
    public get result() { return this.analysisResult; }

    /**
     * Gets the canvas where charts should be added.
     */
    public get chartCanvas() { return this.canvas; }

    /**
     * Gets the canvas parent.
     */
    public get chartCanvasContainer() { return this.canvasContent; }

    /**
     * Creates a new instance of StatsCanvas and attaches it on the DOM.
     * @param frame the container of the canvas.
     */
    public constructor(frame: HTMLElement, title: string) {
        const canvasTemplate = (document.getElementById("stats-canvas-template") as HTMLTemplateElement)
            .content.querySelector(".stats-canvas");

        this.frame = frame;
        this.canvasContainer = document.importNode<any>(canvasTemplate, true);
        this.canvasTitle = this.canvasContainer.querySelector("label") as HTMLLabelElement;
        this.canvasContent = this.canvasContainer.querySelector(".stats-canvas-content") as HTMLDivElement;
        this.canvas = this.canvasContainer.querySelector("canvas") as HTMLCanvasElement;
        this.analysisResultIcon = this.canvasContainer.querySelector("div[class='analysis-result-icon']") as HTMLDivElement;

        this.canvasTitle.innerText = title;
        frame.appendChild(this.canvasContainer);
    }

    /**
     * Removes the current canvas from the DOM.
     */
    public remove() {
        this.frame.removeChild(this.canvasContainer);
    }

    /**
     * Sets the result and the text associated with the analysis of the data displayed on this canvas.
     * @param result the analysis result.
     * @param text the text that will be displayed in a tooltip.
     */
    public setAnalysisResult(result: AnalysisResult) {
        this.analysisResult = result;
        this.analysisResultIcon.innerHTML = result.icon;
        if (result.title === "") return;

        const tooltip = document.createElement("div");
        tooltip.innerHTML = `<span class="tooltip-block-title">${result.title}</span><br><br><span>${result.text}</span>`;
        tooltip.classList.add("tooltip-block");
        this.analysisResultIcon.appendChild(tooltip);
    }
}