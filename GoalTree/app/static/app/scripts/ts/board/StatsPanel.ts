
abstract class StatsPanel {

    private readonly timestampExpirationTime: number = 5000;

    private readonly manager: DashboardManager;
    private readonly container: HTMLDivElement;
    private readonly panelTitle: HTMLDivElement;
    protected readonly panelContent: HTMLDivElement;
    private readonly analysisResultIcon: HTMLDivElement;
    private readonly loader: HTMLElement;
    private readonly colorIdentifier: number;
    protected readonly pk: number;
    private isActive: boolean;

    private canvases: StatsCanvas[];

    /**
     * Gets the panel identifier.
     */
    public abstract get id(): string;

    /**
     * Creates a new instance of StatsPanel
     * @param panel the dashboard manager.
     * @param panel the div element that will contain the statistics.
     */
    protected constructor(manager: DashboardManager,
        container: HTMLDivElement,
        identifier: number,
        colorIdentifier: number = 8) {
        this.manager = manager;
        this.container = container;
        this.panelTitle = container.querySelector("div[class='statistics-panel-title']") as HTMLDivElement;
        this.panelContent = container.querySelector("div[class='statistics-panel-content']") as HTMLDivElement;
        this.analysisResultIcon = container.querySelector("div[class='panel-analysis-result-icon']") as HTMLDivElement;
        this.loader = container.querySelector("img[class='panel-loader']") as HTMLElement;
        this.pk = identifier;
        this.colorIdentifier = colorIdentifier;
        this.isActive = false;
        this.canvases = new Array<StatsCanvas>();

        this.panelTitle.onclick = () => this.panelClick();
        this.analysisResultIcon.onclick = (e: UIEvent) => e.stopImmediatePropagation();
        this.setColor();
    }

    private showLoader = () => {
        this.loader.style.display = "block";
        this.analysisResultIcon.style.display = "none";
        this.panelTitle.onclick = () => { };
    }
    private hideLoader = () => {
        this.loader.style.display = "none";
        this.analysisResultIcon.style.display = "block";
        this.panelTitle.onclick = () => this.panelClick();
    }

    private panelClick() {
        if (this.isActive) {
            this.deactivate();
        } else {
            this.activate();
        }
    }

    /**
     * Activates this statistics panel.
     */
    public async runPreAnalysis(): Promise<void> {
        this.showLoader();
        const result = await this.fetchDataAndRunAnalysis();
        this.setAnalysisResult(result);
        this.hideLoader();
    }

    /**
     * Activates this statistics panel.
     */
    public async activate(): Promise<void> {
        if (this.isActive) return;

        this.showLoader();
        this.isActive = true;
        const result = await this.fetchDataAddCanvasesAndRunAnalysis();
        this.setAnalysisResult(result);
        this.panelContent.style.maxHeight = this.container.scrollHeight + "px";
        this.container.parentElement!.onresize = () => this.panelContent.style.maxHeight = this.container.scrollHeight + "px";
        this.manager.panelHasBeenActivated(this.id);
        this.hideLoader();
    }

    /**
    * Deactivates this statistics panel.
    */
    public deactivate(): void {
        if (!this.isActive) return;

        this.isActive = false;
        this.panelContent.style.maxHeight = "0";
        this.container.parentElement!.onresize = null;
        this.removeAllCanvasesFromPanel();
    }

    /**
     * This function runs in 2 steps:
     * 1. If the data is not cached, it is fetched. 
     * 2. The analysis are run and the results are shown in the panel.
     */
    protected abstract fetchDataAndRunAnalysis(): Promise<AnalysisResult>;

    /**
     * This function runs in 3 steps:
     * 1. If the data is not cached, it is fetched.
     * 2. Adds the canvases associated to this StatsPanel.
     * 3. The analysis are run and the results are shown in the panel.
     */
    protected abstract fetchDataAddCanvasesAndRunAnalysis(): Promise<AnalysisResult>;


    private setColor() {
        const color = ColorsManager.instance().getColor(this.colorIdentifier);

        this.container.style.color = color.text;
        this.panelTitle.style.background = color.c3;
        this.panelTitle.style.color = color.text;

        this.panelContent.style.background = color.c1;
        this.panelContent.style.color = "#242424";
    }

    private setAnalysisResult(result: AnalysisResult): void {
        this.analysisResultIcon.innerHTML = result.icon;
        if (result.title === "") return;

        const tooltip = document.createElement("span");
        tooltip.innerHTML = `<span>${result.title.replace("\n", "<br>")}</span>`;
        tooltip.classList.add("tooltip-block");
        tooltip.style.width = "max-content";
        tooltip.style.textAlign = "left";
        this.analysisResultIcon.appendChild(tooltip);
    }

    /**
     * Given a timestamp in seconds, it checks whether it is too old and the data needs to be reloaded.
     * @param timestamp the timestamp in seconds that has to be checked.
     */
    protected isTimestampExpired(timestamp: number) {
        const now = Math.floor(Date.now() / 1000);
        return timestamp < now - this.timestampExpirationTime;
    }

    /**
     * Adds the given canvas to the panel collection.
     */
    protected addCanvasToPanel(canvas: StatsCanvas) { this.canvases.push(canvas); }

    /**
     * Removes the canvases from the DOM and cleans the container referencing them.
     */
    protected removeAllCanvasesFromPanel()
    {
        this.canvases.forEach(c => c.remove());
        this.canvases = new Array<StatsCanvas>();
    }
}