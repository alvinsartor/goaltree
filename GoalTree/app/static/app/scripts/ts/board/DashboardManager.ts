class DashboardManager {

    private readonly userPanel: UserStatsPanel;
    private readonly panels: { [identifier: string]: StatsPanel; } = {};

    public constructor() {
        // add user panel
        const userStatsContainer = document.querySelector("div[tag='user']") as HTMLDivElement;
        const userId = userStatsContainer.getAttribute("data-id");
        this.userPanel = this.addUserPanel(userStatsContainer, Number(userId));
        this.userPanel.activate();

        // add categories panels
        const categoriesStatsContainers = document.querySelectorAll("div[tag='category']");
        Array.from(categoriesStatsContainers).forEach(container => {
            const categoryId = container.getAttribute("data-id");
            const categoryColorId = container.getAttribute("data-color");
            const categoryPanel = this.addCategoryPanel(container as HTMLDivElement, Number(categoryId), Number(categoryColorId));
            categoryPanel.runPreAnalysis();
        });
    }

    private addUserPanel(panelDiv: HTMLDivElement, id: number): UserStatsPanel {
        const panel = new UserStatsPanel(this, panelDiv, id);
        this.panels[panel.id] = panel;
        return panel;
    }

    private addCategoryPanel(panelDiv: HTMLDivElement, id: number, colorId: number): CategoryStatsPanel {
        const panel = new CategoryStatsPanel(this, panelDiv, id, colorId);
        this.panels[panel.id] = panel;
        return panel;
    }

    /**
     * Function called by the panels when they are clicked on.
     * It deactivates all the other panels.
     * @param panelId the identifier of the panel that has just been activated.
     */
    public panelHasBeenActivated(panelId: string): void {
        Object.keys(this.panels)
            .filter(key => key !== panelId)
            .forEach(key => this.panels[key].deactivate());
    }
}