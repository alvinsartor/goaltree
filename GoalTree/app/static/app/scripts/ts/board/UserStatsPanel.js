"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
class UserStatsPanel extends StatsPanel {
    /**
     * Creates a new instance of UserStatsPanel.
     * @param manager the dashboard manager.
     * @param panel the panel this statistic is bind to.
     * @param userPk the identifier of the user. Used for data retrieval.
     */
    constructor(manager, panel, userPk) {
        super(manager, panel, userPk);
        this.data = null;
        this.userStatsExtractors = [
            new UserLevelStatsExtractor(),
            new ExperienceStatsExtractor(),
            new HotCategoriesStatsExtractor(),
        ];
        this.goalStatsExtractors = [
            new NoGoalsStatsExtractor(),
            new CompletionStatsExtractor(),
            new AdditionTrendExtractor(),
            new CompletionTrendExtractor(),
            new DistributionStatsExtractor(),
            new LatestGoalsExtractor(),
        ];
    }
    /** @inheritDoc */
    get id() { return `usr${this.pk}`; }
    /** @inheritDoc */
    fetchDataAndRunAnalysis() {
        return __awaiter(this, void 0, void 0, function* () {
            if (this.data === null || this.isTimestampExpired(this.data.timestamp)) {
                this.data = yield $.getJSON(`user_stats/${this.pk}`);
            }
            const results1 = this.userStatsExtractors.map(e => e.extractStatistics(this.data));
            const results2 = this.goalStatsExtractors.map(e => e.extractStatistics(this.data));
            return AnalysisResult.fromResultCollection(results1.concat(results2));
        });
    }
    /** @inheritDoc */
    fetchDataAddCanvasesAndRunAnalysis() {
        return __awaiter(this, void 0, void 0, function* () {
            if (this.data === null || this.isTimestampExpired(this.data.timestamp)) {
                this.data = yield $.getJSON(`user_stats/${this.pk}`);
            }
            const userCharts = this.userStatsExtractors
                .filter(e => e.isApplicable(this.data))
                .map(e => e.createChart(this.panelContent, this.data));
            userCharts.forEach(c => this.addCanvasToPanel(c));
            const goalCharts = this.goalStatsExtractors
                .filter(e => e.isApplicable(this.data))
                .map(e => e.createChart(this.panelContent, this.data));
            goalCharts.forEach(c => this.addCanvasToPanel(c));
            const results = userCharts.map(c => c.result).concat(goalCharts.map(c => c.result));
            return AnalysisResult.fromResultCollection(results);
        });
    }
    ;
}
