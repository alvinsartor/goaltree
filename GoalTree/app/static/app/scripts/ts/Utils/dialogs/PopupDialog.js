"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
class PopupDialog {
    static getContainer() {
        if (PopupDialog.dialogComponents === null) {
            const container = document.getElementsByClassName("dialog-container")[0];
            const dialog = container.querySelector(".custom-dialog");
            const text = container.querySelector("span");
            const okBt = container.querySelector("#dialog-ok-button");
            const cancelBt = container.querySelector("#dialog-cancel-button");
            PopupDialog.dialogComponents = {
                container: container,
                dialog: dialog,
                text: text,
                okBt: okBt,
                cancelBt: cancelBt
            };
            okBt.onclick = () => PopupDialog.okButtonPressed = true;
            cancelBt.onclick = () => PopupDialog.cancelButtonPressed = true;
        }
        return PopupDialog.dialogComponents;
    }
    /**
     * Displays a dialog with a message.
     * The user will need to press a button in order to proceed.
     * @param message the message to display on the dialog.
     */
    static alert(message) {
        return __awaiter(this, void 0, void 0, function* () {
            if (PopupDialog.isDialogShown())
                throw new Error("The dialog is already shown!");
            const container = PopupDialog.getContainer();
            container.cancelBt.style.display = "none";
            container.text.textContent = message;
            PopupDialog.resetPressedVars();
            PopupDialog.showDialog();
            container.okBt.focus();
            yield PopupDialog.waitForButtonPressed();
            PopupDialog.hideDialog();
        });
    }
    /**
    * Displays a dialog with a question for the user.
    * The user will need to press a button in order to proceed.
    * @param message the message containing the question to display on the dialog.
    */
    static confirm(message) {
        return __awaiter(this, void 0, void 0, function* () {
            if (PopupDialog.isDialogShown())
                throw new Error("The dialog is already shown!");
            const container = PopupDialog.getContainer();
            container.cancelBt.style.display = "block";
            container.text.textContent = message;
            PopupDialog.resetPressedVars();
            PopupDialog.showDialog();
            container.okBt.focus();
            const result = yield PopupDialog.waitForButtonPressed();
            PopupDialog.hideDialog();
            return result;
        });
    }
    static resetPressedVars() {
        PopupDialog.okButtonPressed = false;
        PopupDialog.cancelButtonPressed = false;
    }
    static waitForButtonPressed() {
        return __awaiter(this, void 0, void 0, function* () {
            const sleep = (ms) => new Promise(resolve => setTimeout(resolve, ms));
            const getResult = () => PopupDialog.okButtonPressed ? true : PopupDialog.cancelButtonPressed ? false : null;
            do {
                yield sleep(50);
            } while (getResult() === null);
            return getResult();
        });
    }
}
PopupDialog.dialogComponents = null;
PopupDialog.isDialogShown = () => PopupDialog.getContainer().container.style.display === "block";
PopupDialog.showDialog = () => PopupDialog.getContainer().container.style.display = "block";
PopupDialog.hideDialog = () => PopupDialog.getContainer().container.style.display = "none";
