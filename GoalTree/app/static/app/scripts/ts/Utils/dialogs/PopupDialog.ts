

class PopupDialog {
    private static dialogComponents: IPopupDialog | null = null;
    private static okButtonPressed: boolean;
    private static cancelButtonPressed: boolean;

    private static getContainer(): IPopupDialog {
        if (PopupDialog.dialogComponents === null) {
            const container = document.getElementsByClassName("dialog-container")[0] as HTMLDivElement;
            const dialog = container.querySelector(".custom-dialog") as HTMLDivElement;
            const text = container.querySelector("span") as HTMLSpanElement;
            const okBt = container.querySelector("#dialog-ok-button") as HTMLButtonElement;
            const cancelBt = container.querySelector("#dialog-cancel-button") as HTMLButtonElement;

            PopupDialog.dialogComponents = {
                container: container,
                dialog: dialog,
                text: text,
                okBt: okBt,
                cancelBt: cancelBt
            };

            okBt.onclick = () => PopupDialog.okButtonPressed = true;
            cancelBt.onclick = () => PopupDialog.cancelButtonPressed = true;
        }
        return PopupDialog.dialogComponents;
    }

    private static isDialogShown = () => PopupDialog.getContainer().container.style.display === "block";
    private static showDialog = () => PopupDialog.getContainer().container.style.display = "block";
    private static hideDialog = () => PopupDialog.getContainer().container.style.display = "none";

    /**
     * Displays a dialog with a message.
     * The user will need to press a button in order to proceed.
     * @param message the message to display on the dialog.
     */
    public static async alert(message: string): Promise<void> {
        if (PopupDialog.isDialogShown()) throw new Error("The dialog is already shown!");

        const container = PopupDialog.getContainer();
        container.cancelBt.style.display = "none";
        container.text.textContent = message;
        PopupDialog.resetPressedVars();

        PopupDialog.showDialog();
        container.okBt.focus();
        await PopupDialog.waitForButtonPressed();
        PopupDialog.hideDialog();
    }

    /**
    * Displays a dialog with a question for the user.
    * The user will need to press a button in order to proceed.
    * @param message the message containing the question to display on the dialog.
    */
    public static async confirm(message: string): Promise<boolean> {
        if (PopupDialog.isDialogShown()) throw new Error("The dialog is already shown!");

        const container = PopupDialog.getContainer();
        container.cancelBt.style.display = "block";
        container.text.textContent = message;
        PopupDialog.resetPressedVars();

        PopupDialog.showDialog();
        container.okBt.focus();
        const result = await PopupDialog.waitForButtonPressed();
        PopupDialog.hideDialog();

        return result;
    }

    private static resetPressedVars() {
        PopupDialog.okButtonPressed = false;
        PopupDialog.cancelButtonPressed = false;
    }
    
    private static async waitForButtonPressed(): Promise<boolean> {
        const sleep = (ms: number): Promise<void> => new Promise(resolve => setTimeout(resolve, ms));
        const getResult = () => PopupDialog.okButtonPressed ? true : PopupDialog.cancelButtonPressed ? false : null;

        do {
            await sleep(50);
        } while (getResult() === null);

        return getResult()!;
    }

}