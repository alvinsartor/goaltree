
interface IDialogComponents
{
    container: HTMLDivElement;
    dialog: HTMLDivElement;
    okBt: HTMLButtonElement;
}

interface IPopupDialog extends IDialogComponents {
    cancelBt: HTMLButtonElement;
    text: HTMLSpanElement;
}

interface IAnnouncementDialog extends IDialogComponents {
    title: HTMLSpanElement;
    richText: HTMLDivElement;
}