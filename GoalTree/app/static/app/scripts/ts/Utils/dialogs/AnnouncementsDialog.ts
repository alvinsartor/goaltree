

class AnnouncementsDialog {
    private static dialogComponents: IAnnouncementDialog | null = null;
    private static okButton: HTMLButtonElement;
    private static okButtonPressed: boolean;

    private static getContainer(): IAnnouncementDialog
    {
        if (AnnouncementsDialog.dialogComponents === null)
        {
            const container = document.getElementById("announcement-dialog-container") as HTMLDivElement;
            const dialog = container.querySelector(".custom-dialog") as HTMLDivElement;
            const title = container.querySelector("span") as HTMLSpanElement;
            const richText = container.querySelector("div[class='announcement-dialog-text']") as HTMLDivElement;
            this.okButton = container.querySelector("#dialog-ok-button") as HTMLButtonElement;
            
            AnnouncementsDialog.dialogComponents = {
                container: container,
                dialog: dialog,
                title: title,
                richText: richText,
                okBt: this.okButton,
            };

            this.okButton.onclick = () => AnnouncementsDialog.okButtonPressed = true;
        }
        return AnnouncementsDialog.dialogComponents;
    }

    private static isDialogShown = () => AnnouncementsDialog.getContainer().container.style.display === "block";
    private static showDialog = () => AnnouncementsDialog.getContainer().container.style.display = "block";
    private static hideDialog = () => AnnouncementsDialog.getContainer().container.style.display = "none";

    /**
     * Displays a dialog with a message.
     * The user will need to press a button in order to proceed.
     * @param message the message to display on the dialog.
     */
    public static async show(title: string, richText: string, buttonText: string = "Ok", confetti: boolean = false): Promise<void>
    {
        if (AnnouncementsDialog.isDialogShown()) throw new Error("The announcement dialog is already shown!");
        this.okButton.innerText = buttonText;

        const container = AnnouncementsDialog.getContainer();
        container.title.textContent = title;
        container.richText.innerHTML= richText;
        AnnouncementsDialog.okButtonPressed = false;

        AnnouncementsDialog.showDialog();
        if (confetti) window.confetti.start(5000, 150, 150);

        container.okBt.focus();
        await AnnouncementsDialog.waitForButtonPressed();
        AnnouncementsDialog.hideDialog();
    }

    private static async waitForButtonPressed(): Promise<boolean>
    {
        const sleep = (ms: number): Promise<void> => new Promise(resolve => setTimeout(resolve, ms));
        const getResult = () => AnnouncementsDialog.okButtonPressed;

        do
        {
            await sleep(50);
        } while (!getResult());

        return getResult()!;
    }
}