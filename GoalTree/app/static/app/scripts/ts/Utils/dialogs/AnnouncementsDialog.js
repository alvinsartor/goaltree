"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
class AnnouncementsDialog {
    static getContainer() {
        if (AnnouncementsDialog.dialogComponents === null) {
            const container = document.getElementById("announcement-dialog-container");
            const dialog = container.querySelector(".custom-dialog");
            const title = container.querySelector("span");
            const richText = container.querySelector("div[class='announcement-dialog-text']");
            this.okButton = container.querySelector("#dialog-ok-button");
            AnnouncementsDialog.dialogComponents = {
                container: container,
                dialog: dialog,
                title: title,
                richText: richText,
                okBt: this.okButton,
            };
            this.okButton.onclick = () => AnnouncementsDialog.okButtonPressed = true;
        }
        return AnnouncementsDialog.dialogComponents;
    }
    /**
     * Displays a dialog with a message.
     * The user will need to press a button in order to proceed.
     * @param message the message to display on the dialog.
     */
    static show(title, richText, buttonText = "Ok", confetti = false) {
        return __awaiter(this, void 0, void 0, function* () {
            if (AnnouncementsDialog.isDialogShown())
                throw new Error("The announcement dialog is already shown!");
            this.okButton.innerText = buttonText;
            const container = AnnouncementsDialog.getContainer();
            container.title.textContent = title;
            container.richText.innerHTML = richText;
            AnnouncementsDialog.okButtonPressed = false;
            AnnouncementsDialog.showDialog();
            if (confetti)
                window.confetti.start(5000, 150, 150);
            container.okBt.focus();
            yield AnnouncementsDialog.waitForButtonPressed();
            AnnouncementsDialog.hideDialog();
        });
    }
    static waitForButtonPressed() {
        return __awaiter(this, void 0, void 0, function* () {
            const sleep = (ms) => new Promise(resolve => setTimeout(resolve, ms));
            const getResult = () => AnnouncementsDialog.okButtonPressed;
            do {
                yield sleep(50);
            } while (!getResult());
            return getResult();
        });
    }
}
AnnouncementsDialog.dialogComponents = null;
AnnouncementsDialog.isDialogShown = () => AnnouncementsDialog.getContainer().container.style.display === "block";
AnnouncementsDialog.showDialog = () => AnnouncementsDialog.getContainer().container.style.display = "block";
AnnouncementsDialog.hideDialog = () => AnnouncementsDialog.getContainer().container.style.display = "none";
