"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
class UserLevelTracker {
    constructor(userPk) {
        this.experienceStats = null;
        this.userPk = userPk;
        this.assignUserStats();
    }
    /**
     * Initializes the current instance of UserLevelTracker.
     * @param userPk The user pk, used to retrieve data.
     */
    static initialize(userPk) {
        UserLevelTracker.inst = new UserLevelTracker(userPk);
    }
    /** Gets the singleton instance of UserLevelTracker.  */
    static instance() {
        if (UserLevelTracker.inst === null)
            throw Error("The UserLevelTracker has not been initialized yet!");
        return UserLevelTracker.inst;
    }
    assignUserStats() {
        return __awaiter(this, void 0, void 0, function* () {
            this.experienceStats = yield this.getCurrentExperienceStats();
        });
    }
    /**
     * Checks the current user statistics to see if there has been a level up or if the experience changed.
     * @param deletedCategory true if a category has just been deleted. It allows to add a message suggesting to the user to archive it next time.
     */
    checkForLevelChanges(deletedCategory = false) {
        return __awaiter(this, void 0, void 0, function* () {
            const currentStats = yield this.getCurrentExperienceStats();
            if (currentStats.level > this.experienceStats.level)
                this.levelUpProcedure(currentStats);
            else if (currentStats.level < this.experienceStats.level)
                this.levelDownProcedure(currentStats, deletedCategory);
            else if (currentStats.experience < this.experienceStats.experience)
                this.experienceDownProcedure(currentStats);
            else if (currentStats.experience > this.experienceStats.experience)
                this.experienceUpProcedure(currentStats);
            this.experienceStats = currentStats;
        });
    }
    levelUpProcedure(stats) {
        const levelsGained = stats.level - this.experienceStats.level;
        const levels = `${levelsGained} level` + (levelsGained > 1 ? "s" : "");
        MessageManager.success(`You just gained ${levels} and reached level ${stats.level}! 🥳🎉\n\nCheck in the dashboard for more stats.`);
        window.confetti.start(1500, 500, 1000);
    }
    levelDownProcedure(stats, deletedCategory) {
        const levelsLost = this.experienceStats.level - stats.level;
        const levels = `${levelsLost} level` + (levelsLost > 1 ? "s" : "");
        const appendix = deletedCategory
            ? "\n\nArchiving categories instead of deleting them allows you to maintain your experience."
            : "";
        MessageManager.warning(`You just lost ${levels} and went back to level ${stats.level} ☹️${appendix}`);
    }
    experienceDownProcedure(stats) {
        if (Math.random() < 0.9)
            return;
        const experienceLost = this.experienceStats.experience - stats.experience;
        MessageManager.info(`Just to follow up.. \nYou just lost ${experienceLost} experience points 😬\nNo worries! You'll gain it back in no time!`);
    }
    experienceUpProcedure(stats) {
        if (Math.random() < 0.9)
            return;
        const experienceGained = stats.experience - this.experienceStats.experience;
        MessageManager.info(`Just to follow up.. Yassssss, you just gained ${experienceGained} experience points 😉\nKeep doing the good job!`);
    }
    getCurrentExperienceStats() {
        return __awaiter(this, void 0, void 0, function* () {
            return yield $.getJSON(`/dashboard/user_exp_stats/${this.userPk}`);
        });
    }
}
