﻿interface IUserMessagesStatus {
    has_been_thanked_for_donation: boolean;
    has_seen_tutorial_home: boolean;
    has_seen_tutorial_categories: boolean;
    has_seen_tutorial_tree: boolean;
    has_seen_tutorial_dashboard: boolean;
}

class UserMessagesTracker {
    public static async fetchStateAndShowMessageIfNecessary(): Promise<void> {
        const currentStatus = await $.getJSON("/user-messages-status/");
        await this.showMessages(currentStatus);
    }

    private static async showMessages(messagesStatus: IUserMessagesStatus): Promise<void> {
        if (!messagesStatus.has_seen_tutorial_home && location.href.endsWith(location.host + "/")) {
            await UserMessagesTracker.showTutorialHome();
        } else if (!messagesStatus.has_seen_tutorial_categories && location.href.endsWith("categories/")) {
            await UserMessagesTracker.showTutorialCategories();
        } else if (!messagesStatus.has_seen_tutorial_tree && location.href.includes("tree")) {
            await UserMessagesTracker.showTutorialTree();
        } else if (!messagesStatus.has_seen_tutorial_dashboard && location.href.endsWith("dashboard/")) {
            await UserMessagesTracker.showTutorialDashboard();
        }
        //else if (!messagesStatus.has_been_thanked_for_donation)
        //{
        //    await UserMessagesTracker.showDonationReceivedDialog();
        //}
    }


    /* Prefabricated dialogs */

    private static async showTutorialHome(): Promise<void> {
        const title = "Welcome to GoalTree!";
        const text = `
<div style="text-align: center;"> 
    This is a short tutorial to get you used to the website 😉.<br/><br/><br/>
    To start you should go to the <b>categories</b> page.<br/><br/><br/>
    I'll bring you there!
</div> 
`;
        await AnnouncementsDialog.show(title, text);
        await $.post("/user-messages/home-completed");
        location.href = "/categories/";
    }

    private static async showTutorialCategories(): Promise<void> {
        const title1 = "Tutorial: Categories";
        const text1 = `
<div style="text-align: center;"> 
    Ok here is where stuff gets interesting!<br/><br/>
    <b>Categories</b> are the natural containers for your <b>goals</b>.<br/><br/>
    Examples of categories can be '<i>Personal Growth</i>', '<i>Getting Fit</i>', '<i>Build A Drone</i>', '<i>Climb The Everest</i>', '<i>Earn A Million</i>'... <br/><br/>
    Sky is the limit! ✨<br/><br/> 
</div> 
`;
        const title2 = "Tutorial: Create Categories";
        const text2 = `
<div style="text-align: center;"> 
    You can add a category using the '+' button.<br/><br/> 
    Each category has a <u>name</u>, an optional <u>description</u> and a <u>color</u>.<br/>
    Once created, you can modify it anytime by pressing the ⚙️ icon.<br/><br/> 
    
    To get inside the category, just click on it.<br/>
    I'll continue the explanation once you are inside.
</div> 
`;
        await AnnouncementsDialog.show(title1, text1, "Next");
        await AnnouncementsDialog.show(title2, text2, "Ok");
        await $.post("/user-messages/categories-completed");
    }

    public static async showTutorialTree(): Promise<void> {
        const title1 = "Tutorial: Goals";
        const text1 = `
<div style="text-align: center;"> 
    Nice, you are inside a category! You can consider this as your workbench 🔧🔨.<br/><br/>
    You can follow these steps:<br/><br/>
    <ol style="text-align: left;">    
        <li> Start writing out your final <b>goal</b> (or goals) </li>
        <li> Then write smaller ones, that will allow you to get there </li>
        <li> And so on, until you get to very small goals..</li>
    </ol><br/>
    Something like:<br/> 
    '<i>Climb The Everest</i>' ← '<i>Do a 6 hours mountain excursion</i>' ← '<i>Climb 1000 steps</i>' ← '<i>Buy a good pair of shoes</i>'
</div> 
`;
        const title2 = "Tutorial: Creating Goals";
        const text2 = `
<div style="text-align: center;"> 
    <b>Double click</b> to add a new <b>goal</b>.<br/><br/>
    Like with categories, goals have a <u>name</u>, an optional <u>description</u> and a <u>difficulty</u>.<br/><br/> 
    The <u>difficulty</u> is a value you can decide and it represents how tough the goal is. <br/><br/>
    Remember: the higher the difficulty, the higher experience you'll gain after achieving the goal!<br/><br/>
</div> 
`;
        const title3 = "Tutorial: Connections and Side Menu";
        const text3 = `
<div style="text-align: center;"> 
    <b>Connections</b> represent the relationship between goals and describe the route from the point you are now, until the destination 🛣️<br/><br/>    
    To connect the goals, activate the <b>Edge Mode</b> from the menu on the right, then drag the icon to the goals you want to connect to.<br/><br/><br/> 
    On the same menu you will find multiple other tabs, like the <b>Journal</b> or the <b>Pinned Goals</b>.<br/>
    Feel free to explore and discover all the other features yourself.
</div> 
`;
        const title4 = "Tutorial: That's it for now";
        const text4 = `
<div style="text-align: center;"> 
    Once you created some goals, check out the <b>dashboard</b> for some cool charts.<br/><br/>
    Now go and achieve 'em all!<br/><br/>
    <img src='https://i.imgur.com/QgV1lhW.jpg' style="width:85%">
</div> 
`;


        await AnnouncementsDialog.show(title1, text1, "Next (1/3)");
        await AnnouncementsDialog.show(title2, text2, "Next (2/3)");
        await AnnouncementsDialog.show(title3, text3, "Next (3/3)");
        await AnnouncementsDialog.show(title4, text4, "Great");
        await $.post("/user-messages/tree-completed");
    }

    private static async showTutorialDashboard(): Promise<void> {
        const warningIcon = document.createElement("div");
        warningIcon.innerHTML = AnalysisResult.warningIcon;
        warningIcon.querySelector("svg")!.style.height = "30px";

        const title1 = "Tutorial: Dashboard";
        const text1 = `
<div style="text-align: center;"> 
    The <b>dashboard</b> allows you to always keep an eye on the situation of your goals and your level 📈📊<br/><br/>
    Here you will find:<br/><br/>
    <ul style="text-align: left;">
        <li>1 panel for your <b>User Stats</b> (your global summary)</li>
        <li>1 panel for <b>each category</b> you create</li>
    </ul><br/><br/>
    Watch out for the following icon:
        <div>
            ${warningIcon.innerHTML}<br/>
        </div>
    By hovering or clicking it, you will get a list of the problems and hints about how to solve them.
</div> 
`;
        const title2 = "Tutorial: Level and Experience";
        const text2 = `
<div style="text-align: center;"> 
    The first chart is about your <b>level</b>. It symbolizes how hard you worked until now to get out of the comfort zone and achieve your goals.<br/><br/>
    You can see how much <b>experience</b> a goal will give to you once completed directly on the goal tile.<br/><br/><br/>
    The tutorial is officially completed! Congratulations for finishing it and good luck with your goals!
</div> 
`;
        await AnnouncementsDialog.show(title1, text1, "Next (1/2)");
        await AnnouncementsDialog.show(title2, text2, "Ok");
        await $.post("/user-messages/dashboard-completed");
    }

    /**
     * Dialog to be show whenever a user does the first donation.
     * It thanks the user and shoots some confetti.
     */
    private static async showDonationReceivedDialog(): Promise<void> {
        const title = "Thank you for your donation!";
        const text = `
<div> 
    We just received your donation! big or small, it will help GoalTree paying its expenses and keep improving. 
    <br/>
    After all our goal is to get you, our users, the best tools to achieve your own goals!  
    <br/>
    <br/>
    As a special thanks, you will now have access to the following perks:
    <br/>
    <br/>
    <ul>
        <li>Unlimited categories</li>
        <li>Unlimited goals</li>
        <li>Unlimited a picture of me</li>
        <li>A street cat</li>
        <li>Maybe something else as well..</li>
    </ul>
    <br/>
    Enjoy! 
</div> 
`;
        await AnnouncementsDialog.show(title, text, "Ok", true);
        await $.post("/user-messages/donation-completed");
    }
}