"use strict";
/**
 * Class used to find cycles between two connected goals.
 */
class CycleFinder {
    /**
     * Returns True if a path is found from 'destination' to 'start'. False otherwise.
     * @param start The goal the connection is starting from.
     * @param end The goal the connection is arriving to.
     * @param expand The function used to retrieve the connections of a given node.
     */
    static connectionCreatesCycle(start, end, expand) {
        return this.depthFirstSearch(end, start, expand);
    }
    static depthFirstSearch(start, end, expand) {
        if (start === end)
            return true;
        const expanded = expand(start);
        if (expanded.length === 0)
            return false;
        return expanded.some((node) => this.depthFirstSearch(node, end, expand));
    }
}
