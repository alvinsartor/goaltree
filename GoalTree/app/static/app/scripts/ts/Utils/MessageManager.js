"use strict";
class MessageManager {
    static getContainer() {
        if (MessageManager.messageContainer === null) {
            MessageManager.messageContainer =
                document.getElementsByClassName("messages-container")[0];
        }
        return MessageManager.messageContainer;
    }
    /**
     * Spawn a success message. It will automatically disappear after a set timeout.
     * @param text the message text.
     */
    static success(text) {
        MessageManager.createMessage(text, MessageType.Success);
    }
    /**
    * Spawn a warning message. It will automatically disappear after a set timeout.
    * @param text the message text.
    */
    static warning(text) {
        MessageManager.createMessage(text, MessageType.Warning);
    }
    /**
    * Spawn an error message. It will automatically disappear after a set timeout.
    * @param text the message text.
    */
    static error(text) {
        MessageManager.createMessage(text, MessageType.Error);
    }
    /**
    * Spawn an info message. It will automatically disappear after a set timeout.
    * @param text the message text.
    */
    static info(text) {
        MessageManager.createMessage(text, MessageType.Info);
    }
    static createMessage(text, type) {
        const message = new Message(text, type, MessageManager.getContainer());
        setTimeout(() => message.destroy(), 6050);
    }
}
MessageManager.messageContainer = null;
var MessageType;
(function (MessageType) {
    MessageType[MessageType["Success"] = 0] = "Success";
    MessageType[MessageType["Warning"] = 1] = "Warning";
    MessageType[MessageType["Error"] = 2] = "Error";
    MessageType[MessageType["Info"] = 3] = "Info";
})(MessageType || (MessageType = {}));
class Message {
    /**
     * Creates a new instance of Message.
     * @param text the message text.
     * @param type the message type.
     */
    constructor(text, type, container) {
        const template = document.getElementById("message-template")
            .content.querySelector("div[class='message']");
        this.container = container;
        this.messageTile = document.importNode(template, true);
        this.messageTile.classList.add(this.messageClass(type));
        this.messageTile.querySelector("span").textContent = text;
        this.container.appendChild(this.messageTile);
    }
    /**
     * Remove the message from the container.
     */
    destroy() {
        this.container.removeChild(this.messageTile);
    }
    messageClass(type) {
        switch (type) {
            case MessageType.Success:
                return "message-success";
            case MessageType.Warning:
                return "message-warning";
            case MessageType.Error:
                return "message-error";
            case MessageType.Info:
                return "message-info";
            default:
                throw new Error(`Unknown message type: '${type}'`);
        }
    }
}
