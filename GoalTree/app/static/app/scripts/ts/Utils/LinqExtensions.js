"use strict";
/**
 * Returns the first element in the array that satisfies the given predicate.
 * @param predicate the function to be satisfied by the element.
 * @returns The first element that satisfies the predicate. Null if no element does.
 */
Array.prototype.firstOrDefault = function (predicate) {
    return this.reduce((accumulator, currentValue) => {
        if (!accumulator && predicate(currentValue))
            accumulator = currentValue;
        return accumulator;
    }, null);
};
