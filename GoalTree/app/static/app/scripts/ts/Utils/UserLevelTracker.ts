class UserLevelTracker
{
    private static inst: UserLevelTracker | null;
    private experienceStats: IExperienceStats | null = null;
    private readonly userPk: number;

    /**
     * Initializes the current instance of UserLevelTracker.
     * @param userPk The user pk, used to retrieve data.
     */
    public static initialize(userPk: number) {
        UserLevelTracker.inst = new UserLevelTracker(userPk);
    }

    /** Gets the singleton instance of UserLevelTracker.  */
    public static instance() {
        if (UserLevelTracker.inst === null) throw Error("The UserLevelTracker has not been initialized yet!");
        return UserLevelTracker.inst;
    }

    private constructor(userPk: number) {
        this.userPk = userPk;
        this.assignUserStats();
    }

    private async assignUserStats(): Promise<void> {
        this.experienceStats = await this.getCurrentExperienceStats();
    }

    /**
     * Checks the current user statistics to see if there has been a level up or if the experience changed.
     * @param deletedCategory true if a category has just been deleted. It allows to add a message suggesting to the user to archive it next time.
     */
    public async checkForLevelChanges(deletedCategory: boolean = false): Promise<void> {
        const currentStats = await this.getCurrentExperienceStats();

        if (currentStats.level > this.experienceStats!.level) this.levelUpProcedure(currentStats);
        else if (currentStats.level < this.experienceStats!.level)
            this.levelDownProcedure(currentStats, deletedCategory);
        else if (currentStats.experience < this.experienceStats!.experience) this.experienceDownProcedure(currentStats);
        else if (currentStats.experience > this.experienceStats!.experience) this.experienceUpProcedure(currentStats);

        this.experienceStats = currentStats;
    }

    private levelUpProcedure(stats: IExperienceStats) {
        const levelsGained = stats.level - this.experienceStats!.level;
        const levels = `${levelsGained} level` + (levelsGained > 1 ? "s" : "");
        MessageManager.success(
            `You just gained ${levels} and reached level ${stats.level
            }! 🥳🎉\n\nCheck in the dashboard for more stats.`);
        window.confetti.start(1500, 500, 1000);
    }

    private levelDownProcedure(stats: IExperienceStats, deletedCategory: boolean) {
        const levelsLost = this.experienceStats!.level - stats.level;
        const levels = `${levelsLost} level` + (levelsLost > 1 ? "s" : "");
        const appendix = deletedCategory
            ? "\n\nArchiving categories instead of deleting them allows you to maintain your experience."
            : "";

        MessageManager.warning(`You just lost ${levels} and went back to level ${stats.level} ☹️${appendix}`);
    }

    private experienceDownProcedure(stats: IExperienceStats) {
        if (Math.random() < 0.9) return;
        const experienceLost = this.experienceStats!.experience - stats.experience;
        MessageManager.info(
            `Just to follow up.. \nYou just lost ${experienceLost
            } experience points 😬\nNo worries! You'll gain it back in no time!`);
    }

    private experienceUpProcedure(stats: IExperienceStats) {
        if (Math.random() < 0.9) return;
        const experienceGained = stats.experience - this.experienceStats!.experience;
        MessageManager.info(
            `Just to follow up.. Yassssss, you just gained ${experienceGained
            } experience points 😉\nKeep doing the good job!`);
    }

    private async getCurrentExperienceStats(): Promise<IExperienceStats> {
        return await $.getJSON(`/dashboard/user_exp_stats/${this.userPk}`);
    }
}