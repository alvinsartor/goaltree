"use strict";
class LiteEvent {
    constructor() {
        this.handlers = [];
    }
    /**
    * @inheritDoc
    */
    subscribe(handler) {
        this.handlers.push(handler);
    }
    /**
    * @inheritDoc
    */
    unsubscribe(handler) {
        this.handlers = this.handlers.filter(h => h !== handler);
    }
    /**
    * @inheritDoc
    */
    unsubscribeAll() {
        this.handlers = [];
    }
    /**
     * Executes the handlers that subscribed to the event.
     * @param data the data that will be passed to the handlers as argument.
     */
    trigger(data) {
        this.handlers.slice(0).forEach(h => h(data));
    }
    /**
     * Exposes the accessible methods of LiteEvent (so the receiver cannot execute).
     */
    expose() {
        return this;
    }
}
