"use strict";
class Point {
    /**
     * Initializes a new instance of Point
     * @param x the X coordinate
     * @param y the Y coordinate
     */
    constructor(x, y) {
        this.x = x;
        this.y = y;
    }
    /**
     * Returns a point located in coordinates (0;0).
     */
    static zero() {
        return new Point(0, 0);
    }
    /**
     * Given two points, it returns the point at the center.
     * @param p1 the first point.
     * @param p2 the second point.
     */
    static center(p1, p2) {
        return p1.sum(p2).multiply(0.5);
    }
    /**
     * Truncates the values of this point to the closest integer.
     */
    truncate() {
        return new Point(this.x | 0, this.y | 0);
    }
    /**
     * Calculates the distance between this point and the given one.
     * @param other The point the distance will be calculated with.
     */
    distance(other) {
        return Math.sqrt(Math.pow(this.x - other.x, 2) + Math.pow(this.y - other.y, 2));
    }
    /**
     * Returns the normalized version of this point.
     */
    normalize() {
        const u = Math.sqrt(this.x * this.x + this.y * this.y);
        return new Point(this.x / u, this.y / u);
    }
    /**
     * Returns a point that represents the sum of this and the given point.
     * @param p the point that will be summed to this one.
     */
    sum(p) {
        return new Point(this.x + p.x, this.y + p.y);
    }
    /**
     * Returns a point that represents the subtraction between this and the given point.
     * @param p the point that will be subtracted to this one.
     */
    subtract(p) {
        return new Point(this.x - p.x, this.y - p.y);
    }
    /**
     * Returns a new point that represent the multiplication between this point and the given scalar.
     * @param s The scalar that will be multiplied to this point.
     */
    multiply(s) {
        return new Point(this.x * s, this.y * s);
    }
    /**
     * Returns the dot product between this point and the given one.
     * @param p The point that will be used to calculate the dot product.
     */
    dot(p) {
        return this.x * p.x + this.y * p.y;
    }
    /**
     * Returns a new instance, built with the data contained in this instance.
     */
    clone() {
        return new Point(this.x, this.y);
    }
    /**
     * Checks if this point is in the same coordinates as the given point.
     */
    equals(other) {
        return this.x === other.x && this.y === other.y;
    }
    /**
     * Returns the string representation of this object.
     */
    toString() {
        return `[${this.x};${this.y}]`;
    }
    /**
     * Extracts the page point from a UIEvent.
     * e: MouseEvent -> {e.PageX, e.PageY}
     * e: TouchEvent -> {e.touches[0].pageX, e.touches[0].pageY}
    */
    static extractPagePointFromUiEvent(e) {
        if (e instanceof MouseEvent) {
            const me = e;
            return new Point(me.pageX, me.pageY);
        }
        else if (e instanceof TouchEvent) {
            const te = e;
            return new Point(te.touches[0].pageX | 0, te.touches[0].pageY | 0); // using bitwise |0 to truncate
        }
        else {
            throw Error("It seems that the passed event is neither a MouseEvent nor a TouchEvent.");
        }
    }
}
