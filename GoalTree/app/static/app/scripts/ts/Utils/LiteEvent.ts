
interface ILiteEvent<T> {

    /**
     * Add a handler to the specified event.
     * @param handler the function that will be called when the event is triggered.
     */
    subscribe(handler: { (data?: T): void; }): void;

    /**
     * Remove a previously added handler from the event.
     * @param handler the handler that must be removed.
     */
    unsubscribe(handler: { (data?: T): void; }): void;

    /**
     * Removes all the handlers from the event.
     */
    unsubscribeAll(): void;
}

class LiteEvent<T> implements ILiteEvent<T> {
    private handlers: { (data?: T): void; }[] = [];

    /**
    * @inheritDoc
    */
    public subscribe(handler: { (data?: T): void; }): void {
        this.handlers.push(handler);
    }

    /**
    * @inheritDoc
    */
    public unsubscribe(handler: { (data?: T): void; }): void {
        this.handlers = this.handlers.filter(h => h !== handler);
    }

    /**
    * @inheritDoc
    */
    public unsubscribeAll(): void
    {
        this.handlers = [];
    }

    /**
     * Executes the handlers that subscribed to the event.
     * @param data the data that will be passed to the handlers as argument.
     */
    public trigger(data?: T) {
        this.handlers.slice(0).forEach(h => h(data));
    }

    /**
     * Exposes the accessible methods of LiteEvent (so the receiver cannot execute).
     */
    public expose(): ILiteEvent<T> {
        return this;
    }
}