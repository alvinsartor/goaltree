export { };

declare global {
    interface Window {
        confetti: IConfetti;
    }

    interface IConfetti {

        /** starts the confetti animation
         * @param timeout the amount of time the confetti will be thrown for
         * @param min the lower bound for the random number of confetti
         * @param max the upper bound for the random number of confetti
         */
        start(timeout: number, min: number, max: number): void;
    }
}