
type Predicate<T> = (input: T) => boolean;

interface Array<T>
{
    firstOrDefault(predicate: Predicate<T>): T;
}

/**
 * Returns the first element in the array that satisfies the given predicate.
 * @param predicate the function to be satisfied by the element.
 * @returns The first element that satisfies the predicate. Null if no element does. 
 */
Array.prototype.firstOrDefault = function<T>(predicate: Predicate<T>)
{
    return this.reduce((accumulator: T, currentValue: T) =>
    {

        if (!accumulator && predicate(currentValue))
            accumulator = currentValue;

        return accumulator;
    }, null);
};