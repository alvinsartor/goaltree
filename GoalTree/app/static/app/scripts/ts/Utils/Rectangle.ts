class Rectangle {

    public readonly position: Point;
    public readonly size: Point;
    public readonly center: Point;

    public readonly topLine: number;
    public readonly bottomLine: number;
    public readonly leftLine: number;
    public readonly rightLine: number;

    public readonly topAnchorPoint: Point;
    public readonly bottomAnchorPoint: Point;
    public readonly leftAnchorPoint: Point;
    public readonly rightAnchorPoint: Point;

    /**
     * Creates a new instance of Rectangle
     * @param position the rectangle [left; top] position
     * @param size the rectangle [width; height] size
     */
    public constructor(position: Point, size: Point) {
        this.position = position;
        this.size = size;

        this.center = position.sum(size.multiply(0.5));
        this.topLine = position.y;
        this.bottomLine = position.y + size.y;
        this.leftLine = position.x;
        this.rightLine = position.x + size.x;

        this.topAnchorPoint = new Point(this.center.x, this.topLine);
        this.bottomAnchorPoint = new Point(this.center.x, this.bottomLine);
        this.rightAnchorPoint = new Point(this.rightLine, this.center.y);
        this.leftAnchorPoint = new Point(this.leftLine, this.center.y);
    }

    /**
     * Returns true if the given point is contained in this rectangle.
     */
    public contains(pt: Point): boolean {
        return pt.y >= this.topLine &&
            pt.y <= this.bottomLine &&
            pt.x >= this.leftLine &&
            pt.x <= this.rightLine;
    }
}