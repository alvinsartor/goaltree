"use strict";
class Rectangle {
    /**
     * Creates a new instance of Rectangle
     * @param position the rectangle [left; top] position
     * @param size the rectangle [width; height] size
     */
    constructor(position, size) {
        this.position = position;
        this.size = size;
        this.center = position.sum(size.multiply(0.5));
        this.topLine = position.y;
        this.bottomLine = position.y + size.y;
        this.leftLine = position.x;
        this.rightLine = position.x + size.x;
        this.topAnchorPoint = new Point(this.center.x, this.topLine);
        this.bottomAnchorPoint = new Point(this.center.x, this.bottomLine);
        this.rightAnchorPoint = new Point(this.rightLine, this.center.y);
        this.leftAnchorPoint = new Point(this.leftLine, this.center.y);
    }
    /**
     * Returns true if the given point is contained in this rectangle.
     */
    contains(pt) {
        return pt.y >= this.topLine &&
            pt.y <= this.bottomLine &&
            pt.x >= this.leftLine &&
            pt.x <= this.rightLine;
    }
}
