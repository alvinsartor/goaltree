"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
class LoginHandler {
    constructor() {
        this.loginForm = document.querySelector("div[id='login-form']");
        this.loginInitialButton = document.querySelector("button[id='initialLoginBt']");
        this.loginButton = document.querySelector("button[id='login-button']");
        this.switchToLogin = document.querySelector("a[id='switch-to-login-link']");
        this.signinErrors = document.querySelector("div[id='signin-errors']");
        this.signUpForm = document.querySelector("div[id='signup-form']");
        this.signUpInitialButton = document.querySelector("button[id='initialSignupBt']");
        this.signupButton = document.querySelector("button[id='signup-button']");
        this.switchToSignup = document.querySelector("a[id='switch-to-signup-link']");
        this.signupErrors = document.querySelector("div[id='signup-errors']");
        this.forgottenForm = document.querySelector("div[id='forgotten-form']");
        this.forgottenButton = document.querySelector("button[id='forgotten-button']");
        const switchToForgotten1 = document.querySelector("a[id='switch-to-forgotten-link1']");
        const switchToForgotten2 = document.querySelector("a[id='switch-to-forgotten-link2']");
        this.backToLogin = document.querySelector("a[id='back-to-login-link']");
        this.loginInitialButton.onclick = () => this.openLoginForm();
        this.switchToLogin.onclick = () => this.openLoginForm();
        this.signUpInitialButton.onclick = () => this.openSignupForm();
        this.switchToSignup.onclick = () => this.openSignupForm();
        switchToForgotten1.onclick = () => this.openForgottenForm();
        switchToForgotten2.onclick = () => this.openForgottenForm();
        this.backToLogin.onclick = () => this.openSignupForm();
        this.loginButton.onclick = (e) => this.login(e);
        this.signupButton.onclick = (e) => this.signup(e);
        this.forgottenButton.onclick = (e) => __awaiter(this, void 0, void 0, function* () { return this.sendRecoverEmail(e); });
        // if the user wants to be readdressed to another page, let's show the login form directly
        const urlSearch = new URLSearchParams(window.location.search);
        if (urlSearch.has("next"))
            this.openLoginForm();
    }
    openSignupForm() {
        this.loginInitialButton.style.display = "none";
        this.signUpInitialButton.style.display = "none";
        this.signUpForm.style.display = "inline-block";
        this.loginForm.style.display = "none";
        this.forgottenForm.style.display = "none";
        this.cleanAllFields();
        document.getElementById("emailSignupInput").focus();
    }
    openLoginForm() {
        this.loginInitialButton.style.display = "none";
        this.signUpInitialButton.style.display = "none";
        this.signUpForm.style.display = "none";
        this.loginForm.style.display = "inline-block";
        this.forgottenForm.style.display = "none";
        this.cleanAllFields();
        document.getElementById("emailSigninInput").focus();
    }
    openForgottenForm() {
        this.loginInitialButton.style.display = "none";
        this.signUpInitialButton.style.display = "none";
        this.signUpForm.style.display = "none";
        this.loginForm.style.display = "none";
        this.forgottenForm.style.display = "inline-block";
        this.cleanAllFields();
        document.getElementById("emailSigninInput").focus();
    }
    login(e) {
        e.preventDefault();
        e.stopPropagation();
        this.cleanSigninErrors();
        if ($("#emailSigninInput").val() === "") {
            this.addSigninError("Fill up the email field to login");
            return;
        }
        if ($("#passwordSigninInput").val() === "") {
            this.addSigninError("You need a password to login");
            return;
        }
        if ($("#passwordSigninInput").val().length < 8) {
            this.addSigninError("I already know this password can't be right");
            return;
        }
        const form = $("form[name='login-form']");
        $.ajax({
            type: "POST",
            url: `/login/`,
            data: form.serialize(),
            success: () => {
                const urlSearch = new URLSearchParams(window.location.search);
                if (urlSearch.has("next"))
                    location.assign(urlSearch.get("next"));
                else
                    location.reload();
            },
            error: (err) => {
                this.addSigninError("There is a problem with your credentials, check them and try again");
            }
        });
    }
    signup(e) {
        e.preventDefault();
        e.stopPropagation();
        this.cleanSignupErrors();
        if ($("#emailSignupInput").val() === "") {
            this.addSignupError("Fill up the email field to register");
            return;
        }
        if ($("#password1SignupInput").val() === "") {
            this.addSignupError("You need a password to register");
            return;
        }
        if ($("#password1SignupInput").val().length < 8) {
            this.addSignupError("At least 8 characters, that is the only thing I ask");
            return;
        }
        const form = $("form[name='signup-form']");
        $.ajax({
            type: "POST",
            url: `/signup/`,
            data: form.serialize(),
            success: () => {
                const urlSearch = new URLSearchParams(window.location.search);
                if (urlSearch.has("next"))
                    location.assign(urlSearch.get("next"));
                else
                    location.reload();
            },
            error: (err) => {
                this.addSignupError(err.responseText);
            }
        });
    }
    sendRecoverEmail(e) {
        return __awaiter(this, void 0, void 0, function* () {
            e.preventDefault();
            this.cleanSignupErrors();
            const emailFieldContent = $("#emailForgottenInput").val();
            if (emailFieldContent === "") {
                this.addSignupError("Mmh where am I supposed to send the recovery instructions?");
                return;
            }
            if (!emailFieldContent.includes("@") || !emailFieldContent.includes(".")) {
                this.addSignupError("Are you sure that one is an email?");
                return;
            }
            this.forgottenButton.disabled = true;
            const sleep = (ms) => new Promise(resolve => setTimeout(resolve, ms));
            let countdown = 3;
            while (countdown > 0) {
                MessageManager.info(`Email in ${countdown--}...`);
                yield sleep(1000);
            }
            MessageManager.info(`Sent!`);
            yield sleep(200);
            this.forgottenForm.querySelector("form").submit();
        });
    }
    cleanAllFields() {
        $("#emailSigninInput").val("");
        $("#passwordSigninInput").val("");
        $("#emailSignupInput").val("");
        $("#password1SignupInput").val("");
        $("#password2SignupInput").val("");
        this.cleanSigninErrors();
        this.cleanSignupErrors();
    }
    addSignupError(msg) {
        this.signupErrors.innerHTML += msg + "\n";
        this.signupErrors.style.display = "block";
    }
    cleanSignupErrors() {
        this.signupErrors.innerHTML = "";
        this.signupErrors.style.display = "none";
    }
    addSigninError(msg) {
        this.signinErrors.innerHTML += msg + "\n";
        this.signinErrors.style.display = "block";
    }
    cleanSigninErrors() {
        this.signinErrors.innerHTML = "";
        this.signinErrors.style.display = "none";
    }
}
