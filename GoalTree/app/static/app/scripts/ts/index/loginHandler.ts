
class LoginHandler {
    private readonly loginForm: HTMLDivElement;
    private readonly loginInitialButton: HTMLButtonElement;
    private readonly loginButton: HTMLButtonElement;
    private readonly switchToLogin: HTMLLinkElement;
    private readonly signinErrors: HTMLDivElement;

    private readonly signUpForm: HTMLDivElement;
    private readonly signUpInitialButton: HTMLButtonElement;
    private readonly signupButton: HTMLButtonElement;
    private readonly switchToSignup: HTMLLinkElement;
    private readonly signupErrors: HTMLDivElement;

    private readonly forgottenForm: HTMLFormElement;
    private readonly forgottenButton: HTMLButtonElement;
    private readonly backToLogin: HTMLLinkElement;

    constructor() {
        this.loginForm = document.querySelector("div[id='login-form']") as HTMLDivElement;
        this.loginInitialButton = document.querySelector("button[id='initialLoginBt']") as HTMLButtonElement;
        this.loginButton = document.querySelector("button[id='login-button']") as HTMLButtonElement;
        this.switchToLogin = document.querySelector("a[id='switch-to-login-link']") as HTMLLinkElement;
        this.signinErrors = document.querySelector("div[id='signin-errors']") as HTMLDivElement;

        this.signUpForm = document.querySelector("div[id='signup-form']") as HTMLDivElement;
        this.signUpInitialButton = document.querySelector("button[id='initialSignupBt']") as HTMLButtonElement;
        this.signupButton = document.querySelector("button[id='signup-button']") as HTMLButtonElement;
        this.switchToSignup = document.querySelector("a[id='switch-to-signup-link']") as HTMLLinkElement;
        this.signupErrors = document.querySelector("div[id='signup-errors']") as HTMLDivElement;

        this.forgottenForm = document.querySelector("div[id='forgotten-form']") as HTMLFormElement;
        this.forgottenButton = document.querySelector("button[id='forgotten-button']") as HTMLButtonElement;
        const switchToForgotten1 = document.querySelector("a[id='switch-to-forgotten-link1']") as HTMLLinkElement;
        const switchToForgotten2 = document.querySelector("a[id='switch-to-forgotten-link2']") as HTMLLinkElement;
        this.backToLogin = document.querySelector("a[id='back-to-login-link']") as HTMLLinkElement;

        this.loginInitialButton.onclick = () => this.openLoginForm();
        this.switchToLogin.onclick = () => this.openLoginForm();
        this.signUpInitialButton.onclick = () => this.openSignupForm();
        this.switchToSignup.onclick = () => this.openSignupForm();
        switchToForgotten1.onclick = () => this.openForgottenForm();
        switchToForgotten2.onclick = () => this.openForgottenForm();
        this.backToLogin.onclick = () => this.openSignupForm();

        this.loginButton.onclick = (e: Event) => this.login(e);
        this.signupButton.onclick = (e: Event) => this.signup(e);
        this.forgottenButton.onclick = async (e: Event) => this.sendRecoverEmail(e);

        // if the user wants to be readdressed to another page, let's show the login form directly
        const urlSearch = new URLSearchParams(window.location.search);
        if (urlSearch.has("next")) this.openLoginForm();
    }

    private openSignupForm() {
        this.loginInitialButton.style.display = "none";
        this.signUpInitialButton.style.display = "none";
        this.signUpForm.style.display = "inline-block";
        this.loginForm.style.display = "none";
        this.forgottenForm.style.display = "none";
        this.cleanAllFields();

        document.getElementById("emailSignupInput")!.focus();
    }

    private openLoginForm() {
        this.loginInitialButton.style.display = "none";
        this.signUpInitialButton.style.display = "none";
        this.signUpForm.style.display = "none";
        this.loginForm.style.display = "inline-block";
        this.forgottenForm.style.display = "none";
        this.cleanAllFields();

        document.getElementById("emailSigninInput")!.focus();
    }
    
    private openForgottenForm() {
        this.loginInitialButton.style.display = "none";
        this.signUpInitialButton.style.display = "none";
        this.signUpForm.style.display = "none";
        this.loginForm.style.display = "none";
        this.forgottenForm.style.display = "inline-block";
        this.cleanAllFields();

        document.getElementById("emailSigninInput")!.focus();
    }

    private login(e: Event) {
        e.preventDefault();
        e.stopPropagation();

        this.cleanSigninErrors();
        if ($("#emailSigninInput").val() === "") {
            this.addSigninError("Fill up the email field to login");
            return;
        }
        if ($("#passwordSigninInput").val() === "") {
            this.addSigninError("You need a password to login");
            return;
        }
        if (($("#passwordSigninInput").val() as string).length < 8) {
            this.addSigninError("I already know this password can't be right");
            return;
        }
        const form = $("form[name='login-form']");

        $.ajax({
            type: "POST",
            url: `/login/`,
            data: form.serialize(),
            success: () => {
                const urlSearch = new URLSearchParams(window.location.search);
                if (urlSearch.has("next")) location.assign(urlSearch.get("next") as string);
                else location.reload();
            },
            error: (err) => {
                this.addSigninError("There is a problem with your credentials, check them and try again");
            }
        });
    }

    private signup(e: Event) {
        e.preventDefault();
        e.stopPropagation();

        this.cleanSignupErrors();
        if ($("#emailSignupInput").val() === "") {
            this.addSignupError("Fill up the email field to register");
            return;
        }
        if ($("#password1SignupInput").val() === "") {
            this.addSignupError("You need a password to register");
            return;
        }
        if (($("#password1SignupInput").val() as string).length < 8) {
            this.addSignupError("At least 8 characters, that is the only thing I ask");
            return;
        }

        const form = $("form[name='signup-form']");

        $.ajax({
            type: "POST",
            url: `/signup/`,
            data: form.serialize(),
            success: () => {
                const urlSearch = new URLSearchParams(window.location.search);
                if (urlSearch.has("next")) location.assign(urlSearch.get("next") as string);
                else location.reload();
            },
            error: (err) => {
                this.addSignupError(err.responseText);
            }
        });
    }

    private async sendRecoverEmail(e : Event): Promise<void> {
        e.preventDefault();


        this.cleanSignupErrors();
        const emailFieldContent = $("#emailForgottenInput").val() as string;
        if (emailFieldContent === "")
        {
            this.addSignupError("Mmh where am I supposed to send the recovery instructions?");
            return;
        }

        if (!emailFieldContent.includes("@") || !emailFieldContent.includes(".")) {
            this.addSignupError("Are you sure that one is an email?");
            return;
        }

        this.forgottenButton.disabled = true;

        const sleep = (ms: number): Promise<void> => new Promise(resolve => setTimeout(resolve, ms));
        let countdown = 3;
        while (countdown > 0) {
            MessageManager.info(`Email in ${countdown--}...`);
            await sleep(1000);
        }

        MessageManager.info(`Sent!`);
        await sleep(200);
        this.forgottenForm.querySelector("form")!.submit();
    }

    private cleanAllFields(): void {
        $("#emailSigninInput").val("");
        $("#passwordSigninInput").val("");
        $("#emailSignupInput").val("");
        $("#password1SignupInput").val("");
        $("#password2SignupInput").val("");
        this.cleanSigninErrors();
        this.cleanSignupErrors();
    }

    private addSignupError(msg: string): void {
        this.signupErrors.innerHTML += msg + "\n";
        this.signupErrors.style.display = "block";
    }

    private cleanSignupErrors(): void {
        this.signupErrors.innerHTML = "";
        this.signupErrors.style.display = "none";
    }

    private addSigninError(msg: string): void {
        this.signinErrors.innerHTML += msg + "\n";
        this.signinErrors.style.display = "block";
    }

    private cleanSigninErrors(): void {
        this.signinErrors.innerHTML = "";
        this.signinErrors.style.display = "none";
    }

}