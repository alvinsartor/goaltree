
class MenuEntryUnderline{

    public static selectCurrentMenuEntry(): void {
        const landscapeLinks = Array.from(document.getElementById("menu-bar-landscape")!.querySelectorAll("a"));
        landscapeLinks.forEach(link => {
            if (location.href === link.href) {
                link.classList.add("selected");
            }
        });

        const portraitLinks = Array.from(document.getElementById("menu-bar-portrait")!.querySelectorAll("a"));
        portraitLinks.forEach(link => {
            if (location.href === link.href) {
                link.classList.add("selected");
            }
        });
    }
}