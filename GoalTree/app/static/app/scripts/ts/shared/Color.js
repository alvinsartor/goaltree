"use strict";
class Color {
    constructor(pk, name, c1, c3, c5, c7, c9, text) {
        this.pk = pk;
        this.name = name;
        this.c1 = c1;
        this.c3 = c3;
        this.c5 = c5;
        this.c7 = c7;
        this.c9 = c9;
        this.text = text;
    }
    static fromIColor(color) {
        return new Color(color.pk, color.fields.name, color.fields.c1, color.fields.c3, color.fields.c5, color.fields.c7, color.fields.c9, color.fields.text);
    }
    static default() {
        return new Color(-1, "Default Color", "#b2dfdb", "#4db6ac", "#009688", "#00796b", "#004d40", "#ffffff");
    }
}
