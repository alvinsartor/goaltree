interface IColor {
    pk: number;
    fields: {
        name: string;
        c1: string;
        c3: string;
        c5: string;
        c7: string;
        c9: string;
        text: string;
    };
}

class Color
{
    public readonly pk: number;
    public readonly name: string;
    public readonly c1: string;
    public readonly c3: string;
    public readonly c5: string;
    public readonly c7: string;
    public readonly c9: string;
    public readonly text: string;

    public static fromIColor(color: IColor): Color {
        return new Color(color.pk,
            color.fields.name,
            color.fields.c1,
            color.fields.c3,
            color.fields.c5,
            color.fields.c7,
            color.fields.c9,
            color.fields.text);
    }

    constructor(pk: number, name: string, c1: string, c3: string, c5: string, c7: string, c9: string, text: string) {
        this.pk = pk;
        this.name = name;
        this.c1 = c1;
        this.c3 = c3;
        this.c5 = c5;
        this.c7 = c7;
        this.c9 = c9;
        this.text = text;
    }

    public static default(): Color {
        return new Color(-1, "Default Color", "#b2dfdb", "#4db6ac", "#009688", "#00796b", "#004d40", "#ffffff");
    }
}