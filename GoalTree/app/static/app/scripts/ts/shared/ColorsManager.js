"use strict";
class ColorsManager {
    constructor() {
        this.isInitialized = false;
        this.colorsContainer = {};
    }
    /**
     * Creates a new instance of ColorsManager.
     * @param colors The list of supported colors.
     */
    static initialize(colors) {
        if (this.inst.isInitialized)
            throw Error("The ColorsManager has already been initialized.");
        colors.forEach(color => this.inst.addColor(color));
        this.inst.isInitialized = true;
    }
    addColor(color) {
        if (this.colorsContainer[color.pk])
            throw new Error(`Color with id '${color.pk}' already exists.`);
        this.colorsContainer[color.pk] = Color.fromIColor(color);
    }
    /**
     * Gets the instance of ColorsManager.
     */
    static instance() {
        if (!this.inst.isInitialized)
            throw Error("The ColorsManager has not been initialized yet.");
        return this.inst;
    }
    /**
     * Returns the list of all colors.
     */
    get colors() {
        return Object.keys(this.colorsContainer).map(c => this.colorsContainer[Number(c)]);
    }
    /**
     * Gets the default colors for category and goal.
     */
    get defaultCategoryAndGoalColor() {
        const keys = Object.keys(this.colorsContainer);
        const categoryColor = this.colorsContainer[Number(keys[0])];
        const goalsColor = this.colorsContainer[Number(keys[1])];
        return [categoryColor, goalsColor];
    }
    /**
     * Returns the color associated to the given pk.
     */
    getColor(pk) {
        if (!this.colorsContainer[pk])
            throw new Error("Trying to fetch nonexistent color.");
        return this.colorsContainer[pk];
    }
}
ColorsManager.inst = new ColorsManager();
