
class ColorsManager {

    private static inst = new ColorsManager();
    private isInitialized = false;
    private colorsContainer: { [pk: number]: Color; } = {};

    private constructor() {}

    /**
     * Creates a new instance of ColorsManager.
     * @param colors The list of supported colors.
     */
    public static initialize(colors: IColor[])
    {
        if (this.inst.isInitialized) throw Error("The ColorsManager has already been initialized.");

        colors.forEach(color => this.inst.addColor(color));
        this.inst.isInitialized = true;
    }

    private addColor(color: IColor): void
    {
        if (this.colorsContainer[color.pk]) throw new Error(`Color with id '${color.pk}' already exists.`);
        this.colorsContainer[color.pk] = Color.fromIColor(color);
    }

    /**
     * Gets the instance of ColorsManager.
     */
    public static instance()
    {
        if (!this.inst.isInitialized) throw Error("The ColorsManager has not been initialized yet.");
        return this.inst;
    }

    /**
     * Returns the list of all colors.
     */
    public get colors(): Color[]
    {
        return Object.keys(this.colorsContainer).map(c => this.colorsContainer[Number(c)]);
    }

    /**
     * Gets the default colors for category and goal.
     */
    public get defaultCategoryAndGoalColor(): [Color, Color]
    {
        const keys = Object.keys(this.colorsContainer);
        const categoryColor = this.colorsContainer[Number(keys[0])];
        const goalsColor = this.colorsContainer[Number(keys[1])];

        return [categoryColor, goalsColor];
    }

    /**
     * Returns the color associated to the given pk.
     */
    public getColor(pk: number): Color
    {
        if (!this.colorsContainer[pk]) throw new Error("Trying to fetch nonexistent color.");
        return this.colorsContainer[pk];
    }
}