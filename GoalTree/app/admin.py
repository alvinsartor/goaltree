"""
Customizations for the Django administration interface.
"""

from django.contrib import admin
from app.models import Category, Goal, Color, Experience, User, JournalEntry, UserMessages
from django.contrib.auth.admin import UserAdmin as DjangoUserAdmin


class MessagesInline(admin.TabularInline):
    """ Definition of the user messages inline admin editor. It will be shown on the User admin page. """
    model = UserMessages

@admin.register(User)
class UserAdmin(DjangoUserAdmin):
    """Define admin model for custom User model with no email field."""

    fieldsets = [
        ('Basic Info', {'fields': ['email', 'password', 'has_donated']}),
        #('Permissions', {'fields': ('is_active', 'is_staff', 'is_superuser', 'user_permissions')}),
        ('Important dates', {'fields': ('last_login', 'date_joined')}),
    ]
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('email', 'password1', 'password2'),
        }),
    )
    list_display = ['email', 'date_joined', 'last_login', 'categories_count', 'goals_count']
    ordering = ['-last_login']
    inlines = [MessagesInline]

    def categories_count(self, obj):
        return obj.category_set.count()

    def goals_count(self, obj):
        return Goal.objects.filter(category__user=obj).count()

class GoalInline(admin.TabularInline):
    """Goal objects can be edited inline in the Category editor."""
    model = Goal
    extra = 3

@admin.register(Category)
class CategoryAdmin(admin.ModelAdmin):
    """Definition of the Category admin editor."""
    fieldsets = [
        (None, {'fields': ['name', 'description', 'canvas_color', 'goals_color', 'user', 'archived']})
    ]
    inlines = [GoalInline]

@admin.register(Goal)
class GoalAdmin(admin.ModelAdmin):
    """Definition of the Goal admin editor."""
    fieldsets = [
        ('Basic Info', {'fields': ['title', 'description', 'category']}),
        ('Visualization', {'fields': ['posX', 'posY']}),       
        ('connections', {'fields': ['higher_goals']}),       
    ]
    exclude = ('connections',)
    list_display = ('title', 'pub_date')
    list_filter = ['pub_date']
    search_fields = ['title']
    date_hierarchy = 'pub_date'

@admin.register(Color)
class ColorAdmin(admin.ModelAdmin):
    """Definition of the color admin editor."""
    fieldsets = [(None, {'fields': ['name', 'c1', 'c3', 'c5', 'c7', 'c9', 'text', 'order']})]
    list_display = ['name', 'order']
    ordering = ['order']

@admin.register(Experience)
class ExperienceAdmin(admin.ModelAdmin):
    """Definition of the experience admin editor."""
    fieldsets = [(None, {'fields': ['level', 'description', 'exp_points']})]
    list_display = ['level', 'description', 'exp_points']

@admin.register(JournalEntry)
class JournalAdmin(admin.ModelAdmin):
    """Definition of the experience admin editor."""
    fieldsets = [(None, {'fields': ['text', 'category']})]

