import math
import time
import json
from functools import reduce
from datetime import timedelta

from django.utils import timezone
from django.core import serializers
from django.db.models import Sum, Count

class UserLevelHandler():

    @classmethod
    def get_user_level_from_experience(cls, exp):
        sum = 0
        level = 0
        while sum <= exp:
            level += 1
            sum += UserLevelHandler.get_required_experience(level)
        return (level, sum - exp)

    @classmethod
    def get_level_completion_percentage(cls, level, experience):
        next_level_experience = UserLevelHandler.get_accumulative_required_experience(level+1)
        current_level_experience = UserLevelHandler.get_accumulative_required_experience(level)
        return (experience - current_level_experience) / (next_level_experience - current_level_experience)

    @classmethod
    def get_required_experience(cls, level):
        return math.floor(math.pow(level, 2.05))
    
    @classmethod
    def get_accumulative_required_experience(cls, level):
        level = 1 if level < 1 else level
        return reduce((lambda acc, level: acc + UserLevelHandler.get_required_experience(level)), range(level))


class GoalsStatsExtractor():

    @classmethod
    def get_latest_completed_goals(self, goals_queryset):        
        goals = goals_queryset.filter(completed=True).select_related('category').order_by('-completion_date')[:10]
        result = [{
            'title' : goal.title, 
            'backgroundColor': goal.category.goals_color.c3, 
            'textColor': goal.category.goals_color.text}  for goal in goals]
        return result

    @classmethod
    def get_goal_difficulty_distribution(self, goals_queryset):        
        difficulty_distribution = goals_queryset\
            .values('experience__level')\
            .annotate(lv_count=Count('experience__level'))\
            .order_by('experience__level')
        on_dict = dict((lv['experience__level'], lv['lv_count']) for lv in difficulty_distribution)        
        # now we fill the gaps with [lv, 0] to make it easier for the client-side script
        result = []
        for i in range(1, 11):
            count_i = [i, on_dict[i]] if i in on_dict else [i, 0] 
            result.append(count_i)
        return result
        
        goals = dict((g['day'], g['count']) for g in completed_goals_last_90_d)
        days = ["{:%Y-%m-%d}".format(last_90_d + timedelta(days=x)) for x in range(1, 91)]
        day_goals = [[day, goals[day] if day in goals else 0] for day in days]        
        reduced = [[day_goals[i][0], day_goals[i][1] + day_goals[i-1][1] + day_goals[i-2][1]]  for i in range(2, 91, 3)]
        summed = [val if indx == 0 else [val[0], val[1] + reduced[indx-1][1]] for indx, val in enumerate(reduced)]

        return summed
    
    @classmethod
    def get_goal_distribution_over_categories(self, categories):
        def get_category_data(category):
            open_goals = category.goal_set.filter(completed=False).count()
            completed_goals = category.goal_set.all().count() - open_goals
            return [category.name, open_goals, completed_goals]
        def total_nr_goals(entry):
            return entry[1] + entry[2]
        l = [get_category_data(category) for category in categories]
        l.sort(key=total_nr_goals, reverse=True)
        return l

    @classmethod
    def get_goals_completion_sum_for_last_period(self, goals_queryset):
        last_60_d = timezone.now() - timedelta(days=60)
        completed_goals_last_60_d = goals_queryset\
            .filter(completion_date__gte=last_60_d)\
            .extra(select={'day': "TO_CHAR(completion_date, 'MM-DD')"})\
            .values('day') \
            .order_by('day') \
            .annotate(count=Count('completion_date'))
        
        goals = dict((g['day'], g['count']) for g in completed_goals_last_60_d)
        days = ["{:%m-%d}".format(last_60_d + timedelta(days=x)) for x in range(1, 61)]
        return GoalsStatsExtractor.group_and_accumulate(goals, days, 5)


    @classmethod
    def get_goals_addition_sum_for_last_period(self, goals_queryset):
        last_60_d = timezone.now() - timedelta(days=60)
        addition_goals_last_60_d = goals_queryset\
            .filter(pub_date__gte=last_60_d)\
            .extra(select={'day': "TO_CHAR(pub_date, 'MM-DD')"})\
            .values('day') \
            .order_by('day') \
            .annotate(count=Count('pub_date'))
        
        goals = dict((g['day'], g['count']) for g in addition_goals_last_60_d)
        days = ["{:%m-%d}".format(last_60_d + timedelta(days=x)) for x in range(1, 61)]
        return GoalsStatsExtractor.group_and_accumulate(goals, days, 5)

    @classmethod
    def group_and_accumulate(cls, goal_count, days, size):
        """
            Given a dictionary containing a count of goals and a list of days, it groups the goals in groups of set size

            goal_count: dictionary with the following structure => { day1: count1, day5: count5, ... }
            days: list containing all the days to be considered => [ day1, day2, day3, day4, day5, ... ]
            size: size of the groups

            returns a list with tuples containing day and count => [ [day10, count1-10], [day20, count11-20], ... ]
        """
        day_goals = [[day, goal_count[day] if day in goal_count else 0] for day in days]        
        reduced = []
        for i in range(size, len(days), size):
            accumulator = 0
            for j in range(i-size, i):
                accumulator += day_goals[j][1]
            reduced.append([day_goals[i][0], accumulator])

        accumulator = 0
        for k in range(i, len(days)):
            accumulator += day_goals[k][1]
        reduced.append([day_goals[len(days)-1][0], accumulator])

        summed = []
        for indx, val in enumerate(reduced):
            if indx == 0:
                summed.append(val)
            else:
                summed.append([val[0], val[1] + summed[len(summed)-1][1]])
        return summed