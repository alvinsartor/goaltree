# Generated by Django 2.2.9 on 2020-06-13 16:45

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0016_auto_20200613_1603'),
    ]

    operations = [
        migrations.AlterField(
            model_name='usermessages',
            name='user',
            field=models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, primary_key=True, related_name='messages', serialize=False, to=settings.AUTH_USER_MODEL),
        ),
    ]
