import django

def test_experience_formula():
    """The experience formula returns plausible results"""
    django.setup()
    from app.utils import UserLevelHandler
        
    assert(UserLevelHandler.get_required_experience(0) == 0)

    for i in range(1, 100):
        assert(UserLevelHandler.get_required_experience(i) > UserLevelHandler.get_required_experience(i+1))

