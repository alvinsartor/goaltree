import django
from django.utils import timezone

def test_GoalsCanBeConnected():
    """New Goals can be created and connected together"""
    django.setup()
    from app.models import Goal, Category, Edge

    fooCategory = Category(name="fooCategory")
    fooCategory.save()

    goal1 = Goal(title="finish this thing", 
                 description="it'd be amazing if I could finish this Django Project",
                 category = fooCategory,
                 pub_date = timezone.now())
    goal1.save()

    goal2 = Goal(title="learn Django", 
                 description="learning Django will allow me to make more and more projects",
                 category = fooCategory,
                 pub_date = timezone.now())
    goal2.save()

    edge = Edge(from_goal=goal2, to_goal=goal1)
    edge.save()

    assert(goal1.is_final_goal())
    assert(not goal1.is_leaf_goal())
    assert(goal2.is_leaf_goal())
    assert(not goal2.is_final_goal())

    goal2.delete()

    assert(goal1.is_final_goal())
    assert(goal1.is_leaf_goal())

    goal1.delete();
    fooCategory.delete();